---
title : Once in a while
category : ego
layout : default
---

Once in a while there's that feeling that the world isn't right.

And it feels like you've been signed up for a long term study as a child.

You feel like you've been watched and observed your entire life.

And everyone around you just acts as if they received orders by someone of importance.

You play the game. They get their gain.

The entries in your journal keep repeating and you wonder why?

You are Truman Burbank, Dennis Quaid and Neo, but there's no one to save and no girl to get.

And in the end, it's not a movie.

And in the end life was just a game.

(n-th cycle, 2022)

(No, it isn't supposed to rhym)
