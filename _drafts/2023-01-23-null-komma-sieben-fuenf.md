---
title: Null Komma Sieben Fünf
layout: default
category: ego
---
Nachdem ich 2015, am Tag als ich meine Bachelor-Urkunde erhielt, das letzte mal mehr als ein Bier am Tag getrunken habe, habe ich heute eine ganze Flasche Wein geschafft.

<iframe width="560" height="315" src="https://www.youtube.com/watch?v=ejp4hPnckg8" title="Home Concert #44 (3h Ambient set with Moog, Oberheim, Roland & Waldorf synths)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
