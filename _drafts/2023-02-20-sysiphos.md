---
title: Sysiphos
category: ego
layout: default
---

Die Webseite lto berichtet ganz aktuell, dass durch einen Interpunktionsfehler bei der letzten Novellierung des NpSG im September 2022 verboten geglaubte Medikamente versehentlich legalisiert wurden.

Das erstmals im Jahr 2016 erlassene [Neue-psychoaktive-Stoffe-Gesetz](https://www.bundesgesundheitsministerium.de/service/begriffe-von-a-z/n/npsg.html) sollte nach einem Urteil des Europäischen Gerichtshof Medikamente, die nicht unter den Begriff Arzneimittelbegriff des Betäubungsmittelsgesetzes fallen, weil deren Monetarisierung nicht durch Pharmakonzerne gesichert schien, zeitnah verbieten können, sobald neue im Volksmund auch "Drogen" genannte Wirkstoffe bekannt werden.

Ähnlich wie die Pharmaindustrie, die regelmäßig neue Wirkstoffe mit geringem Zusatznutzen oder sogar schädlicher Wirkung auf den Markt bringt, werden durch genossenschaftlich organisierte Produktionsstätten neue Wirkstoffe entwickelt, die einem verbotenen psychoaktiven Wirkstoff chemisch ähneln, in der Wirkung dem verbotenen Stoff gleich sind, aber durch eine Erfassung im NpSG illegal sind.

Um neu entwickelte "Drohschen" in die Illegalität zu überführen bedarf es einer Novellierung des Gesetzes, bei der neue Moleküle mit psychoaktiver Wirkung oder Stoffgruppen in das Gesetz aufgenommen werden.

Viele Medikamente bestehen aus einem Teil, der im Körper zu einer bestimmte und gewünschten Reaktion führt. Andere Teile der Medikamente dienen lediglich zur Stabilisierung oder der besseren Haltbarkeit. Diese chemischen Moleküle haben keine bekannte Auswirkungen auf den Körper.

In den frühen 2000er Jahren machten sich das genossenschaftlich denkende Chemiestudenten zu eigen und konnten dadurch wirkungsvolle Medikamente ausserhalb der regulierten Pharmaindustrie entwickeln, ohne dabei illegal handeln zu müssen. In den folgenden Jahren kamen zahlreiche, sogenannte "Legal Highs" auf den bunten Markt, die ihre Wirkung wie die verbotenen "Drooschen" entfalteten, juristisch aber nicht vom BtMG und später dem NpSG erfasst waren, und somit legal waren.

Ein bekanntes Beispiel ist das ehemalige Medikament [LSD-25](https://de.wikipedia.org/wiki/LSD). Die psychoaktive Wirkung LSD-25 wurde von [Albert Hofmann](https://de.wikipedia.org/wiki/Albert_Hofmann) zufällig entdeckt. Nach Jahrzehnten wurde LSD-25 dann gegen den Willen Albert Hofmanns und vieler anderer Konsumenten in vielen Ländern der Welt verboten.

LSD ist die Abkürzung für Lysergsäurediethylamid. Die Nummer 25 beschreibt lediglich den 25. Versuch Hofmanns ein Kreislaufstimulans auf Basis der Lysergsäure zu entwickeln. "Lyserg"- leitet sich von "ergot" ab, dem französischen Wort für Mutterkorn. Das Mutterkorn ist ein Pilz der auf dem Weizen wächst, dessen Alkaloid ["Ergin"](https://de.wikipedia.org/wiki/Ergin) (kurz LSA) als Basis für die industrielle Arzneimittelforschung und -entwicklung dient. Ergin ist daher nicht vim BtMG. An sich ist es in vielen Pflanzensamen und eben auch im eher schwer erhältlichen Mutterkorn enthalten. Diejenigen die sich auskennen, können also Ergin aus dem Mutterkorn oder bestimmten Pflanzensamen (z. B. der frei erhältlichen Prunkwinde oder der Muskatnuss) extrahieren. Doch, ja. Wirklich.

Die jenigen, die sich besser auskennen können es auch synthetisch herstellen. Die meisten können sich gar nicht vorstellen, wieviel Medikamente einfach so in der Wildnis wachsen. Ein gekauften Pflanzenlexikon auf einem Flohmarkt hilft.

Wenn man sich die chemische Struktur von Ergin (LSA) und LSD-25 anschaut, erkennt man (auch als Laie) die Ähnlichkeit. Es ist daher nicht verwunderlich, das sowohl Ergin, als auch das verbotene LSD-25 bei Einnahme eine psychoaktive Wirkung entfalten.

Albert Hofmann beschrieb dies so:

    Nach der Entdeckung der psychischen Wirkungen von LSD hatte ich auch Lysergsäure-amid im Selbstversuch geprüft und festgestellt, daß es — allerdings erst in einer etwa zehn- bis zwanzigmal höheren Dosierung als LSD — ebenfalls einen traumartigen Zustand erzeugte. Dieser war gekennzeichnet durch ein Gefühl geistiger Leere und der Unwirklichkeit und Sinnlosigkeit der äußeren Welt, durch gesteigerte Empfindlichkeit des Gehörs und eine nicht unangenehme körperliche Müdigkeit, die schließlich in Schlaf mündete. … Ferner sind die psychischen Wirkungen von Ololiuqui doch verschieden von denen von LSD, da die euphorische und die halluzinogene Komponente weniger ausgeprägt sind und meistens Gefühle geistiger Leere, oft der Angst und Depression vorherrschen. Auch der schlapp- und müdemachende Effekt ist bei einem Rauschmittel unerwünscht.

Als das chemische Wissen und die Hygiene bei der Lebensmittelherstellung mit Weizen, noch nicht so umfangreich war, kam mes häufig zu "Vergiftungen" durch Ergin. Je nach aufgenommener Dosis kam es hier zu Schmerzlinderungen aber auch zu Halluzinationen, wie sie durch andere natürlich vorkommene Pflanzen und Tiersekrete oder auch durch synthetische Stoffe bekannt sind. Teils wurde in diesen Zeiten das Mutterkorn gezielt als Schmerzmittel, z. B. bei Wehen eingesetzt.

**Was hat das also mit dem Gesetz zu tun?**

Das NpSG listet eine ganze Reihe von chemischen Verbindungen,
 die an wirksame Moleküle (https://www.gesetze-im-internet.de/npsg/NpSG.pdf), die möglicherweise als Stubstitut für e



**Hinweis**

Vom Konsum von rohen Pflanzensamen oder Pflanzen rate ich dringend ab. Diese enthalten nicht nur die vielleicht gewünschten Wirkstoffe, sondern auch zahlreiche Andere, die beim Menschen zu Übelkeit, Angstzuständen und anderer negativer Begleiterscheinung führen können. Nur eine richtig durchgeführte Extraktion, mehrfache Destillation und Säuberung von Wirkstoffen ist erfolgsversprechend. Die allermeisten der zahlreich auf Youtube verfügbaren Videos zeigen nicht wie man es richtig macht. Ein Laie könnte die Wirkstoffe nach Anleitung und mit Hilfe der richtigen Geräte
extrahieren, der Vorgang ist aber fehleranfällig und benötigt bereits einige Materialien, die einiges an Geld kosten.

