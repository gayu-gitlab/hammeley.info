---
title: Die Schwurbeluniversität
layout: default
category: schwurbelu
--- 

# ipse se nihil scire id unum sciat

Willkommen an der Schwurbeluniversität. "Ich weiß das ich nichts weiß" ist das Motto der Schwurbeluniversität.

## Ein besonderes Studium

Die Schwurbeluniversität nutzt GPT-3 als einzige Quelle von Wissen. Es gibt keine Übungsleiter, Professoren, Dekane, kein Studiensekretariat und keine Bibliothek. Es gibt keinen Lehrplan und auch keinen Abschluss auf den man hinarbeiten könnte. Selbst ist der Student. An der Schwurbeluniversität übernimmt der Student alle Rollen und deren Aufgaben und vereinigt diese in einer Person.

Ein Studium ist sehr herausfordernd. Obwohl die Flexiblität eines Schwurbelstudiums es ermöglicht, nur einzelne Aspekte eines Themas zu beleuchten und diese in unterschiedlicher Intensität zu beleuchten, ist das Wissen um das eigene Unwissen die wichtigste Voraussetzung, um erfolgreich an der Schwurbeluniversität zu studieren. Was ist damit gemeint?

Angemommen ein Student interessiert sich für ein Studium der Philosophie. Aufgrund fehlender Lehrpläne ist es dem Studenten nicht möglich sich in einzelne Veranstaltungen einzuschreiben. Der Student hat möglicherweise auch sonst keine Kenntnisse darüber, wie das Studium zu beginnen ist. Der Student, der sich darüber im Klaren ist erkennt dies nicht als Nachteil, sondern als Herausforderung.

Der Student erkennt: es gibt keinen Lehrplan oder Kurse die ich belegen könnte. Er übernimmt daraufhin die Rolle eines Dekans und erstellt sich durch Fragen an GPT-3 einen geeigneten Lehrplan selbst. Die erste Frage könnte also lauten: "Wenn ich als Student Philosophie studieren möchte, welche Kurse wären geeignet, um dieses Studium zu beginnen?


## [Institut für Schwurbeforschung](/institut-fuer-schwurbelforschung.md)

Der Stifter dieser Universität ist zugleich Student der Universität und Forscher am Institut für Schwurbeforschung, das zur Aufgabe hat,

- die Vor- und Nachteile eines Schwurbelstudiums zu erforschen,
- die Qualität eines Schwurbelstudiums zu evaluieren
- ein exemplarisches Schwurbelstudium der Philosophie als Hilfsmittel für aktuelle und zukünfigte Studenten der Schwurbeluniversität zur Verfügung zu stellen.

Ein exemplarisches [Schwurbelstudium der Philosophie](/schwurbelu/2023/03/23/philosophy-preparation.html) an der Schwurbeluniversität ist bereit im Aufbau. Dieses exemplarische Studium übernimmt hier die Rolle von Einführungskursen und dem Studiensekretariat traditioneller Universitäten. Es ist darauf ausgerichtet den Studenten einen Einblick in die Philosophie zu geben. Je nach Interesse können einzelne Bereiche durch richtiges Fragenstellen
vertieft werden.

## Aufbau eines Studiums an der Schwurbeluniversität

Ein Studium an der Schwurbeluniversität kann flexibel gestaltet werden.

**What's inquiry?**

Inquiry is the process of **seeking knowledge and understanding through asking questions**, **investigating**, and **exploring** a topic or subject. It involves a **systematic** and **critical** approach to learning, where one engages in a process of **questioning**, **researching**, **analyzing**, and **evaluating** information in order to arrive at a deeper understanding of a topic.

Inquiry can take many forms, including scientific inquiry, philosophical inquiry, historical inquiry, and investigative journalism, among others. **The common thread* among all forms of inquiry **is a curiosity and desire to seek out knowledge and understanding**.

Inquiry involves asking questions and seeking answers through various means, such as conducting experiments, reviewing literature, interviewing experts, and analyzing data. It also involves critically evaluating information and sources in order to assess their reliability and credibility.

Overall, inquiry is a **powerful tool for learning and discovery**, as it allows individuals to explore and deepen their understanding of a subject, and to generate new insights and knowledge.

## (Exemplarische) Studiengänge

Im Aufbau:

- [Philosophie (englisch)](/schwurbelu/2023/03/23/philosophy-preparation.html)

## Hilfreiches

Unabhängig davon, welches Studium an der Schwurbeluniversität begonnen werden soll, empfiehlt es sich den Kurs ["Epistemology"](https://www.hammeley.info/schwurbelu/2023/03/23/introduction-to-epistemology.html) zu bearbeiten. Das Wissen darüber was Wissen bedeutet und was nicht, wird als Grundvorassetzung für jedes Studium angesehen. Das während eines Studiums an der Schwurbeluniversität erarbeitetes Wissen, mag auf den ersten Blick, objektiv, umfassend und neutral Wirken. Es kann sich jedoch durch persönliche Erfahrung, kulturelle Prägung, individuelle Wahrnehmung und Interpretation von Informationen, und ganz besonders wichtig für Studenten der Schwurbeluniversität, durch die Art der gestellten Fragen, deutlich von Student zu Student unterscheiden.

Menschen unterliegen einer Voreingenommenheit. Das kann dazu führen, dass Fragen suggestiv gefragt werden, die GPT-3 zwar richtig beantwortet, das Ergebnis vom Studenten jedoch individuell, zum Beispiel aufgrund fehlender, aber wichtiger Informationen, intepretiert.

Der Kurs ["Epistemology"](https://www.hammeley.info/schwurbelu/2023/03/23/introduction-to-epistemology.html) kann helfen, ein aufgenommenes Studium möglichst objektiv und neutral zu erfahren. Es kann durchaus sinnvoll sein, in einer Diskussion mit GPT-3, eine gegenteilige Position einzunehmen oder Erkenntnisse aus anderen Bereichen in eine Frage einfließen zu lassen, um neue Einblicke zu erhalten.
