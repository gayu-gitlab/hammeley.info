---
title: Contextualism
layout: default
category: schwurbelu
faculty: philosophy
hidden: true
---
# Philosophy

## Epistemology 1.1

### The Nature of Knowledge 1.1.1

#### The Gettier Problem 1.1.1.4

##### Contextualism 1.1.1.4.1

**I want to take a step back and explore the concept on contextualism. I know, thanks to the Gettier problem, that a belief can be true and justified, yet it may not be knowledge. I already have an idea of contextualism. For the purpose of the exploration of this topic I want to try something new. Are you ready?**

Sure, I'm ready! What would you like to try?

**I am going to describe a simple scenario. I want you to tell me, if you have understood the scenario. Then I will ask you a question. You are supposed to come to a conclusion that justifies your anwer to the question. Therefore, you may simulate an initial belief forming a potential answer. The details I provide in the scenario, may not be sufficient for you to justify the initial belief. You are allowed to ask me further questions, in order to get an idea of the context of the scenario. Finally, you are supposed to tell me your answer to my question, provide the justification taking the context of the scenario into account. Do you think that's possible?**

Yes, I think that's possible. Please go ahead and describe the scenario.

**I was working in a large organization. The organization is structured vertically and horizontally. The vertical structure represents the hierarchy of the chain of command. Horizontally, the organization showed a functional organizational structure that reflects the field of activity and expertise. There are departments, unit groups and units, which have been given a short-code. I was working in the unit with the short-code: "V A 3". Have you understood the scenario so far?**

Yes, I have understood the scenario so far. You were working in a large organization that has both a vertical and a horizontal structure. The vertical structure represents the hierarchy of the chain of command, while the horizontal structure reflects the field of activity and expertise, with departments, unit groups, and units that have short codes. You were working in the unit with the short code "V A 3".

**Correct. The question is: "How do you pronouce the symbol V in the short-code of my unit? Do you pronounce it like the letter V, or do you pronounce it like the Roman numeral that stands for the number Five?**

Based on the information you provided, I cannot come to a justified conclusion about how the symbol "V" is pronounced in the short-code of your unit "V A 3". To make a justified conclusion, I would need to ask further questions to clarify the context and conventions of the organization regarding the pronunciation of short-codes. For example, I would ask if the short-codes are typically pronounced as acronyms or spelled out letter by letter, or if there are any specific conventions within the organization regarding the pronunciation of particular symbols. Without more information, I cannot determine a justified answer to the question.

**The symbols in the short-codes are spelled out one by one. I can give you some information on the naming convention of departments, unit groups and groups. The departments are given the short-codes with the symbols: I, II, III, IV, V, and VI. The unit groups in this organization are: either A or B. It seems that the units are given the symbols, 1, 2, 3, 4, 5, 6, or 7.**

Based on this additional information, I would revise my initial belief and answer that the symbol "V" in the short-code "V A 3" is pronounced like the Roman numeral for the number five, and not like the letter "V". This is because the convention for naming departments in the organization involves using Roman numerals, and the short-codes for the units involve using Arabic numerals. Therefore, it seems likely that the "V" in the short-code refers to the Roman numeral, rather than the letter "V". However, if there is any ambiguity or uncertainty regarding this, it may be necessary to consult with others in the organization or seek clarification from superiors to confirm the correct pronunciation.

**I am impressed. The anwer is correct. Now the question I have for you. Was this little question game suitable to explain contextualism in the context finding knowledge or was my idea of the concept wrong?**

Your idea of the concept of contextualism was correct. In this case, contextualism is related to how the meaning of a certain symbol or word can change depending on the context. In the scenario you provided, the pronunciation of the symbol "V" depended on the context of the organizational short-codes, and it was necessary to ask for more information in order to arrive at the correct answer. This shows how contextual factors can influence our beliefs and knowledge, and how they need to be taken into account in order to reach a justified conclusion.

**Thank you very much. Is there anything else I need to know about contextualism?**

Contextualism is a complex and nuanced philosophical theory, so there's always more to learn and explore. However, I can give you a brief summary of some of the key ideas and debates within contextualism.

One of the main features of contextualism is the idea that the truth conditions for knowledge attributions vary depending on the context of the attributor and the attributions. This means that whether or not a belief counts as knowledge may depend on factors such as the stakes involved, the salience of alternative possibilities, the evidence available, and so on.

Contextualism can be contrasted with other theories of knowledge such as **invariantism**, which holds that the truth conditions for knowledge attributions are fixed and do not vary with context. There are also other positions in between these two extremes, such as **moderate contextualism**.

One of the challenges facing contextualism is the problem of accounting for how **context affects the truth conditions** for knowledge attributions. Another challenge is providing a principled account of how to identify and distinguish contexts that are relevant for knowledge attributions.

Overall, contextualism is an interesting and ongoing area of debate in epistemology, with many different approaches and variations being explored.
