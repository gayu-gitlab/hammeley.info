---
title: Jahresrückblick 2022
category: ego
layout: default
---

Jeder, der was auf sich hält, führt einen Jahresrückblick durch. Da will ich natürlich in nichts nachstehen und präsentiere auch einen Jahresrückblick.

#### Aus dem Leben

Den erhöhten Fleischkonsum aus dem letzten Jahr konnte ich dieses Jahr noch erhöhen. Nach den wiederholten Aussagen der Grünen und anderer Interessensverbänden, dass Luxusartikel wie [Lebensmittel viel zu günstig](https://www.derwesten.de/politik/cem-oezdemir-gruene-lebensmittel-teuer-preise-kritik-ramschpreise-supermarkt-id234180121.html) seien, war ein Fleisch-vorfressen alternativlos. Als cleverer und solidarischer Bürg:ender des [besten Deutschlands aller Zeiten](https://www.rnd.de/politik/steinmeier-wir-leben-im-besten-deutschland-das-es-jemals-gegeben-hat-79e657f9-a7b2-4fc8-b330-3a6eddfd5622.html), fand ich einen Weg meinen Plan zu realisieren, ohne den Bildungskonsumenten, die sich in den Bioläden eindecken, etwas wegzuessen. Sozialchichtstypisch ernährte ich mich überwiegend von "Discounter"-Fleisch und Wurst. Ich konnte dadurch auch mein Mitgefühl mit den in Tierwohl-Haltung Klasse 1 und 2 aufgewachsenen Tieren zeigen, indem ich sie durch eine rasche Schlachtung und einer persönlichen Verdauungsmaßnahme früh von Ihrem Leid befreit habe. Finanziell wurden aber auch die notleidenden Bauern durch meine passgenaue Transferleistung unterstützt.

Der erhöhte Sozialschicht typische Billigfleischkonsum riss natürlich ein großes Loch in mein eingeschränktes Budget. Den erhöhten Finanzbedarf konnte ich jedoch durch die Wahl einer alternativen, CO2-neutralen und darüber hinaus auch günstigeren Stromversorgung teilweise kompensieren (im vgl. zu "Natur- bzw. Ökostrom"). Nach vielen Jahren nutze ich nun wieder Elektrizität, die u. a. aus nuklearem Spaltmaterial gewonnen wird. Dank der vorausschauenden Politik im Lande brauchte ich dafür nicht einmal aktiv werden, da der bisherige solidarische Energieversorger von sich aus meinen Vertrag eigenseitig kündigte.

Laut bisher kaum beachteten Ankündigungen aus der Politik wird die Elektrizitätsgewinnung aus Atomkraftwerken wohl bis Ende 2024 gesichert, sodass auch mein zukünftiger Fleischkonsum erst einmal gesichert scheint.

Nach 15 Jahren erhalte ich meine zweite Mieterhöhung. Nachdem die Mietnebenkosten vor einigen Jahren wegen der gestiegenen Grundsteuer angehoben wurden, wurden die Betriebsnebenkosten und die Kosten für die Wärmeversorgung nun um ca. 59 % erhöht (u. a. durch die um fast 30 % gestiegenen Netzentgelte).
Durch diese Maßnahme wird auch mir als passivproduktiver (und ungeimpfter) Sozialschädling die Möglichkeit eröffnet, einen Beitrag zur Steigerung des Bruttoinlandsproduktes leisten zu können. Die finanz-, umwelt- und sozialpolitischen Maßnahmen zeigen aber auch, wie schamlos sich hier das (nicht-jüdische) Großkapital des Landes (hier Land Berlin) am verarmten Staatsfeind wie mir bereichert.

Mensch, würde ich mich ärgern, wenn ich noch arbeiten dürfte und das alles aus der eigenen Tasche zahlen müsste.

Ich beende Korrespondenz mit Behörden und Ämtern nun mit der inklusiven und geschlechtsneutralen Grußformel "Für Deutschland". Nach anfänglicher Schnappatmung der Adressaten kann zum Ende des Jahres die Etablierung und Akzeptanz dieser dem Zeitgeist entsprechenden Grußformel festgestellt werden.

#### Aus dem Bezirk

Die von mir antizipierten Preissteigerungen des Jahres förderten im Kiez neuartige Nachbarschaftsorganisationen. Insbesondere die hart arbeitende, aber naiv-grüne Bevölkerung des Bezirks trifft sich nun in digitalen und analogen Diskussionsräumen mit dem Ziel, sich wegen allgemeiner Preissteigerungen und den gestiegenen Mietnebenkosten zu organisieren.
Die idealistischen Versag:enden der naiv-grünen Gentrifizierer (der unteren Mittelschicht) der frühen 1990 und 2000er-Jahre verlassen, nachdem sie ihre Schuldigkeit zur Stabilisierung des Kiezes (ehemals QM) getan haben, nun vermehrt diesen und werden durch multikulturellen Zuzug, der mit einer gemütlicheren, also engeren Wohnraumsituation weniger Probleme hat, ersetzt.

Entgegen vieler Klagen der Zwischen-Gentrifizierer über zu hohe Mieten ist es jedoch eine Tatsache, dass Wohnraum viel zu günstig ist. Laut den grünen Vorfeldorganisationen wie Fridays for Future sind Mehrfachbelegungen und [kleinere Wohnungen](https://www.faz.net/aktuell/wirtschaft/klima-nachhaltigkeit/fridays-for-future-fordern-kleinere-wohnungen-16999975.html) darüber hinaus viel umweltverträglicher.

Nachdem im Jahr 2021 öffentliche Plätze noch von Landes- und Bundespolizei geräumt und nach unvermummten Verfassungsfeinden (auch Maskenverweigerer genannt) durchsucht wurden (teils mit sichtbar getragenen Maschinenpistolen am Bahnhof Ostkreuz) ist nun nach jahrelanger Planung der Boxhagener Platz zur ["Zero Waste Zone"](https://www.berlin.de/ba-friedrichshain-kreuzberg/politik-und-verwaltung/beauftragte/entwicklungspolitik-und-nachhaltigkeit/artikel.892709.php) geworden.
Dank großer Transparente und [knackiger Anglizismen](https://www.berliner-woche.de/friedrichshain-kreuzberg/c-umwelt/bezirk-setzt-auf-mehrweg_a353240), die nun den Platz schmücken, ist der Platz und die nähere Umgebung nun so sauber, dass sich selbst Oberschichts-Münchner im Bezirk wohlfühlen könnten, welche durch Zuzug die kommumale Finanzsituation auf der Einnahmenseite verbessern könnten.

Etwas verwundert es mich doch, dass gerade in Bezirken mit der größten Anzahl an Umweltschützern der meiste Müll auf den Straßen liegt. Selbst ich als Umweltnazi schaffe es nahezu immer, Müll zu vermeiden oder ihn in einen der zahlreichen Müllbehälter zu bringen. Auch lasse ich keine Glasflaschen oder Kronkorken fallen und bestehe auch nicht auf mein Recht, Zigaretten auf den Boden werfen oder in ein Blumenbeet drücken zu dürfen.

Wahrscheinlich bin ich als in Berlin Geborener dafür einfach nicht alternativ genug.

#### Aus Deutschland

Der amtierende Finanzminister und Magier Christian Lindner heiratete seine aus ärmlichen Verhältnissen stammende und im Medienbereich ausgebeutete Freundin auf einer in Deutschland noch nie gesehenen Hochzeitsfeier mit zahlreichen Gästen aus dem politischen Jetset. Während die quäkenden Versager der Mittelschicht von einem Realitätsverlust sprechen, ist deren Reaktion in Wirklichkeit nur eine typisch deutsche Neiddebatte.
Selbstverständlich dürfen Menschen und Götter, die in Deutschland etwas leisten, sich auch was leisten. Insbesondere dann, wenn sie ihr Gehalt durch freie und geheime Wahlen legitimieren.

Dass Herr Lindner ein außergewöhnlicher Leistungsträger ist, erkennt man schon daran, dass er es geschafft hat, ca. 500 Mrd. Euro aufzutreiben, ohne die Neuverschuldung nennenswert erhöhen zu müssen - und das, ohne die zukünftigen Generationen von Bürg:enden finanziell zu belasten zu müssen. Da wäre ein wenig Dankbarkeit doch durchaus angebracht.

Mich würde es nicht wundern, wenn der schöne Christian dank seiner finanzpolitischen Expertenkenntnisse im Laufe seiner Amtszeit sogar Rekordsteuereinnahmen verkünden könnte.

Etwas neidisch sind jedoch auch die Steuer- und Abgabenempfänger im Bundestag um den schönen Christian Lindner, wenn sie erfahren, dass Leistungsträger der öffentlichrechtlichen Rundfunkanstalten ein höheres Salär als sie selbst oder der Bundeskanzler erhalten. Das geht nun gar nicht. Um das Defizit an Gerechtigkeit wieder herzustellen und verwaltungstechnische Probleme zu vermeiden, wird anstelle von Gehaltskürzungen bei den Öffentlich Rechtlichen über eine Erhöhung der Diäten im Bundestag nachgedacht.

Der rbb macht endlich auch mal Schlagzeilen, nämlich dank seiner Intendier:enden Schlesinger. Diese hat sich gesetzeskonform "bereichert" und erhält dafür eine große Abfindung in Form einer fetten Pension. Genau so, wie es das Gesetz vorsieht. Der Aufschrei ist natürlich wieder deutsch-typisch, neidgetrieben.

Die Sozialdemokraten übertreffen sich wieder einmal selbst. Durch geschicktes Ändern des juristisch irrelevanten Trivialnamens für Leistungen zum Lebensunterhalt bisher "Hartz IV", nun "Bürgergeld" genannt, wird das soziale Profil der Partei deutlich geschärft. Durch diesen Coup werden größere, zeitaufwändige Änderungen der Gesetzeslage unnötig. Die größte Sozialreform der letzten 20 Jahre kommt ohne große Revisionen der Sozialgesetzbücher I - XIV aus.

- nominale Erhöhung von Leistungen um bis zu 53,00€
- Mindestlohnempfänger und Arbeitslose dürfen nun ein Vermögen von ganzen 15.000€ ansparen (bis zur Rente)
- die vermögende Boomergeneration, die nun von Arbeitslosigkeit bedroht ist, darf sich dank Karenzzeit in die Frührente retten

Leider wurde die Bildungspolitik durch die Vorgängerregierungen, u. a. mit Beteiligung der Sozialdemokraten sträflich vernachlässigt. So konnte die SPD in Wahlumfragen kaum von ihrer Meisterleistung profitieren. Die durch [Doppelwumms](https://www.spd.de/aktuelles/detail/news/doppel-wumms-fuer-bezahlbare-energie/29/09/2022/), [Winter- und Schneeketten](https://www.bundesregierung.de/breg-de/themen/coronavirus/infektionsschutzgesetz-2068856) verwöhnten Bürg:enden sind einfach zu dumm.

#### Aus der Welt

Vladimir Putin, freiberuflicher Agent der Grünen, setzt das lang erhoffte Wahlprogramm der Partei um. Insbesondere die von der Partei gewünschten Preissteigerungen bei Luxusgütern wie Strom, Wasser, Wärme und Lebensmittel konnten durch den russischen Staatspräsidenten zeitnah nach Amtsantritt der Bundesregierung im November 2021 realisiert werden.

Die hochgelobte und tief fallende Aussenministrierende und Vorsitzende der Friedenspartei A. C. A. Baerbock, äußert sich cis-staatsfrauisch und dementiert die förderliche Amtshilfe aus Russland. Sie fordert stattdessen mit ihrem langhaarigen Kindbesitz- und präsentier:enden, aber ausgemusterten und ungedienten Kollegen Hofreiter mehr Waffenexporte Deutschlands in Krisen- und Kriegsregionen. Ganz dem Motto "An deutschen Waffen soll die Welt genesen."

~~Lauterbach führt sei~~

#### Empfehlung

Dieser Jahresrückblick ist natürlich durch meine staasfeindliche, delegitimierende Art und westliche Sozialisation geprägt. Wer einen Jahresrückblick von einem echten Ost-Berliner und Schlagzeuger aus der professionellen (Nazi-Bild)-Journalie sehen möchte, dem empfehle ich ["Schuler! Fragen, was ist"](https://www.youtube.com/embed/uKRmagLiCUc), den Jahresrückblick von Ralf Schuler mit Wolfgang Kubicki.

<iframe width="560" height="315" src="https://www.youtube.com/embed/uKRmagLiCUc" title="Klartext-Kubicki: Reichsbürger-Razzia gegen „Mickey-Mouse-Truppe" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ralf Schuler bei [Wikipedia](https://de.wikipedia.org/wiki/Ralf_Schuler) und [twitter](https://twitter.com/drumheadberlin).

**P.S.**

Gemäß den [Empfehlungen aus dem Gesundheitsministerium](https://www.deutschlandfunk.de/lauterbach-haelt-ein-glas-wein-oder-bier-in-der-regel-fuer-gesund-und-warnt-vor-zu-viel-fleisch-kons-102.html) begann ich dieses Jahr damit morgens ein Glas Wein und abends eine Flasche Wein zu trinken. Ich empfehle Wein aus dem [Markgräflerland](https://de.wikipedia.org/wiki/Markgr%C3%A4flerland#Weinanbau) und der Region um Sulzburg / Laufen.
