---
title: "Ein Querdenker, der nur seinem Gewissen gefolgt ist."
category: "phantasia"
layout: default
---

## Neueste Meldungen aus Phantasia

(manchmal sogar der Zeit voraus)

### Querdenker Ströbele jetzt bei Gott

Der RAF-Anwalt und wegen Unterstützung einer kriminellen Vereinigung verurteile Grünen-Politikbetreib:ende Hans-Christian Ströbele ist am 31.8.2022 ist gestorben.

Ein von Russen infiltierter Redakteur der Online-Redaktion tagesschau.de schreibt unter anderem: "ein Querdenker, der nur seinem Gewissen gefolgt ist".
Der unkorumpierbare, immer ehrliche, aufrichtige Staatsfunk und unabhängige Korrektiv hat durch das schnelle Eingreifen einen großen Schaden vom Deutschen Volk abgewendet  Es heißt nun richtig: "Hans-Christian Ströbele - einer, der nur seinem Gewissen gefolgt ist".

### Baerbock Opfer von falscher Propaganda geworden

In einer außerordentlich guten sechsminütigen Rede der besten Außenminister:in aller Zeiten am 10.9.2022, die A.C.A.B. mit nur fünf Versprechern und zwölf Wortneuschöpfungen erfolgreich abschloss, erkennt die vom Völkerlinks kommende eloquente Frau, dass "wir [..] zum Glück in einem freien Land [leben]". Ein tosender Applaus füllte das Hohe Haus für ihre Aufforderung es dem "russischen Regiehm" nicht zu einfach zu machen. "Falsche Propaganda" (Feldherr:in Annalena Baerbock, 10.9.2022) müsse unbedingt durch richtige Propaganda ersetzt werden. Zu den "prioritäre Priotäten" gehöre es jetzt, den Volkskörper zusammenzuhalten egal was die Wähl:enden darüber denken.

Das Ausspielen von bedürftigen Gruppen sei zynisch bemerkte die Menschenfreund:in, unterstützung sollten jedoch nur Bevölkernde erhalten, die auf Parteilinie sind.

### Falsche Sonntagsfrage veröffentlicht

Das Umfrageinstitut infratest veröffentlichte am 15.9.2022 die Falschmeldung, die SPD würde nur noch 17% der Wählerstimmen, und damit nur 3% mehr als die rechtsextremistische Alternative für Deutschland erhalten. Auch falsch ist die Behauptung, dass die Koalition nach weniger als einem Jahr, nur noch 45% der Währerstimmung auf sich vereinigen kann. Ähnliche Meldungen veröffentlichten in den letzten Wochen auch andere bekannte Umfrageinstitute.

Eine Unterwanderung der Umfrageinstitute ist möglicherweise von Russen unterwandert. An schlechter Politik kann es nicht liegen, sind sich alle Regier:enden einig.

Olaf Scholz soll bereits dabei sein eine Sondertruppe "Wahlumfragenfälschung" einzurichten. Nach den erfolgreichen Wahlkorrekturen in Berlin 2021 soll das Volk nicht durch zweideutige Umfragen verunsichert werden. Zudem sollten nur noch vom Staat organisierte Demonstrationen stattfinden, damit Staats- und Verfassungsfeinden keine Bühne gegeben wird. "Wer gegen jahrelange Kurzarbeit, Arbeitslosigkeit, Insolvenzen, die Gasumlage, oder wegen 500% steigenden Strom- und Gaspreisen und gegen die seit drei Jahren herrschende Corona-Plandemie demonstriert ist ein Staatsfeind. Dazu zählen auch die als Rentner- und Kinder getarten Terroristen. Die Polizei muss nun mit aller Härte gegen diese Zersetzer vorgehen. Frau Faeser arbeitet zum Glück bereits wieder daran den Überwachungsstaat weiter auszubauen. Auch ein Mauerbau um Deutschland herum, um uns gegen das faschistische Ausland zu schützen wäre denkbar. Bin ja 1984 nicht ohne Grund in die DDR gereist. Ich habe damals viel gelernt, wie man das umsetzen tut."

### Berlin liefert

Die beiden tollen Frauen aus Berlin, Giffey und Jarasch verkünden stolz einen Nachfolg:enden für das beliebte 9€-Ticket, das in Deutschland unbeschränktes Reisen im Regionalverkehr ermöglichte.

Kritische Stimmmen, dass es sich lediglich um eine auf drei Monate befristete Vergünstigung der Abonnements wie Umweltkarte Tarifbereichen A und B der BVG handele (keine Einzelmonatstickets) und das eingeräummte Sonderkündigungsrecht zum 31.12.2022 auch schon wegen der gleichzeitig bekanntgemachten Erhöhung der Beförderungspreise gelte, lassen die Frauen nicht gelten: "Wenn wir sagen, Berlin liefert, dann ist das verdammt noch mal so zu akzeptzieren. Schauen sie doch mal wie fröhlich wir in die Kameras grinsen. Die Berliner haben jetzt froh zu sein und außerdem, nur mit dem Abomodell können wir Bundesmittel aus Bayern und Baden-Württemberg abgreifen."

Auch dass ca. 440.000 Leistungsbezieher in der Stadt nicht profitieren und Pendler (Tarifzonen B, C, und A, B, C) unter umständen schlechter gestellt werden, wenn sie zwar nun ein vergünstigtes AB-Abo kauften, aber dafür ein teuren Anschlussfahrschein für den Tarifbereich C benötigten, wäre russische Propaganda. "Wenn sich das 29,00€ Ticket nicht als Erfolg herausstellt, ist die SPD in Brandenburg, oder der Russe schuld, das ist mir eigentlich Wurst wer dafür dann die Verantwortung übernimmt. Bin ja eh nicht durch korrekte Wahlen legitimiert." Jarasch fügt hinzu: "Das 29-Euro-Ticket sei von den Menschen in Berlin gewünscht worden.", also ist das so.

Die Maskentragenden Besserverdiener der Grünen freuen sich: "Das eingesparrte Geld kann ich gut für meinen Maskenkonsum gebrauchen. Danke liebe Frau Jarasch"

**Quellen:**

- [durch den Sender Phoenix manipulierte Rede der Völkerlinker:in](https://youtu.be/7lwKzMgVKXY?t=252)
- [Originale Falschmeldung auf tagesschau.de im Internetarchiv](https://web.archive.org/web/20220831115424/https://www.tagesschau.de/inland/nachruf-hans-christian-stroebele-gruene-101.html)
