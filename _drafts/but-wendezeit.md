---
title: Bild und Ton der Wendezeit
layout: playlist
permalink: /bild-und-ton/wendezeit
era-short: wendezeit
---
{% assign era = site.data.era | find: 'era-short', page.era-short %}
**Hinweis: Die folgenden Videos können Spuren von Müllfahrerwesten, freier Liebe und Optimismus enthalten.**
