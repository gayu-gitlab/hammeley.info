---
title: I've seen the realness
layout: default
category: music 
---
All I know  
Is everything is not as it's sold  
But the more I grow the less I know  
And I have lived so many lives  
Though I'm not old  
And the more I see, the less I grow  
The fewer the seeds the more I sow  

Then I see you standing there  
Wanting more from me  
And all I can do is try  
Then I see you standing there  
Wanting more from me  
And all I can do is try, try  

I wish I hadn't seen all of the realness  
And all the real people are really not real at all  
The more I learn the more I learn  
The more I cry the more I cry  
As I say goodbye to the way of life  
I thought I had designed for me  

Then I see you standing there  
Wanting more from me  
And all I can do is try  
Then I see you standing there  
I'm all I'll ever be  
But all I can do is try  
Oh, try, try  

All of the moments that already passed  
We'll try to go back and make them last  
All of the things we want each other to be  
We never will be, we never will be  
And that's wonderful, and that's life  
And that's you, baby  
This is me, baby  
And we are, we are, we are, we are  
We are, we are  
Free  
In our love  
We are free in our love  

<div class="ratio ratio-16x9">
    <lite-youtube videoid="3--1Kw2UHDQ" playlabel="Nelly Furtado - Try" params="controls=1"> </lite-youtube>
</div>
