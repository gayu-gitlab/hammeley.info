---
title: Soziales Berlin
layout: default
category: phantasia
---

**ACHTUNG: Der folgende Beitrag enthält Fake-News, Täter-Opfer-Umkehrungen und es wird ein rechtes Narrativ bedient. Distanzieren Sie sich umgehend von diesem Beitrag!**

1) Schön zu sehen wie sozial unsere tolle Stadt doch ist. Da berichtet sogar die Rechte darüber:

- [Wie Berlin mit viel Geld ein Luxushotel zum Asylheim macht](https://jungefreiheit.de/politik/deutschland/2023/berlin-luxushotel-asylheim/)

2) Derweil stellt die ehemalige Verpackungsmittelmechanikerin, dann Coach für "Zugewanderte" nun leistungstrag:ende Sachenbearbeit:ende Frau van Aart beim Jobcenter, die sich beim letzten Treffen selbst "Verdienende" nennt, noch einmal klar, dass ich unrecht habe und ich freiwillig meine Wohnung geräumt habe.

3) Schulden-Drecksau Saskia Esken möchte einen Tag nachdem das Verfassunggericht entschieden hat dass die Schuldenbremse einzuhalten sei, die Schuldenbremse für zwei Jahre aussetzen. Um das Vorhaben durchzusetzen zählt sie dabei auf die Stimmen der CDU und der AfD.

4) Demjenigen der mich vor Ende des Jahres erschießt, gebe ich Zugang zu meinen Krypto-Wallets. Versprochen.

HEIL!
