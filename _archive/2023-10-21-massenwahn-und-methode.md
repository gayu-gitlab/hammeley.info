---
title: Massenwahn und Methode
layout: default
category: ego
---
# Wir müssen die Versklavungsmechanismen der Moderne durchbrechen

***Wer die Massen kontrolliert, kontrolliert den Lauf der Geschichte. Wie befreit man sich aus einer kollektiven Psychose?***

**von Milosz Matuscheck**

Wenn es eine Lehre aus den letzten drei Jahren der pandemischen Machtergreifung gibt, dann folgende: Es ist möglich, einer Masse von Menschen die Vorstellung einer neuen Realität zu vermitteln und diese als so verbindlich erscheinen zu lassen, dass die Verblendeten sogar bereit sind, die neue Realität bis aufs Blut zu verteidigen. Das gelingt, wenn man Menschen, wie Plastelin, zu einer Masse formt. Der belgische Psychologe und Universitätsprofessor Mathias Desmet hat die Theorien Gustave Le Bons auf die Corona-Zeit angewandt und festgestellt: Die „Massenformation“ funktioniert unabhängig vom Bildungs- oder Zivilisationsgrad. Sie ist ein Hack des geistigen menschlichen Programms. Das Ausnützen einer Schwachstelle. Und diese Schwachstelle lässt sich für verschiedene Themen immer wieder neu ausnutzen.

Den ganzen Artikel gibt es kostenlos [auf Substack](https://substack.com/app-link/post?publication_id=95541&post_id=138161178&utm_source=substack&utm_medium=email&utm_content=share&utm_campaign=email-share&action=share&triggerShare=true&isFreemail=true&r=a0tfx&token=eyJ1c2VyX2lkIjoxNjgzNDMxNywicG9zdF9pZCI6MTM4MTYxMTc4LCJpYXQiOjE2OTc4NzM0MDAsImV4cCI6MTcwMDQ2NTQwMCwiaXNzIjoicHViLTk1NTQxIiwic3ViIjoicG9zdC1yZWFjdGlvbiJ9.ulMMTHON3-2rxImNImBRcM0RzC2M5bt-eao5GPAQrA0)

