---
title: Das sagenumwobene Asylantis
layout: default
category: phantasia
---
Die wahrheitsvoraussagenden Meinungsmacherinstitute prognostizieren einen weiteren Aufschwung der AfD von derzeit 18 % auf mind. 23 %. Es besteht die Chance, so die Institute, dass sich die AfD vor SPD und der Die Grün:enden als zweitstärkste politische Macht etablieren könnte.

Die in den Medien zurecht gephotoshoppte feministische Außenministrier:ende A. C. A. Baerbonski möchte deshalb Lager in Drittstaaten etablieren, in denen Asylanten konzentriert aufbewahrt werden können, bis über ihren Aufenthaltsstatus entschieden ist. Sie erhofft sich davon verlorene Stimmen ihrer Stammwähler von der AfD zurückgewinnen zu können. Die Meinungsinstitute gehen jedoch davon aus, dass die Enttäuschten einmal zu häufig verarscht wurden, und deshalb dauerhaft ihre Stimme der AfD geben werden.

Ganz wichtig für Anni Quack-Quack-Baerbock ist die Einhaltung von Menschenrechtstandards. Eine würdevolle Unterbringung der zukünftigen Fachkräfte ist damit jedoch nicht gemeint. Die schöne Trampolinspringerin träumt von riesen großen und vor allem bunten Zeltstädten. Da von mehreren Millionen Personer auszugehen ist, die jedes Jahr einreisen wollen, um Prinzessin Anni begrüßen zu können, werden die einzelnen Zeltlager ganz schön riesig werden. 

An dieser Stelle möchte ich als dichter Denker die mülltrennende aber gleichmachende Leni Riefenbock geistig etwas unterstützen. Ich präsentiere folgend ein bis ins Detail ausgearbeitetes Konzept, das bei einer Resort übergreifenden Implementierung alle Probleme auf einmal löst. Die Endlösung sichert denb deutschen Wohlstand auf Jahrstausende und integriert die Asylanten menschenwürdig und volksschonend in die Deutsche Leitkultur.
<!-- more -->
### Das sagenumwobene Asylantis

#### Orte

Anstatt sich ausschließlich auf EU-Drittstaaten an den EU-Außengrenzen zu konzentrierten, in denen Aufnahmelager errichtet werden sollen, können Ayslantis-Städte weltweit in Partnerstaaten etabliert werden. 

Das primäre Ziel der Atlantis-Städte ist die Reduzierung des Risikos, dass die zukünftigen Fachkräfte auf ihrer teils schweren Reise ums Leben kommen, oder geschwächt ankommen, bevor sie vom deutschen Arbeitsmarkt verwertet werden können.

Prinzipiell eignen sich alle Standorte aus denen ein erhöhter Migrationsdruck zu erwarten ist. Hier pachtet oder erwirbt das Deutsche Volk einen geeigneten Landstrich in Erbpacht. Auf dem so erworbenen Landstrich werden Städte mit einer Kapazität von 50 - 100.000 Asylantier:enden (ehemals Asylantier) erbaut. Der Standard der Asylantis-Städte bewegt sich etwas über dem der Siedlungsländer, jedoch zwingend nach DIN-Normen.

Neben Ländern wie Syrien oder Türkei, bieten sich auch die Ukraine, zahlreiche Staaten in Afrika in Südamerika oder auch Kiautschou in China an.

Asylantis steht jedem Bürg:enden des Universums zur Einreise und zum Aufenthalt offen. Jeder registrierter Asylantier hat ein Aufenthalts- und Arbeitsrecht in Asylantis. Der Zuzug von Migranten in die Asylantis-Städte ist auf die jeweilige Kapazität beschränkt. Ein Umsiedlungsbüro am Toreingang zu Asylantis bietet Informationen zu alternativen Asylantis-Städten. Eine Voranmeldung zur Einreise-Willigkeit kann hier vorgenommen werden. Die Reise muss durch Eigenmittel bewerkstelligt werden. 

Da auch Deutsche Bürg:enden Bürg:enden des Universums sind, steht natürlich der Einreise Deutscher nach Asylantis, nichts entgegen, sofern die Kapazitätsgrenzen noch nicht überschritten sind.

#### Verwaltung und Recht

In Asylantis wird eine deutsche Verwaltung aufgebaut, mindestens jedoch ein Asylamt, das außerhalb des Diplomatischen Dienstes im Wirtschaftsministerium untergebracht ist. 

Um Asylantier schonend auf ihr Leben in Deutschland vorzubereiten, gilt hier überwiegend das deutsche Recht, z. B. das rechts fahren, Steuern, G.E.Z.-Gebühren, Krankenkasse, Sprache, Schulsystem usw.

Ist das Siedlungsland in einigen Fragen fortschrittlicher, wird das Recht des Staates angenommen. Ist z. B. eine 14-jährige im Gastland bereits heiratsfähig, so gilt dies auch in Asylantis.

#### Infrakstruktur

Die Musterstädte werden mit menschenwürdiger Infrastruktur, leicht über dem Standard des Siedlungsstaates ausgestattet. Dazu gehören Verwaltung, Gesundheit, Bildung, Wohn- und Einkaufsmöglichkeiten usw. Der Zuzug in die Musterstädte steht für jeden Menschenbürg:enden offen. 

Der Aufbau wird durch deutsche Unternehmen realisiert.

Kinder können hier zur Schule gehen und die deutsche Sprache lernen. Eltern können bei Integrationskursen auf ihre Zeit in Deutschland vorbereitet werden.

Um der Digitalisierung einen Sscub zu geben, kann Asylantis auch als Treiber für Innovation dienen. Um den umweltschädlichen Plastikbedarf für die Identitätskarten der Asylantier zu vermeiden, werden RFID-Chips aus umweltschonenden Edelmetallen unter der Haut der Asylantier bei Ankunft implantiert. 

In Asylantis wird mit einer auf der Blockchain basierenden Asylimark bezahlt. Diese wird zu Beginn an den Euro gebunden. 

Mit dem RFID-Chip kann auch sehr einfach und hygienisch bezahlt werden.

#### Wirtschaft 

Deutsche Unternehmen, die Waren und Dienstleistungen nach Asylantis exportieren und in Asyli-Mark bezahlt werden, können jederzeit gegen Euro tauschen. Personer, die in Asylantis einer Arbeit nachgehen und z. B. für ein deutsches Unternehmen Remote im Home-Office arbeiten, werden in Asyli-Mark bezahlt, ebenso werden Steuern in Asyli-Mark beglichen.

Zwischen Asylantis und den Siedlungsländern besteht freier Waren-, Dienstleistungs-, Personen- und Geldverkehr. Damit wird sichergestellt, dass sowohl die Siedlungsländer, aber auch Asylantis-Städte wirtschaftlich von dern deutschen Asylfrage profitieren können. Ggf. kann je nach Region von diesem Grundsatz abgewichen werden.

#### Kostenausgleich

Das Siedlungsland wird für die Erbpacht finanziell entschädigt. Die Höhe der Pacht richtig sich nach der Wirtschaftsleistung der Asylantis-Städte. 

Deutsche Unternehmen, die in Atlantis investieren, den Aufbau und den Unterhalt der Stadt organisieren, werden über Asylantis-Steuern entschädigt.

Für die Gastfreundschaft des Deutschen Volkes wird in Asylantis ein Solidarzuschlag erhoben und für den Unterhalt der Mark Deutschland (deutsches Kerngebiet in Mitteleuropa) verwendet.

#### Fachkräfte-Emigration

Es bietet sich an, deutsche Fachkräfte in Deutschland, die über Sprachkenntnisse der Siedlungsländer und der dort überwiend vorzufindenen Asylantier anzuwerben, um vor Ort den Ausbau und die Verwaltung der Asylantis-Städte zu vereinfachen.

Um die Diversität und die feministische Politik Deutschlands sicherzustellen, werden dauerhaft Feministinnen, Tunten und Umweltschütz:enden nach Asylantis entsandt. Hier können sie ihre Expertise vor Ort unter Beweis stellen, und die zukünftig Migrierten indoktrinieren.

Ebenso braucht es in Asylantis eine große Anzahl an geschulten Integrationsfachkräften, die bereits in Deutschland dieser Arbeit nachgehen.

Auch private Sicherheitskräfte aus Deutschland werden wahrscheinlich benötigt.

#### Migration nach Deutschland

Sobald die migrationswilligen Personer und alle ihre Kinder über eine für den deutschen Arbeitsmarkt nützliche Fachausbildung und eine Zusage über einen Arbeitsplatz verfügen, kann mit dem Beginn des Asylantrags begonnen. Bei einem positiven Bescheid können diese Personer nach Deutschland zur Arbeitsaufnahme einreisen. Wenn es ihre Qualifikation erlaubt können sie Asylantis verbleiben und dort ihren genehmigten Asylaufenthalt verbringen. 

Da in Asylantis deutsches Recht und deutsches Bier getrunken wird, die Infrastruktur modern und zukunftsweisend ist, werden sich zahlreiche Asylantier entscheiden in ihrer bekannten Umgebung Asylantis zu verbleiben. 

Zur endgültigen Einreisebewilligung wird die Integrationswilligkeit geprüft. Obwohl sich die Asylantis den Städten im deutschen Kernland ähneln, kann es durch unterschiedliche Rechtssprechung (z. B. heiratsfähige 14-jährige) unterscheiden. Wird die Bekenntnis nicht abgegeben oder bestehen zweifel, darf der Asylantier in Asylantis verbleiben.

Geeignete Videokonferenzen, Deutsche Sprache und Deutsches Staatsfernsehen könnte Asylantiern zusätzlich das Gefühl geben, sie seien bereits im Zielland angekommen. Den meisten geht es ja sicherlich um sichere und gute Lebensverhältnisse. Diese dürften nach diesem Konzept in Asylantis gewährleistet sein.

#### Zukunfft

Nach einer erfolgreichen Erprobung einer einzelnen Asylantis-Städt können weltweit zahlreiche Asylantis-Städte erbaut werden, so dass Deutschland über die feministische Migrationsendlösung potentiell (derzeit) ca. 8 Mrd. Menschen integrieren könnte.

Übertrifft die Population der Asylantier in einem Siedlungsland die Population des Gastlandes wird der verbleibene Rumpfstaat nach diesem Konzept in ein Asylantis-Land umgeformt.

Sobald alle Staaten dieser Welt als deutsche Asylantis-Länder eingegliedert wurden, müsste überall auf der Welt ein angeglichener Lebensstandard herrschen, die Weltsprache deutsch gesprochen werden. 

Da nun alle Bürg:enden dieser Welt vertuntete Umweltschütz:enden geworden sind, ist die Welt gerettet. Es ist von einem dauerhaften Weltfrieden auszugehen.

Der in Deutschland eingeführte Euro wird zu Gunsten der weltweit eingeführten Asyli-Mark auch in Deutschland eingeführt.


