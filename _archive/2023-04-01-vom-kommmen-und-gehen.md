---
title: Vom Kommen und Gehen
layout: default
category: ego
---

<div class="ratio ratio-16x9">
	<hmmly-ctrl id="genug" videoid="_P2cc1fH7Jw" playlist="gk" playlabel="Habe ich genug getan?" params="controls=1"></hmmly-ctrl>
</div>

Weitere Empfehlungen: [Ich bin so enttäuscht](#){:.vid-link id="genug" videoid="I28GKP3iSRs" playlabel="Ich bin so enttäuscht"} \| [Was, wenn sie das nochmal machen?](#){:.vid-link id="genug" videoid="Ixgf1KFIeKs" playlabel="Was, wenn sie das nochmal machen?"} \| [Wie wäre es, selber zu denken?](#){:.vid-link id="genug" videoid="TUUEv6mcaXw" playlabel="Wie wäre es, selber zu denken?"} \| [Meine Message an Covidioten](#){:.vid-link id="genug" videoid="_V0CatsytJk" playlabel="Meine Message an Covidioten"} \| [Minderwertigkeitskomplex und Mut](#){:.vid-link id="genug" videoid="QnXrDbt0Qwc" playlabel="Minderwertigkeitskomplex und Mut"} \| [Was wir wollen - Und was wir kriegen](#){:.vid-link id="genug" videoid="ry5oJRUfKdc" playlabel="Was wir wollen - Und was wir kriegen"} \| [Alles ist so kaputt](#){:.vid-link id="genug" videoid="gonwZ5s3ccA" playlabel="lles ist so kaputt"}
