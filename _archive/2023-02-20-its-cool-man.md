---
title: It's cool man
category: music
layout: default
---

Ganz anders als die sonstigen Frankfurter, wie z. B. Pascal FEOS (\*1968 +2020), war Pete Namlook (\*1960 +2012) einer der ruhigeren Chillout & Ambient-Produkzenten der 1990er Jahre.

Das folgende Album Ambient Gardener : Autumn dürfte eines der letzten veröffentlichen Alben (zusammen mit Spring, Summer und Winter) gewesen sein, das am 2. April 2012 auf Fax-49-69450464 erschien.

<div class="ratio ratio-16x9">
    <lite-youtube videoid="yeQ4nlPbqA4" playlabel="Pete Namlook - Ambient Gardener : Autumn [full album]" params="controls=1"> </lite-youtube>
</div>

Auch wenn das ganze Album toll ist, empfehle ich unbedingt das Stück das in Teilen an Vangelis und Bladerunner errinnert: [4Voice - New York, 25.11.2089](https://youtu.be/yeQ4nlPbqA4?t=4068).
