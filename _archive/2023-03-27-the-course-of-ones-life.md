---
title: The Course of one's Life
layout: default
category: music
---

I stand firm for our soil (a source)  
Lick a rock on foil (nigga I'll come forth)  
Say reduce me, seduce me (They juice me, seduce me)  
Dress me up in Stussy  
Hell is round the corner where I shelter  
Ism's and schisms  
we're living helter skelter (been livin' on a study)  
If you believe and (or) deceive common sense says shouldn't receive  
Let me take you down the corridors of my life  
And when you walk, do you walk to your preference?  
No need to answer till I take further evidence  
I seem to need a reference to get residence  
A reference to your preference to say  
I'm a good neighbor, I trudge (shrug)  
So judge me for labor  
The bond on me ensures (lobotomy) my good behavior  
The constant struggle (strum) ensures my insanity  
Passing the ignorance ensures the struggle for my family  
We're hungry beware of our appetite  
Distant drums bring the news of a kill tonight  
The kill which I share with my passengers  
We take our fill, take our fill, take our fill  

I stand firm for our soil (a source)  
Lick a rock on foil (nigga I'll come forth)  
Say reduce me, seduce me (They juice me, seduce me)  
Dress me up in Stussy  

Confused by different memories  
Details of Asian remedies
Conversations, of what's become of enemies  
My brain thinks bomb-like  
So I listen he's a calm type  
And as I grow, I grow collective  
Before the move sit on the perspective  
Mr. Quaye (Mr. Kray) lay in the crevice (distant cradle in the crevice)  
And watches from the precipice  
Imperial passage  
Heat from the sun some days slowly passes  
Until then, you have to live with yourself
Until then, you have to live with yourself

I stand firm for our soil (a source)  
Lick a rock on foil (nigga I'll come forth)  
Say reduce me, seduce me (They juice me, seduce me)  
Dress me up in Stussy  
Hell is round the corner where I shelter  
Ism's and schisms  
we're living helter skelter (been livin' on a study)  
If you believe and deceive common sense says shouldn't receive  
Let me take you down the corridor  

My brain thinks bomb-like  
bomb-like  
My brain thinks bomb-like  
bomb-like, bomb-like  
My brain thinks  
bomb-like  
beware of my appetite  

<div class="ratio ratio-16x9">
    <lite-youtube videoid="E3R_3h6zQEs" playlabel="Tricky - Hell is around the Corner" params="controls=1"> </lite-youtube>
</div>

[Tricky](https://de.wikipedia.org/wiki/Tricky), aus Bristol gilt als Begründer des "Trip Hop". In den 1990er und frühen 2000er Jahren waren Tricky, Massive Attack, Portishead, Morcheeba, Björk die prägestens Künstler, Küntler-Kollektive und Bands des Genres. Aber auch zahlreiche andere Künstler und Bands wie Gus Gus, Hooverphonic, Nightmares on Wax, Kruder und Dorfmeister, Smoke City und viele viele andere waren Künstler dieser Zeit.

Diese Trip-Hop[Kompilation](https://www.youtube.com/watch?v=orWhn96X5b4) mit vielen bekannten Stücken, deren Namen man schon vergessen hat, vermittelt einen guten Überblick über das Genre.
