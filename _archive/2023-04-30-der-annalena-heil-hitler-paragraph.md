---
title: Der Annalena-Hitler-Heil-Paragraph
layout: default
category: phantasia
---

#### Nazisymbole im Justizministerium

Der krebskranke Youtuber [Timm Kellner](https://www.youtube.com/@TimKellner?origin=https://hammeley.info/phantasia/2023/04/30/der-annalena-heil-hitler-paragraph/) soll per Strafbefehl zu einer Haftstrafe von acht Monaten, ausgesetzt zu drei Jahren auf Bewährung, wegen Verstoß gegen den erst kürzlich aktualisierten Paragraphen 188 des Strafgesetzbuches verurteilt worden sein. 

§ 188 StGB. Gegen Personen des politischen Lebens gerichtete Beleidigung, üble Nachrede und Verleumdung besagt:

> (1) Wird gegen eine im politischen Leben des Volkes stehende Person öffentlich, in einer Versammlung oder durch Verbreiten eines Inhalts (§ 11 Absatz 3) eine Beleidigung (§ 185) aus Beweggründen begangen, die mit der Stellung des Beleidigten im öffentlichen Leben zusammenhängen, und ist die Tat geeignet, sein öffentliches Wirken erheblich zu erschweren, so ist die Strafe Freiheitsstrafe bis zu drei Jahren oder Geldstrafe. Das politische Leben des Volkes reicht bis hin zur kommunalen Ebene.
>
> (2) Unter den gleichen Voraussetzungen wird eine üble Nachrede (§ 186) mit Freiheitsstrafe von drei Monaten bis zu fünf Jahren und eine Verleumdung (§ 187) mit Freiheitsstrafe von sechs Monaten bis zu fünf Jahren bestraft.

<!-- more -->
Timm Kellner soll auf seinem Kanal eine der zahlreichen inhaltsvollen, weltbewegenden und insbesondere korrekten Äußerungen der besten Außenmenstruier:enden A.C.A. Baerbock in einer Weise kommentiert haben, die das Wirken der Außenmensturier:enden auf der politischen Weltbühne erheblich erschwert habe. Mein Beileid geht an das Opfer. Ich war bisher der Meinung, sie sei mental instabil und schade sich daher gerne selbst. Naja, man lernt ja nie aus.

Wer sich fragt, wer Timm Kellner überhaupt ist, dem sei gesagt, er ist einer der zahlreichen Youtuber, die das politische Geschehen mit gepsitzter Zunge für eine fein ausgewählte Zielgruppe kommentiert. Ganze 200.000 bis 1,6 Mio. Menschen aus dem ganzen Universum verfolgen regelmäßig oder auch unregelmäßig seine Beiträge.

Mit der Verurteilung ohne durchlaufenes Verfahren (Strafbefehl) wird die Leistung des Wirkens Timm Kellners hochrichterlich anerkannt.

Frau könnte zwar behaupten, dass Außenmenstruier:ende, die durch öffentliche Beleidigungen, üble Nachrede oder verschleumderische Kommentare in ihrem Wirken stark eingeschränkt werden, nicht geeignet seien, die viertgrößte Volkswirtschaft des Planeten zu repräsentieren, jedoch wäre dieses nur ein gültiges Argument in normalen Zeiten.

In Zeiten in denen ein Staatsfunk:ender einen Grimmepreis dafür bekommt, dass er einen angeblichen Diktator eines Landes in dem es noch nicht einmal eine allumfassende Chat-Kontrolle gibt, als Ziegenficker betitelt, ist es natürlich selbstverständlich, dass jemand der in der Breite der Bevölkerung, geschweige denn in der Welt kaum bekannt ist, per Strafbefehl verurteilt wird.

Zum Glück lässt sich die wunderschöne und stets perfekt gestylte A.C.A. Baerbock auf keinen Fall ihre grauen Haare färben. Die im Staatsfunk billig aussehenden Strähnen sind alle natur. Wären ihre Haare gefärbt, könnten sich einige dazu verleiten lassen, sich abfällig und justiziabel über ihre Haarfärbepraktiken zu äußern. Das war schon für den Altkanzelnden Schröder eine Zumutung, und was das für ein Aufwand für die Gerichte wäre.

Man muss in dem linksextremistischen Timm Kellner aber auch seinen beschränkten Geist erkennen. Hätte er einfach behauptet, dass die noch bis zum Ende ihrer ablaufenden Menstruationsperiode Außenministrier:ende sich von Zicken ficken lässt, wäre er wahrscheinlich anstatt mit einer Haftstrafe, mit dem Bundesverdienstkreuz ausgezeichnet worden.

Welche der Auszeichnungen nun angesehener ist kann natürlich jeder für sich selbst entscheiden. Wir leben ja schließlich in einem Freien Land, in dem die Gedanken (dank schleppenden technischen Fortschritts) noch frei sind.

#### Saftsack Schissli

Wo wir bei Susanne Chefli wären, bei der kompetenten Twitter-Tante der Sozialpropagandistischen Partei Deutschlands (SPD). Sie durfte vor einiger Zeit sogar, wie der Weltenretter Wilhelm Thor aus Nordamerika, in der tagesschau auftreten und ihr kommunikatives Können unter Beweis stellen.

Dort soll sie sich wohl darüber echauffiert haben, dass 90% ihrer Anzeigen die sie regelmäßig wegen Beleidigung oder ähnlichem macht, eingestellt würden. Sie erhielte von der Polizei, der Staatsanwaltschaft oder wer auch immer ihr antwortet die Begründung, dass die Äußerungen die sie zur Anzeige brächte, (Anm. noch) von der Meinungsfreiheit gedeckt seien. Mit Unverständnis fuhr sie fort und sagte: "Das müsse aufhören." Meinungsfreiheit ist eh so 20. Jahrhundert.

#### Solidarische Strompreisbeteiligung

Ihre SPD und die Grünen fordern derweil einen steuerfinanzierten Industriestrompreis. Nachdem der Strompreis für die Industrie durch das Handeln der Regierung zu teuer geworden ist, sollen nun die Steuerzahler den Strompreis für die Industrie subventionieren dürfen. Sozusagen als Arbeitsplatzsicherungsmaßnahme. Ricardo Breite von den Grünen: "Wir nehmen da jetzt mal richtig viel Geld in die Hand, um Arbeitsplätze in Deutschland zu sichern." - Aber nicht gleich alles für Cheeseburger ausgeben.

#### Das große Kotzerama

Bei all den tollen Nachrichten der letzten Wochen gibt es dennoch ganz traurige Nachrichten. Erstens, mein Kotzreiz besteht weiterhin. Bei jeder Äußerung der Politik:- und Medienbetreib:enden überkommt mich eine bis zum Erbrechen starke Übelkeit.

#### 33,3% - Dumme Ungeimpfte werden immer mehr

Zweitens, die Verdummung der Gesellachaft durch ständige Wahrheitsverkündungen der Politik:- und Medienbetreib:-ende schreitet mit rasendem Tempo voran. Bei den letzten Sonntagsfragen stürzen die Grünen weiter ab.

Bei dem Fake-Umfrage-Institut Emnid steht die AfD mit den Grünen gleichauf bei 16%. Beim rechtsextremistischen Allensbach-Institut liegt die AfD mit 16% inzwischen mit einem Prozentpunkt vor den Grünen, und nur noch zwei Punkte hinter der Sozialpropagandistischen Partei Deutschlands. Bei dem russischen Propagandainstitut INSA/YouGov liegt die AfD bereits zwei Punkte vor den Grünen. Die Regierung kommt derzeit auf Zustimmungswerte von 41-43%. Das ist fast doppelt so viel wie die dreifache Mehrheit. Prognosen gehen von bis zu 33.3% für die AfD in einzelnen Bundesländern aus.

So traurig. Aber vielleicht hat es auch was Gutes. Bald werden wir sehen, wie die Rückgradlosen im Staatsfunk neue Töne anschlagen werden. Vielleicht wird dem Einem oder der Anderen wegen fehlender Flexibilität des persönlichen Wertekanons auch eeieieine neue Herausforderung gesucht. Das Jobcenter ist sicherlich gerne behilflich. Auf jeden Fall wird das ein großer Spaß werden, wenn die Blockparteien ...

#### Erster Mai - Tag des Arbeits:ende

In Berlin bereitet sich die mit Linksextremisten durchseuchte Polizei auf die vom rechtsextremistischen Rand angedrohte Randale am Ersten Mai vor. 

In Friedrichshain und Kreuzberg sind bereits Fenster und Türen der Banken und einiger Geschäfte verbarrikadiert. Es werden auch wieder viele Assi-Touris aus dem Westreich erwartet, die alle Jahre meine Heimat verdrecken, sich dann verpissen und beim tagesschau Gucken mit der Mutti über die Chaoten in Berlin herziehen. Dank Videoüberwachung und 49€-Ticket ist es nun aber möglich die Besucher zu besuchen und sich dafür umgehend zu bedanken.

#### Nazis in Jerusalem

In Israel demonstrieren 160.000 rechtsextremistische Juden gegen die Demokratie sichernden Gesetze der Regierung Bibi Netanjahu 666.

#### Wetter im Tagespendelbereich

Ansonsten freue ich mich auf den Monat Mai. Dank 49€ und Übergangsticket zur Ersten Klasse im Bereich des VBB werde ich nun täglich im Tagespendelbereich herumreisen. Wegen des Klimawandels und des daraus resultierencen kältesten Frühling der Erdgeschichte, war ich im April und Ende März etwas betrübt.

**Hinweis für die Gestapo**

Der Personer und die Handlung des Beitrags sind frei erfunden. Etwaige Ähnlichkeiten mit tatsächlichen Begebenheiten oder im politischen Leben des Volkes stehender oder gestandener Personer wären rein zufällig.
