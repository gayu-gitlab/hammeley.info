---
title: Elektronische Entspannugsmusik
category: musik
layout: playlist
playlist_id: eem
data: "music,playlists"
content_top: | 
    "<figure>
        <blockquote>
            <p>„Jede Musik basiert auf Mathematik. Hör dir Acid House an. Wenn man das hedonistische Moment rausdividiert und die Nebenbedingungen weglässt, dann ist Acid House eine sehr geometrische Musik, sehr funktional. Nimm zum Beispiel 808 State. Das ist pure Mathematik, Musik wie nach Gleichungen gebaut. Unsere Musik funktioniert nach Formeln. Wir bauen Stücke, die die physikalischen Formeln von Gas oder Wasser simulieren. Du hast z. B. ein Geräusch von tropfendem Wasser. Dieses Geräusch kann man mit Hilfe von Daten simulieren. Und dann kannst du es manipulieren. Wir sind von solchen Simulationen vollkommen besessen.“</p>
        </blockquote>
        <figcaption class="blockquote-footer">
            <cite title="Wikipedia">Sean Booth, Autechre</cite>
        </figcaption>
    </figure>"
content_bottom: "In den 90ern hörte ich im Radio unter anderem auf 98,8 Kiss FM [ED2000](#eem){: #sQ9vgpPojIU .hmmly_link data-receiver="eem-player"} (Corin Arnold), meistens zum Einschlafen. Ich ging ja noch zur Schule. ED2000 gehörte mit &quot;Rave Satellite&quot; mit Marusha auf Radio Fritz und Housefrau auf VIVA zu den ersten musiklaischen Sendungen, die ich damals regelmäßig hörte und anschaute.<br/><br/>Zum Nikolaustag gibt es daher von mir kleine Liste von Stücken der elektronischen Entspannungsmusik aus den letzten 30 Jahren."
---

