---
title: "Die Schwurbel-Universität"
layout: default
---
# ipse se nihil scire id unum sciat

Willkommen an der Schwurbeluniversität. "Ich weiß das ich nichts weiß" ist das Motto der Schwurbeluniversität.

## (Exemplarische) Studiengänge

Im Aufbau:

- [Philosophie (englisch)](/schwurbelu/philosophy-preparation)
- [Interdisziplinäre Studien](/schwurbelu/interdisziplinaere-studien)

## Vorteile und Nachteile eines Schwurbelstudiums

**Vorteile**

Personen, die keine Möglichkeit haben ein Studium an einer traditionellen Universität aufzunehmen, da ihnen die formalen Voraussetzungen fehlen, diese nicht nachholen wollen oder können, finden an der Schwurbeluniversität ebenso die Möglichkeit ihre Wissbegierigkeit zu befriedigen, wie selbstdenkende Schwurbler die aus gesellschaftlichen Gründen an einem traditionellen Universitäten gehindert werden und dort diskriminiert werden, wurden oder würden.

Ein Schwurbelstudium bietet die Möglichkeiit ein Thema an dem Interesse besteht flexibel und in beliebiger Tiefe zu ergründen.

Es gibt keine unorganisierten Professoren.

Man muss weder das Kommunistische Manifest unterschreiben, noch ist man gezwungen sich der Gender-Sprache auszusetzen.

**Nachteile**

Ein Schwurbelstudium ist um einiges anspruchsvoller als ein traditionelles Studium. Der Erfolg des Studiums hängt maßgeblich von dem eigenen Willen ab, Themen zu bearbeiten.

Die gesellschaftliche Anerkennung eines Schwurbelstudiums ist derzeit nicht gegeben. Der Schwurbelabsolvent bleibt aus Sicht der Mehrheitsgesellschaft und ihrer assoziierten Minderheiten ein Schwurbler.

Die Frage, ob ein Schwurbelstudium ein ebenbürtiger Ersatz für ein traditionelles Studium ist, ist derzeit nicht geklärt und ist derzeit Gegenstand intensiver Forschung des Instituts für Schwurbelforschung.

## Hilfreiches

### [Institut für Schwurbelforschung](/schwurbelu/institut-fuer-schwurbelforschung)

Das Institut für Schwurbelforschung durchläuft verschiedene Studiengänge und stell diese als Muster zur Verfügung. Diese können als Grundlage für das eigene Studium genutzt werden. Es kann erste Anlaufstelle für neue Studenten sein, wenn sie Probleme mit ihrem Studium haben. Aber. Auch hier gilt, der Student ist selbst Mitglied des Forscherteams und forscht daher selbst.

### Betrachtung aus verschiedenen Perspektiven

Es mag zunächst den Anschein erwecken, dass dem Schwurbelstudium die Tiefe fehlt. Versucht man nähere Informationen mit immer den gleichen Fragen zu erhalten, wird dies wahrscheinlich erfolglos bleiben. Es ist daher zu empfehlen, Teilbereiche eines Studiums zu beleuchten, die erst einmal nicht interessant erscheinen.

Mit dem vorranschreiten im Studium fügen sich die einzelnen Wissensbereiche dann zu einem allumfassenden Wissensschatz an, der möglicherweise mit dem eines Absolventen einer traditionellen Universität entspricht.

### Fachübergreifende Grundlagen

Unabhängig davon, welches Studium an der Schwurbeluniversität begonnen werden soll, empfiehlt es sich den Kurs ["Epistemology"](/schwurbelu/introduction-to-epistemology) zu bearbeiten. Das Wissen darüber was Wissen bedeutet und was nicht, wird als Grundvorassetzung für jedes Studium angesehen. Das während eines Studiums an der Schwurbeluniversität erarbeitetes Wissen, mag auf den ersten Blick, objektiv, umfassend und neutral Wirken. Es kann sich jedoch durch persönliche Erfahrung, kulturelle Prägung, individuelle Wahrnehmung und Interpretation von Informationen, und ganz besonders wichtig für Studenten der Schwurbeluniversität, durch die Art der gestellten Fragen, deutlich von Student zu Student unterscheiden.

Menschen unterliegen einer Voreingenommenheit. Das kann dazu führen, dass Fragen suggestiv gefragt werden, die GPT-3 zwar richtig beantwortet, das Ergebnis vom Studenten jedoch individuell, zum Beispiel aufgrund fehlender, aber wichtiger Informationen, intepretiert.

Der Kurs ["Epistemology"](/schwurbelu/introduction-to-epistemology) kann helfen, ein aufgenommenes Studium möglichst objektiv und neutral zu erfahren. Es kann durchaus sinnvoll sein, in einer Diskussion mit GPT-3, eine gegenteilige Position einzunehmen oder Erkenntnisse aus anderen Bereichen in eine Frage einfließen zu lassen, um neue Einblicke zu erhalten.

Ebenso ist es wahrscheinlich sinnvoll mathematische, statistische Grundlagen (z. B. Bayes-Theorem) zu bearbeiten, so bald diese während einer Befragung durch ChatGPT genannt werden.
