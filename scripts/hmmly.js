"use strict";
// import { Bootstrap } from './lib/bootstrap.bundle.min.js';
import * as constants from './lib/libhmmly/constants.js';
import * as crypt from './lib/libhmmly/crypt.js';
import * as elements from './lib/libhmmly/elements.js';
import { lazyLoadImages } from './lib/libhmmly/lazyload.js';
import * as utils from './lib/libhmmly/utils.js';
import * as jQuery from './lib/jquery-3.6.3.min.js';

const LS_LAST_ACIVE_SLIDE = "activeSlide";

const slideToLastActiveSlide = () => {
    console.log(window.location.pathname);
    if (window.location.pathname === "/bild-und-ton") {
        var activeSlide = localStorage.getItem(LS_LAST_ACIVE_SLIDE);
        if (activeSlide !== null) {
            utils.Utils.dbg("last slide before reload: " + parseInt(activeSlide));
            $('#hmmlyPlayerCarousel').carousel(parseInt(activeSlide));
        } else {
            utils.Utils.dbg("last slide before reload: " + parseInt(activeSlide));
            activeSlide = 0;
        }
    }
}

const storeActiveSlide = () => {
    $('#hmmlyPlayerCarousel').on('slid.bs.carousel', () => {
        var activeSlide = $('#hmmlyPlayerCarousel').find('.carousel-item.active').index();
        localStorage.setItem(LS_LAST_ACIVE_SLIDE, activeSlide);
        utils.Utils.dbg("storing before reload: " + activeSlide);
    });
}

$( document ).ready( () => {
    console.log("hammeley.info (hmmly.js) app and its dependencies loaded.");
    var searchTimeout; // promise
    var last_query = "";
    var query_string = "";
    var min_query_length = 4;
    var searchTrackingPool = []; // 2-dimensional
    var trackDelay = 3000;

    slideToLastActiveSlide();
    storeActiveSlide();
    lazyLoadImages();

    // SEARCH + SEARCH TRACKING

    $(document).on("input", "#text-filter", () => {
        query_string = $(this).val().toLowerCase();
     
        $(".searchable").each( () => {
            let $element = $(this);
            let containsText = $element.text().toLowerCase().includes(query_string);
            let containsTags = $element.is("[data-tags]") && $element.data("tags").toLowerCase().includes(query_string);
            $element.css("display", containsText || containsTags ? "" : "none");
        });
        $(window).scrollTop(0);
    });
   
    /** collect trackable search queries */
    const addToPool = () => {
        // add query to tracking pool under conditions
        if (last_query !== query_string && query_string.length >= min_query_length && query_string !== "") {
            let num_results = $(".searchable:visible").length;
            let trackingItem = [query_string, window.location.pathname, num_results];
            searchTrackingPool.push( trackingItem );
            // last query was added to the pool.
            last_query = query_string;
        } else {
            utils.dbg("Query not added to the pool. No change.");
        }
    }

    /** trigger tracking */
    $("#text-filter").on("blur focusout mouseleave", () => {
        if (last_query !== query_string  && query_string.length >= min_query_length && query_string !== "") {
            addToPool();
            searchTimeout = utils.delay(trackDelay);
            searchTimeout.then( () => {
                track();
                clearTimeout(searchTimeout);
            });
        }
    });
    
    /** track before unload */
    $(window).on('beforeunload', (event) => {
        if (searchTimeout !== null) {
            clearTimeout(searchTimeout);
            track();
        }
    });    
    
    const track = () => {
        console.log(searchTrackingPool);
        for (var _ in searchTrackingPool) {
            _paq.push(['trackSiteSearch', searchTrackingPool[0][0], searchTrackingPool[0][1], searchTrackingPool[0][2]]);
            searchTrackingPool.shift();
        }
    }
    
 
    // HEADER SHADOW ON PAGE SCROLL
    $(window).scroll( () => {
        if ($(this).scrollTop() > 7) {
            $("header").addClass("shadow");
        } else {
            $("header").removeClass("shadow");
        }
    });

    // 404 error handling
    if (document.title === "404 | hammeley.info") {
        document.getElementById("page_path").innerHTML = window.location.pathname;
        utils.ErrorRedirect.notFound(10);
    }    
});