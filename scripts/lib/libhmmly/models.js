"use strict";
import * as constants from './constants.js';
import * as utils from './utils.js';

/**
 * TODDO: each Hmmly-element that extends HTMLElement has a set of supported attributes:
 * 	static supportedAttributes = [constants.HmmlyAttribute.PLAYLIST_ID, constants.HmmlyAttribute.VIDEO_ID];
 *  This class validates the provided data-attributes and returns its values
 **/

export class AttributeValidator {
    static validate(element, supportedAttributes) {
        const attributes = Object.values(supportedAttributes);
        const unsupportedAttribute = attributes.find(attr => !element.dataset[attr]);
        if (unsupportedAttribute) {
            console.error(`Error: Unsupported attribute "${unsupportedAttribute}"`);
            return false;
        }
        return true;
    }
}

export class Validator {
	static validate(fields, object) {
        for (let i in fields) {
            console.log("Media.validate(): " + fields[i]);
            if (Object.prototype.hasOwnProperty.call(object, fields[i])) {
                if (this[fields[i]] === undefined) {
                    object.log(object.constructor.name + ".validate(" + object.id + ") has got an undefined " + fields[i] + ". Please fix.");
                    object[fields[i]] = object.constructor.name + ".validate(" + object.id + "): added value. Please fix!";
                }
            } else {
				// if this happens, class implementation is missing this field.
                throw new Error("Media.validate(): Object is missing mandaroty field " + fields[i] + ".");
            }
        }
    }
}

export class Media {
	static mandatory_fields = ["id", "artist", "title", "date"];
	constructor(artist, title, date, date_comment, id, img_size) {
        for (field in Media.mandatory_fields) {
            if (field === undefined) {
                utils.Utils.dbg("Media(): mandatory field " + field + " is undefined.");
                break;
            }
        }
		this.artist = artist;
		this.title = title;
		this.date = date;
		this.dateComment = date_comment;
		this.id = id;
		this.img_size = img_size;
	}
}

export class Playlist {
    static supportedAttritbutes = [constants.HmmlyAttribute.PLAYLIST_ID, constants.HmmlyAttribute.VIDEO_ID];

    constructor(title, id, content) {
        this.title = title;
		this.id = id;
        this.content = content;
    }

    static async by(hmmlyAttribute, value) { // todo: HmmylAttributes
		utils.Utils.dbg("Playlist.by(" + hmmlyAttribute + ", " + value + "): Searching..");
        if (hmmlyAttribute === constants.HmmlyAttribute.PLAYLIST_ID) {
            return utils.DataProvider.fetch(constants.MEDIA_PATH).then( (collection) => {
                var pl = collection.playlists.content.find( (playlist) => playlist.id === value );
                return new Playlist(pl.title, pl.id, pl.content);
            });
        } else if (hmmlyAttribute === constants.HmmlyAttribute.VIDEO_ID) {
            // returns a playlist with a single video, that is video.id
            return utils.DataProvider.fetch(constants.MEDIA_PATH).then( (collection) => {
                // TODO: collection parameter: music, film, news or search through all
                var video = collection.music.content.find( (video) => video.id === value );
                var title = video.artist + " - " + video.title;
                var content = [
                    {
                        "title": title,
                        "id": video.id
                    }
                ];
                return new Playlist(title, "playlist-" + video.id, content);
            });
        } else {
            utils.Utils.dbg("Playlist.by(" + hmmlyAttribute + ") unsopported. Supported: " + Playlist.supportedAttritbutes);
        }
	}
    
    //todo HmmlyAttributes
	static from(json) { // check if correct
        var pl = Object.assign(new Playlist, JSON.parse(json));
        return new Playlist(pl.id, pl.title, pl.content);
	}

    static toIdStrings(playlist) {
		utils.Utils.dbg("Playlist.toIdStrings(" + playlist.id + "): Converting Playlist content into string of ids.");
        utils.Utils.dbg("Playlist.toIdStrings(" + playlist.content + ") < Content.");
        var cs = playlist.content.map(a => a.id).toString(); 
		return cs;
    }
}
