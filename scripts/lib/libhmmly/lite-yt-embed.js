
/** 
 * A fork
 * of https://github.com/paulirish/lite-youtube-embed
 * by https://gitlab.com/aspera-non-spernit/
 * but heavily altered.
 *
 **/
"use strict";
import * as utils from './utils.js';
import * as youtube from './players/youtube.js';

export const ORIGIN = "https://hammeley.info" + window.location.pathname;

export class LiteYTEmbed extends HTMLElement {
    static suffix = "-player";

 	constructor(id, playlist) {
        super();
        
        this.id = id;
        if(playlist === undefined || playlist.length === 0) {
            throw new Error("LiteYTEmbed(): Received invalid playlist." + playlist + ", length: " + playlist.length);
        } else if (playlist !== undefined) {
            utils.Utils.dbg("LiteYTEmbed(): Playlist contains " + playlist.content.length + " item(s).");
            playlist.content = utils.Filter.disabled(playlist.content); // playlist.content.filter( (t) => { return t.disabled === undefined || t.disabled !== true });
            this.playlist = playlist;
        } 
        utils.Utils.dbg("LiteYTEmbed(): LiteYTEmbed player \"" + id + "\" created."); 
    }

    async connectedCallback() {
        // current Media. first in playlist
        this.currentMedia = await utils.DataProvider.findMediaById(this.playlist.content[0].id);
        if (this.currentMedia === undefined || this.currentMedia === null || this.currentMedia.length === 0) {
            utils.Utils.dbg("LiteYTEmbed.connectedCallback(): Something went wrong. Couldn't find " + this.playlist.content[0].id + ". This may happen if media found in a playlist but is missing in the music.json or films.json");
        } else {
            utils.Utils.dbg("LiteYTEmbed(): Starting with: " + this.currentMedia.id);
            if (!this.style.backgroundImage) { await this.setBackgroundImage(this.currentMedia.yt_img); }
            
            this.playLabel = this.currentMedia.artist + " - " + this.currentMedia.title;
        }
       
        var playButton = this.playButton();
        this.addPlayButton(playButton, this.playLabel);

        this.addListeners();
    }

/* PREP WRAP (FAKE PLAYER) VIEW */
    playButton() { return this.querySelector('.lty-playbtn'); }

    addPlayButton(playButton, label) {
        // Set up play button, and its visually hidden label
        if (!playButton) {
            playButton = document.createElement('button');
            playButton.type = 'button';
            playButton.classList.add('lty-playbtn');
            this.append(playButton);
        }
        if (!playButton.textContent) {
            const playBtnLabelEl = document.createElement('span');
            playBtnLabelEl.className = 'lyt-visually-hidden';
            playBtnLabelEl.textContent = label;
            playButton.append(playBtnLabelEl);
        }
        playButton.removeAttribute('href');
    }

 	async setBackgroundImage(img_size) {
        if (img_size !== undefined && img_size !== null && img_size !== "") {
            utils.Utils.dbg("LiteYTEmbed.setBackgroundImage(): Image info found for \"" + this.currentMedia.id + "\". Using Youtube's " + img_size + ".jpg");
            this.style.backgroundImage = `url("https://i.ytimg.com/vi/${this.currentMedia.id}/${img_size}.jpg")`;
        } else {
            utils.Utils.dbg("LiteYTEmbed.setBackgroundImage(): No image info found for \"" + this.currentMedia.id + "\". Using Youtube's hqdefault.jpg");
            this.style.backgroundImage = `url("https://i.ytimg.com/vi/${this.currentMedia.id}/hqdefault.jpg")`;
        }
    }

    async addListeners() {
        utils.Utils.dbg("LiteYTEmbed.addListeners(): pointerover warms up YT connections. click adds iframe and player to hmmly-ctrl.");
        this.addEventListener("pointerover", youtube.Youtube.warmConnections, { once: true } );
        this.addEventListener("click", this.addPlayer); // the actual yt embedding
    }

    async params() {
        // TODO: make default, option to override
        var urlFragments = new URLSearchParams(this.getAttribute('params') || []);
        utils.Utils.dbg("LiteYTEmbed.params(): Parameters passed from <hmmly-ctrl> " + this.id + ":" + urlFragments + ". Appending more parameters: ");
        urlFragments.append("autoplay", "1");
        urlFragments.append("playsinline", "1");
        urlFragments.append("modestbranding", "1");
        urlFragments.append("enablejsapi", "1");
        urlFragments.append("controls", "1");
        urlFragments.append("origin", ORIGIN);
        utils.Utils.dbg("LiteYTEmbed.params(): ?" + urlFragments);
        return urlFragments;
    }

    async addPlayer() {
        utils.Utils.dbg("LiteYTEmbed.addPlayer("+ this.id +")");
        
        if (this.classList.contains('lyt-activated')) return;
        
        this.classList.add('lyt-activated');

        var params = await this.params();
       
        utils.Utils.dbg("LiteYTEmbed.addPlayer(): Adding placeholder for iframe.");
                    
        this.youtube = new youtube.Youtube(this.playlist, this.currentMedia, params, this);

        this.addIframe();
        await this.youtube.init();
    }

    async addIframe() { 
        utils.Utils.dbg("LiteYTEmbed.addIframe(): Adding iframe.");
        const paramsObj = Object.fromEntries(this.youtube.params.entries());
        var iframe = document.createElement("iframe");
        iframe.src = await this.youtube.makeUrl();;
        iframe.width = 560;
        iframe.height = 315;        
        iframe.allow = 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture';
        iframe.allowFullscreen = true;
        iframe.title = this.playLabel;
    
        if ( !this.contains(iframe) ) {
            this.appendChild(iframe);
        }
        iframe.focus();
    }    

    cue(id) {
        // Check if the YouTube API is available
        if (this.youtube) {
            // Forward the cue request to the YouTube player
            this.youtube.loadVideoById(id);
        } else {
            // Handle the case when the YouTube API is not available
            utils.Utils.dbg("LiteYTEmbed.cue(): YouTube API not available.");
        }
    }
}

customElements.define('lite-youtube', LiteYTEmbed);