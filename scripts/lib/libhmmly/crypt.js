// Encryption function#
export const KDF = "PBKDF2";

export function encrypt(text, passphrase, userName) {
    let encoder = new TextEncoder();
    var data = encoder.encode(text);
    var encodedPassphrase = encoder.encode(passphrase);
    let encodedUsername = encoder.encode(userName);
    var salt = new Uint8Array([...encodedPassphrase, ...encodedUsername]);

    console.log("salt " + salt);

    return crypto.subtle.importKey(
        "raw",
        encodedPassphrase,
        { name: KDF },
        false,
        ["deriveKey"]
    )
    .then(baseKey => crypto.subtle.deriveKey(
        { name: KDF, salt, iterations: 1000, hash: "SHA-256" },
        baseKey,
        { name: "AES-GCM", length: 256 },
        false,
        ["encrypt"]
    ))
    .then(key => crypto.subtle.encrypt({ name: "AES-GCM", iv: new Uint8Array(12) }, key, data))
    .then(encrypted => new TextDecoder().decode(encrypted));
}

// Decryption function
export function decrypt(encryptedText, passphrase, userName) {
    var decoder = new TextDecoder();
    let encoder = new TextEncoder();
    let encodedPassphrase = new TextEncoder().encode(passphrase);
    let encodedUsername = encoder.encode(userName);
    var salt = new Uint8Array([...encodedPassphrase, ...encodedUsername]);
    return crypto.subtle.importKey(
        "raw",
        encodedPassphrase,
        { name: KDF },
        false,
        ["deriveKey"]
    )
    .then(baseKey => crypto.subtle.deriveKey(
        { name: KDF, salt, iterations: 1000, hash: "SHA-256" },
        baseKey,
        { name: "AES-GCM", length: 256 },
        false,
        ["decrypt"]
    ))
    .then(key => crypto.subtle.decrypt({ name: "AES-GCM", iv: new Uint8Array(12) }, key, new TextEncoder().encode(encryptedText)))
    .then(decrypted => decoder.decode(decrypted));
}
