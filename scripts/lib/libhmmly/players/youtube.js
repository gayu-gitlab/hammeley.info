"use strict";
import * as utils from '../utils.js';
import * as lite from '../lite-yt-embed.js';
import * as models from '../models.js';

export const YOUTUBE_BASE_URLS = ["https://www.youtube.com", "https://www.youtube-nocookie.com"];
export const YOUTUBE_IMAGE_BASE_URL = "https://i.ytimg.com/vi/";
export const PRECONNECTS = [
    YOUTUBE_BASE_URLS[0],
    YOUTUBE_BASE_URLS[1],
    "https://www.google.com",
    "https://googleads.g.doubleclick.net",
    "https://static.doubleclick.net",
];

// TODO: catch 503 error, timeout retry
export class Youtube {
    constructor(playlist, currentMedia, params, parent) {
        this.playlist = playlist;
        this.currentMedia = currentMedia;
        this.params = params;
        this.parent = parent;
        this.player = null;
        this.api = null;
    }

    async init() {
        if (this.url === undefined) {
            this.url = this.makeUrl();
            console.log("Youtube.init(): " + this.url);
        } 
        this.player = await this.#create();
    }
    
    async #create() {
        await this.#fetchApi();
        var scriptHold = document.createElement("div");
        scriptHold.setAttribute("id", "scriptHolder");

        return new YT.Player(scriptHold, { // Do not add scriptHold to DOM!
            width: '100%',
            videoId: this.currentMedia.id,
            playerVars: this.params,
            events: {
                'onReady': (event) => {
                    utils.Utils.dbg("LiteYTEmbed.onPlayerReady(event): Playing video: " + this.currentMedia.id);
                    this.api = event.target;
                    event.target.playVideo();
                },
                'onPlayerStateChange': (state) => { 
                    utils.Utils.dbg("Youtube.onPlayerStateChange(): " + state);
                },
                'onError': (e) => { utils.Utils.dbg("Youtube.create().onError: " + e); }
            }
        });
    }

    async #fetchApi() {
        utils.Utils.dbg("Youtube.fetchYTPlayerApi(): Loading Youtube API.");
        if (window.YT || (window.YT && window.YT.Player)) return;

        return new Promise((res, rej) => {
            var ytScript = document.createElement('script');
            ytScript.src =  YOUTUBE_BASE_URLS[0] + '/iframe_api';
            ytScript.onload = _ => {
                utils.Utils.dbg("Youtube.fetchApi(): YT.ready()");
                YT.ready(res);
            };
            ytScript.onerror = (error) => {
                utils.Utils.dbg("Youtube.fetchApi(): Error loading Youtube API: ", error);
                rej(error);
            };
            this.parent.appendChild(ytScript);
        });
    }

    makeUrl() {      
        utils.Utils.dbg("Youtube.makeUrl(): Making url ready for player. Using playlist: \"" + this.playlist.id + "\"");
        var url = YOUTUBE_BASE_URLS[0] + "/embed/";
        url += `${encodeURIComponent(this.currentMedia.id)}`;
        url += `?${this.params.toString()}`;
        if (this.playlist !== undefined && this.playlist.content.length >= 2) {
            utils.Utils.dbg("Youtube.makeUrl(): Adding URL encoded and comma separated playlist as url fragment &playlist=.");
            url += `&playlist=${models.Playlist.toIdStrings(this.playlist)}`;
		}
        utils.Utils.dbg("Youtube.makeUrl(): Youtube URL: " + url);
		return url;
	}

/** CONNECTIONS */
    static warmConnections() {
        utils.Utils.dbg("Youtube.warmConnections(): Mouse hover detected. Warming up connections:");
        if (this.preconnected) return;

        PRECONNECTS.forEach( (u) => {
            utils.Utils.dbg("Youtube.warmConnections():" + u);
            Youtube.addPrefetch("preconnect", u);
        });
        this.preconnected = true;
        utils.Utils.dbg("Youtube.warmConnections(): So cozy now..");
    }
	
    static addPrefetch(kind, url, as) {
        const linkEl = document.createElement('link');
        linkEl.rel = kind;
        linkEl.href = url;
        if (as) {
            linkEl.as = as;
        }
        document.head.append(linkEl);
    }

    loadVideoById(id) {
        if (this.api !== null && this.api !== undefined) {
            console.dir(this.api);
           // this.api.mute();
            this.api.loadVideoById(id, "large");
        } else {
            utils.Utils.dbg("Youtube.loadVideoById(" + id + "): YT.Player not yet ready.");
        }

        if (this.player !== null && this.player !== undefined) {
            console.dir(this.player);
            this.player.loadVideoById(id, "large");   
        } else {
            utils.Utils.dbg("Youtube.loadVideoById(" + id + "): YT.Player not yet ready.");
        }
    }
}
