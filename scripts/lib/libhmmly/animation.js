import * as utils from './utils.js';

export class AmbientLighting {
    constructor(parent) {
        this.canvas = document.createElement('canvas');
        this.ctx = this.canvas.getContext('2d');
        this.parent = parent;
    }

    initialize() {
        utils.Utils.dbg("AmbientLighting.initialize()");
    }
/**
        // Check if there are multiple iframes within the parent element
        const iframes = this.parentElement.querySelectorAll('iframe');
        if (iframes.length > 1) {
            // Handle multiple iframes
            this.handleMultipleIframes(iframes);
        } else if (iframes.length === 1) {
            // Only one iframe, proceed with ambient lighting
            this.computeAmbientLighting(iframes[0]);
        } else {
            // No iframes found, do nothing
            console.log('No iframes found within the parent element.');
        }
*/
 
/**
    handleMultipleIframes(iframes) {
        // Wait for all iframes to be loaded
        const iframePromises = Array.from(iframes).map(iframe => new Promise((resolve, reject) => {
            if (iframe.contentWindow && iframe.contentWindow.document.readyState === 'complete') {
                resolve();
            } else {
                iframe.onload = resolve;
            }
        }));
        
        Promise.all(iframePromises).then(() => {
            // All iframes are loaded, proceed with ambient lighting
            this.computeAmbientLighting(iframes[0]); // For example, compute ambient lighting based on the first iframe
        }).catch(error => {
            console.error('Error loading iframes:', error);
        });
    }

    computeAmbientLighting(iframe) {
        // Compute ambient lighting based on the content of the iframe
        // Example: Take a snapshot of the iframe and apply the ambient lighting effect
    }

    start(parent) {
        if (parent.querySelector('iframe') !== null) {
            console.log("Ambient: starting");
            this.parent.appendChild(this.canvas);
            this.videoIFrame = parent.querySelector('iframe');
            this.canvas.width = this.videoIframe.offsetWidth;
            this.canvas.height = this.videoIframe.offsetHeight;
            this.captureFrame(); 
        } else {
            console.log("Ambient: Video iframe not found.");
        }
    }

    captureFrame() {
        // Draw the current video frame onto the canvas
        this.ctx.drawImage(this.videoIframe, 0, 0, this.canvas.width, this.canvas.height);

        // Get the image data from the canvas
        const imageData = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);
    }

    */
}