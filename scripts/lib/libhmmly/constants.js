"use strict";
export const TAG_LITE_YOUTUBE = "lite-youtube";
export const TAG_HMMLY = "hmmly";
export const MEDIA_PATH = "/data/media.json";
export const MEDIA_TYPES = ["music", "films"];
export const DEBUG_MODE = true;
export const HmmlyType = {
	CONTROL: "control",
	LINK: "link",
	PLAYLIST: "playlist",
}

export const HmmlyContentType = {
    Playlist: 'playlist',
    Video: 'video',
};

// simplifying potential attribute name changes.
export const HmmlyAttribute = { 
    ID: "id",
    PLAYLIST_ID: "playlistId",
    RECEIVER_ID: "receiverId",
    VIDEO_ID: "videoId"
}
