"use strict";
import * as animation from './animation.js';
import * as constants from './constants.js';
import * as models from './models.js';
import * as lite from './lite-yt-embed.js';
import * as utils from './utils.js';

export class HmmlyPlayer extends HTMLElement {
	static suffix = "-player";
	static supportedAttributes = [constants.HmmlyAttribute.PLAYLIST_ID, constants.HmmlyAttribute.VIDEO_ID];

	constructor() {
		super();
		this.id = this.getAttribute(constants.HmmlyAttribute.ID) + HmmlyPlayer.suffix;
	}

	bindPlaylist() {
		var plElems = document.querySelectorAll('hmmly-playlist[data-' + constants.HmmlyAttribute.RECEIVER_ID + '="' + this.id + '"]');
		plElems.forEach(playlist => {
			// Add click listeners to .hmmly_link elements within the playlist
			const hmmlyLinks = playlist.querySelectorAll('.hmmly_link');

			hmmlyLinks.forEach(link => {
				link.addEventListener('click', () => {
					const videoId = link.id;
					this.cue(videoId);
				});
			});
		});
	}

	async connectedCallback() {
		var contentType = HmmlyPlayer.supportedAttributes.find(attr => this.dataset.hasOwnProperty(attr));

		utils.Utils.dbg("HmmlyPlayer.connectedCallback(): Type: " + contentType);
		if (contentType !== undefined) {
			this.contentId = this.dataset[contentType];
			utils.Utils.dbg("HmmlyPlayer(): " + this.id + " created. Rendering: (" + contentType + ", " + this.contentId + ")");
			
			this.bindPlaylist();
			
			this.playlist = await models.Playlist.by(contentType, this.contentId);
			// this.playlist = await models.Playlist.by(constants.HmmlyAttribute.ID, this.contentId);
			// todo this.playlist =  await models.Playlist.by(constants.HmmlyAttribute.ID, this.videoId);
			utils.Utils.dbg("HmmlyPlayer.connectedCallback() playlist found by id: " + this.playlist.id);
			if (this.playlist !== undefined) {
				utils.Utils.dbg("HmmlyPlayer.connectedCallback(): found playlist: \"" + this.playlist.title + "\"(" + this.playlist.id + "):");
				utils.Utils.dbg("HmmlyPlayer.connectedCallback(): Adding Eventlistener(\"pointover\") < prepares embedded player.");
				this.addPlayer(); // adds background image.
			} else {
				throw new Error("HmmlyPlayer.connectedCallback(): Couldn't retrieve " + this.contentId);
			}
		} else {
			utils.Utils.dbg("HmmlyPlayer(): No supported attribute found. Supported: " + HmmlyPlayer.supportedAttributes);
		}
		this.#withAmbient();
	}

	#withAmbient() {
		if ( this.closest('.ambient') !== null ) {
			const ambientLighting = new animation.AmbientLighting( this.closest('.ambient') );
			ambientLighting.initialize();
		}
	}

	addPlayer() {
		utils.Utils.dbg("HmmlyPlayer.addPlayer(): Adding \"" + this.id + lite.LiteYTEmbed.suffix + " (LiteYTEmbed(" + this.id + ", " + this.playlist.id + "))");
		this.player = new lite.LiteYTEmbed(this.id + lite.LiteYTEmbed.suffix, this.playlist); //  TODO: access through this.players
		
		this.player.classList.add('screen');
		this.player.classList.add('rounded');
		this.append(this.player);
	}

	removePlayer() { this.player.remove(); }

	cue(id) {
		console.log(this.player.id + " cue to " + id );
		if (this.player) {
            // Forward the cue request to the YouTube player
            this.player.cue(id);
        } else {
            // Handle the case when the player instance is not available
			utils.Utils.dbg("HmmlyPlayer.cue(): YouTube player instance not yet available.");
        }
	}
}

/**
 * 
 * <hmmly-playlist id="{{page.playlist_id}}-playlist" receiver="{{page.playlist_id"> 
 *   <arbitrary element with class="hmmly-link" and id="{referring to a selectable source, ie. youtube id in music, playlists ..}"
 * </hmml-playlist>
 *
 * id: unique id of the playlist
 * receiver: the id of the media player receiving the playlist events (ie. click, sort)
 *
 **/
 
// TODO: separation of HmmlyPlaylist and HmmlyLink
export class HmmlyPlaylist extends HTMLElement {
	static suffix = "-playlist";
	// TODO: unify data source
	constructor() {
		super();
		this.id = this.getAttribute(constants.HmmlyAttribute.ID) + HmmlyPlaylist.suffix;
		this.receiver_id = this.getAttribute(constants.HmmlyAttribute.RECEIVER_ID);
		utils.Utils.dbg("HmmlyPlaylist(" + this.id + "): Creating..");
	}
}

customElements.define('hmmly-player', HmmlyPlayer);
customElements.define('hmmly-playlist', HmmlyPlaylist);