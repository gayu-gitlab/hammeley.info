"use strict";

import * as constants from './constants.js';
import * as dl from '../damerau-levenshtein.js';

export const delay = (ms) => {
	return new Promise(resolve => {
		setTimeout( () => {
			resolve();
		}, ms);
	});
};

export const camelToDash = (str) => {
    return str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1-').toLowerCase();
}

export class DataProvider {
	constructor() {
		this.id = this.getAttribute(constants.HmmlyAttribute.ID);
		Utils.dbg("DataProvider() \"" + this.id + "\" created.");
	}

	static async fetch(path) { // TODO static coll
		if (DataProvider.collection === undefined) {
			Utils.dbg("DataProvider.fetch(" + path + "): Fetching.");
			return fetch(path)
			.then( (response) => response.json() )
			.then( (json) => { 
				DataProvider.collection = eval(json);
				return DataProvider.collection;
			});
		}  else {
			Utils.dbg("DataProvider.fetch(" + path + "): Media file already fetched. ");
			return DataProvider.collection;
		}
	}

	// not to be used in loops. TODO: arg path, rm fetch // better find(id, in_collection)
	static async findMediaById(id) {
		// TODO: select media catalogue
		var collection = await DataProvider.fetch(constants.MEDIA_PATH);
	// TODO: for loop not working
	//	for (var i = 0; i < constants.MEDIA_TYPES.length; i++ ) {  // returns idx
			Utils.dbg("DataProvider.findMediaById(" + id + "): Querying in " + constants.MEDIA_TYPES[0]);
			var media = collection.music.content.find( (entry) => entry.id === id );
			
			if (media !== undefined) { 
				Utils.dbg("DataProvider.findMediaById(" + id + ") Result: " + media.title);
				return media;
			} else {
				Utils.dbg("DataProvider.findMediaById(" + id + "): Querying in " + constants.MEDIA_TYPES[1]);
				media = collection.films.content.find( (entry) => entry.id === id );
				Utils.dbg("DataProvider.findMediaById(" + id + ") Result: " + media.title);
				return media;
			}
		}
	
}

export class ErrorRedirect {

	static async notFound(num_results) {
		var data = await fetch(window.location.origin + "/sitemap.xml")
			.then( (response) => {
				if (!response.ok) {
					throw new Error(`ErrorRedirect.notFound(${num_results}): Failed to fetch sitemap. Status: ${response.status}`);
				}
			return response.text();
			});
		  
		var parser = new DOMParser();
		var xmlDoc = parser.parseFromString(data,"text/xml");
		
		var locs = xmlDoc.getElementsByTagName("loc");
		var distances = [];
		for (var loc of locs) {
			var url = new URL(loc.childNodes[0].nodeValue);			
			var query = url.pathname.split("/").pop();
		
			distances.push(
				{
					distance: dl.distance(query, window.location.pathname),
					path: url.pathname
				 }
			);
			distances.sort((a,b) => a.distance - b.distance); 
		}

		var list = document.getElementById("similar");
		for (var i = 0; i < num_results; i++) {
			if (distances[i] !== undefined) {
				var h3 = document.createElement("h3");
				var a = document.createElement("a");
				a.setAttribute("href", distances[i].path);
				a.innerText = distances[i].path;
				h3.appendChild(a);
				list.appendChild(h3);
			}
		}
	}
}

export class Filter {
    static disabled(collection) {
        return collection.filter( (elem) => { return elem.disabled === undefined || elem.disabled !== true });
    }
}

export class Result {
	constructor(success, error) {
		if (success) {
			this.success = true;
			this.error = null;
		} else {
			this.success = false;
			this.error = error;
		}
	}
}

export class Utils { static dbg(msg) { if (constants.DEBUG_MODE) { console.debug(msg); } } }

export class Validate {
	static links(links) {
		var validated_links = [];
		for (let link in links) {
			validated_links.push( Validate.link(link) );
		}
	}

	static link(link) {
		var err_msg = "";

		// mandatory attributes id and receiver 
		if (link.id === undefined) {
			Utils.dbg("Validate.link(): The current implementation requires an id for each hmmly-link referring to a selectable source.");
			err_msg = link + " has got no mandatory id that refers to a media id in media.json.";
		}
		
		if (link.receiver === undefined) {
			err_msg.concat(" The element is also missing the receiver=\"..\" attribute.");
		}

		// does the receiver element exist in the document?
		if (err_msg === "") {
			if ( !Validate.receiver_exists(link.receiver) ) {
				err_msg = "Validate.link(): The stated receiver: " + link.receiver + " couldn't be found in the document.";
			}
		}

		if (err_msg.length === 0) {
			return new Result(true, null);
		} else {
			return new Result(false, new Error(err_msg));
		}
	}

	/**
	 * 
	 * @param {*} id 
	 * @returns Result { success: true | false , error: Error | null }
	 */
	static playlistId(id) {
		if (id !== undefined && id !== null & id.length !== 0) {
			return new Result(true, null);
		} else if (id === undefined) {
			return new Result(false, new Error("Playlist id is undefined."));
		} else if (id === null ) {
			return new Result(false, new Error("Playlist id is null."));
		} else if (id.length === 0) {
			return new Result(false, new Error("Playlist id length is 0."));
		}
	}
	receiver_exists(id) {
		if (document.getElementById(constants.HmmlyAttribute.RECEIVER_ID) !== undefined) {
			return new Result(true, null);
		} else {
			return new Result(false, new Error("The receiver with id " + id + " couldn't be found in the document."));
		}
	}
}