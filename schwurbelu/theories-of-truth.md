---
title: Theories of Truth | Philosophy
layout: default
faculty: philosophy
---
# Philosophy

## Epistemology 1.0

### Theories of Truth 1.1

**I think I have received a good overview of the traditional analysis of knowledge, knowledge as a mental state, skepticism and the Gettier problem. Now I want to explore the theories of truth. What can you tell me about that?**

Sure! Theories of truth are concerned with understanding what it means for a statement or proposition to be true. There have been many theories of truth proposed throughout the history of philosophy, but some of the most well-known and influential include the **correspondence theory of truth**, **the coherence theory of truth**, **the pragmatist theory of truth**, and **the deflationary theory of truth**.

The correspondence theory of truth is perhaps the most intuitive and widely held theory of truth. According to this theory, a statement is true if it corresponds to or accurately describes reality. For example, the statement "the cat is on the mat" is true if and only if there really is a cat on the mat. The correspondence theory of truth is often associated with **realist metaphysics**, which holds that there is a **mind-independent reality** that exists independently of our beliefs and language.

The coherence theory of truth, on the other hand, suggests that **truth is a matter of coherence or consistency** with a broader set of beliefs or propositions. According to this theory, a statement is true if it fits in with the rest of our beliefs in a coherent and logically consistent way. For example, if we believe that all mammals have hair and we also believe that cats are mammals, then it follows that cats must have hair. The coherence theory of truth is often associated with **idealist** or **constructivist metaphysics**, which holds that reality is constructed by our beliefs and language.

The pragmatist theory of truth, which was developed by American philosophers like William James and John Dewey, suggests that **truth is a matter of practical utility or usefulness**. According to this theory, a statement is true if it is useful in helping us achieve our goals or solve practical problems. For example, the statement "taking antibiotics will cure my bacterial infection" is true if it is useful in helping us get better. The pragmatist theory of truth emphasizes the importance of the practical consequences of our beliefs and actions.

Finally, the deflationary theory of truth suggests that **truth is not a substantial or metaphysical property of statements, but rather a trivial or trivially definable concept**. According to this theory, a statement is true if and only if it is equivalent to a certain formula or statement, such as "snow is white" is true if and only if snow is white. The deflationary theory of truth emphasizes the linguistic and logical nature of truth, and suggests that there is nothing particularly deep or mysterious about the concept of truth.

**In case of the statement regaring a cat sitting on a mat, it's probably without doubt that if there's a cat on the mat, that this statement corresponds to reality. However it still could be argued that what looks like a cat, is actually not a cat, but something else. How can I make sure that my perception of a cat reflects reality?**

**Perception** is one of the ways we come to know about the world around us, but it **is not infallible**. Our senses can be deceived, and our interpretations of what we perceive can be influenced by our beliefs, expectations, and biases.

To ensure that your perception of a cat reflects reality, you may need to use other methods to confirm your belief. For example, you can use additional sense modalities to confirm that what you are perceiving is a cat. You could touch the cat to feel its fur, hear it meow, and even smell it to confirm that it is indeed a cat. Alternatively, you could ask others to confirm that they also see a cat on the mat. You could also use scientific instruments to measure the physical properties of the cat and compare them to known properties of cats. By using multiple methods to confirm your belief, you can **increase the likelihood that your perception reflects reality**.

**But still, depending on cultural influences for instance, a cat may not be identified as a cat, but maybe as a demon. That would be something completely different. I guess there is no reality taking into account of all what I have learned about knowledge so far.**

It is true that cultural influences can affect how we perceive and understand things in the world, including what we consider to be a cat. However, the idea of truth and reality is not necessarily relative to individual or cultural perspectives. While different cultures may have different beliefs and practices, there may still be objective facts about the world that can be independently verified through evidence and reason. In this sense, **reality can be understood as the objective state of things**, while our perceptions and interpretations of that reality may be subjective and influenced by cultural factors.

**In order to agree on statement describing reality, would it make sense to describe the properties of a cat individually instead of calling it a cat? For instance: there is an object on the mat that has two eyes, four legs, it's furry and it makes sounds?**

Describing the properties of the object on the mat in detail can certainly help to avoid any ambiguity or cultural differences in the interpretation of the word "cat." However, it is important to note that the word "cat" serves as a convenient label for the bundle of properties that we associate with a feline animal, and it is a widely accepted convention in language.

Furthermore, it is important to recognize that even when we describe the properties of an object in detail, we are still relying on our **perception** and **interpretation** of those properties, which can be influenced by factors such as our **past experiences**, **cultural background**, and **cognitive biases**. Therefore, while striving for clarity in our language and descriptions can help to reduce misunderstandings, we must also remain aware of the limitations and **potential biases** of our **perception** and **interpretation**.

**Maybe it's a good idea if you outline the key aspects of correspondence theory of truth, so I better understand the link to reality.**

Sure, here are the key aspects of Correspondence Theory of Truth:

1. Truth is about the relationship between propositions and reality.
2. A proposition is true if it corresponds to, or accurately describes, the state of affairs in the world.
3. The truth of a proposition is independent of anyone's beliefs or opinions about it.
4. The goal of inquiry is to discover true propositions that accurately describe reality.
5. The truth or falsity of a proposition is objective and can be established through evidence and logical analysis.
6. Truth is a matter of representation, not creation. Truth is not invented but discovered.
7. The Correspondence Theory of Truth has been the dominant theory of truth throughout most of Western philosophy.

Does that help?

**I am familiar with the word belief learning about the traditional analysis of knowledge. Here the word proposition is used. It seems to the they are used interchangeably. Is that true or is there a difference between the two words?**

In epistemology, "belief" and "proposition" are related but distinct concepts. A **proposition is a statement that can be true or false**, and beliefs are mental states that hold propositions to be true. So while a proposition is an abstract concept that refers to a statement or claim, a belief is a mental attitude or disposition that someone holds toward that proposition.

For example, the proposition "the Earth orbits around the Sun" is a statement that can be either true or false. A belief, on the other hand, is someone's mental attitude towards that proposition. So if someone believes that the Earth orbits around the Sun, then they hold the proposition to be true.

In short, while propositions are abstract objects that can be true or false, **beliefs are mental states that hold propositions to be true**.

**So using this concept, the statement: "I am a student of the AI university" may be my belief, and due to the fact, I just made up my own "AI univserity", which only exists in my mind, I could call this belief true and justified. However, in terms of the  correspondence theory of truth the proposition "I am a student of the AI university" is likely to be false, because it doesn't really reflect reality?**

Yes, that is correct. From a correspondence theory of truth perspective, the statement "I am a student of the AI university" is likely to be false because there is no corresponding reality to this proposition - there is no actual AI university that exists in reality. However, from a belief-based perspective, you could still consider the statement to be true and justified because it corresponds to your personal beliefs and experiences.

**Can you provide a similar example that contrasts truth from the belief-based and the correspondence theory of truth?**

Let's say that you believe it's raining outside because you heard the sound of raindrops and felt wetness on your skin. According to the belief-based theory of truth, your belief that it's raining would be considered true if it corresponds with your mental state.

However, according to the correspondence theory of truth, your belief would only be considered true if it corresponds with the actual state of affairs in the world outside. In other words, your belief is true only if it is actually raining outside, not just in your own mind.

So, in this case, your belief may or may not correspond with reality. If it is actually raining outside, then your belief is both true according to the correspondence theory and justified according to the belief-based theory. But if it's not raining and you just heard a recording of raindrops, for example, then your belief is justified according to the belief-based theory, but not true according to the correspondence theory.

**I understand. It could actually be the case that someone is washing a car outside of my view, to me it sounds like rain, splashes hit my skin, and it may even be cloudy outside. All potential evidence to justify my believe. However it's not really raining.**

Yes, that's correct. The evidence you have may lead you to believe that it is raining, but in reality, it is not. This is where the correspondence theory of truth comes in, as it seeks to align beliefs or propositions with reality, rather than simply relying on internal coherence or subjective experience.
