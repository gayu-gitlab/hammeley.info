---
title: Réalité
category: filme
layout: playlist
---
<hmmly-player id="reality" data-playlist-id="reality" data-provider="films,playlists"></hmmly-player>
<!-- more -->
Als Filmenthusiast, der in seinem Leben sicherlich schon über 1000 Filme gesehen hat, möchte ich heute einen der besten surrealistischsten Filme der 2010er Jahre vorstellen. Reality von Quentin Dupieux (Skript und Regie).

Wer sich beeilt, kann sich den Film sogar auf Youtube anschauen, bis er dort wahrscheinlich gelöscht wird.

[Réalité auf Youtube, 1:26:19](#reality){: #ORD63Pc9wvs .hmmly_link data-receiver="reality-player"}

Wer das Gerne mag, muss den Film gesehen haben. Die Wiederkehrende ["Music with changing Parts"](#reality){: #3Uy_Ag7ETA4 .hmmly_link data-receiver="reality-player"} von Philipp Glass ist aus dem Jahr 1971. 

Übrigens: Mir ist der Titel entfallen, und habe für die Suche GPT3 benutzt. Obwohl ich die Handlung nicht mehr ganz richtig in Erinnerung hatte, konnte mir GPT3 nach einigen Hinweisen den Namen des Films nennen.