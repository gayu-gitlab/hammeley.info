---
title: Super-Mutant-Hero
category: ego
layout: default
last: 2023-05-20
tags: "Ahnen, Forschung, Gene, Diagnostik, Polymorphismus, nukleotid, SNP, ancestry, Krankheit"
---
Durch meine Ahnenforschung bin ich auf die genetische Diagnostik gestoßen, wie sie von Anbietern wie ancestry angeboten wird. Durch das Vergleichen der eigenen Geninformationen mit der anderer Personen ist es möglich genetisch verwandte Personen zu finden. Es ist jedoch nicht immer einfach, die Verwandtschaft einer familiären Seite zuzuordnen.

Am einfachsten ist es, wenn der Nachname der Person bekannt ist, z. B. dadurch, dass der Nachname in eigenen Aufzeichnungen auftaucht. Häufig ist das jedoch nicht der Fall. So braucht es weitere Informationen. Gibt es auch diese nicht, weil z. B. Geburtsurkunden, Heiratsdokumente, Bevölkerungszählungen zerstört wurden oder keine Angaben gemacht wurden, hilft es manchmal den vorhandenen Stammbaum der genetisch Verwandten zu durchsuchen. 

Wenn die Großeltern zum Beispiel aus zwei verschiedenen Regionen stammen (z.B. Franken und Schwarzwald). Findet man nun vermehrt deutsche Nachnamen und Geburtsorte im Stammbaum der meist amerikanischen Verwandten, so könnte das ein Hinweis geben, ob die Person aus der mütterlichen oder väterlichen Seite abstammt. 

Viel häufiger als die nähere Verwandtschaft zeigt ancestry allerdings entfernte Verwandte an. Das wären dann z.B. Nachfahren, die vor vier Generationen vom Cousin der eigenen Ururgroßmutter oder -vater abstammen könnten.

### Mutationen

Etwas später erfuhr ich von Dienstleistern, die das analysierte Erbgut nicht mit dem anderer Personen in derselben Datenbank vergleichen, sondern mit einer Datenbank, in der bekannte Genmutationen, sogenannte SNPs (Single Nucleotide Polymorphism oder Einzelnukleotid-Polymorphismus) gespeichert sind. Dabei handelt es sich um Mutationen, die in einem einzelnen Basenpaar eines Gens auftreten.

<!-- more -->
Mutationen anderer Art, die in mehr als einzelnen Basenpaaren (SNP) stattfanden, werden bei diesen Anbietern nicht untersucht. 

Bekannte Beispiele wären hier die Trisomie 21, das Klinefelter-Syndrom (XXY) oder die XYY-Trisomie.  Das untersuchte Erbgut kann aber auch hierüber Auskunft geben, selbst wenn die SNP-Datenbank diese nicht enthält.

Mutationen in Genen kommen recht selten vor, sie können aber Hinweise auf bestimmte Veranlagungen, den Körperbau- und Merkmalen, der Gesundheit oder Intelligenz geben.

#### Die Normalen

Viele der SNP-Mutationen führen zu keinen erkennbaren Veränderungen, sind völlig harmlos und eher die Regel als die Ausnahme. Blaue Augen, rote Haare, die männliche Glatze oder ein spitzer Haaransatz usw. gehen auf Genmutatonen zurück, die vor Tausenden von Jahren auftraten aber erst dann sichtbar wurden, als Menschen ein bestimmtes Alter erreichten. Heute sind diese ganz normal. Die Auswrikungen vieler SNP sind bis heute auch noch unbekannt oder zu wenig erforscht. 

Manche SNP haben nur geringe Auswirkungen. Abweichungen von der Durchschnittsgröße (je nach Ethnie) werden ebenso in SNPs kodiert, wie auch je nach Geschlecht und Ethnie. Ein weiteres bekanntes Beispiel ist die sogenannte Laktoseintoleranz, die bei asiatischen Ethnien häufiger auftritt als bei anderen, aber auch in Europa vorkommt. Wohne ich mit einer Größe von 1,90 in Kambodscha, dürfte die Suche nach passenden Schuhen problematischer sein, als z. B. in Deutschland. Ein spitzer Haaransatz ist in nahezu allen Fällen unproblematisch, jedoch auch mit anderen [https://de.wikipedia.org/wiki/Spitzer_Haaransatz](schwereren Krankheiten assoziiert).

Andere statistisch signifikante Mutationen können zu einer höheren oder niedrigeren Intelligenz führen. Der Unterschied beträgt jedoch je nach Mutation nur 0,5 bis 3 % (also max. 97-103). Selbst die Addition von mehreren positiv assoziierten "Intelligenz"-Mutationen (>114) kann durch Alkohol- oder Drogenkonsum oder durch ein hemmendes oder förderndes Umfeld zudem gänzlich unbemerkt bleiben.

Sogenannte Volkskrankheiten treten unter Umständen bei bestimmten Mutationen gehäuft auf. Wie der Name schon verrät, sind sie innerhalb einer Ethnie relativ häufig. Mutationen, die das Risiko im Laufe des Lebens z. B. an einer der verschiedene Krebsarten oder Diabetes zu erkranken erhöhen, werden teils von mehr als 50% der Bevölkerung getragen. Das bedeutet jedoch nicht, dass 50% der Bevölkerung erkranken. Obwohl ca. 50% der deutschen oder mitteleuropäischen Bevölkerung Mutationen aufweisen, die z. B. ein erhöhtes Darmkrebsrisiko bedeuten, erkranken natürlich deutlich weniger, nämlich 7 von 100 (oder 1 / 15) Männern an Darmkrebs. Was jedoch trotzdem eine sehr hohe Anzahl ist.

Diese Erkenntnis hat bei uns zu Vorsorgeuntersuchungen geführt, die eine frühzeitige Behandlung ermöglichen. Andererseits hängen Erkrankungen auch in besonderem Maße von den Lebensumständen in der jeweiligen Gesellschaft und dem persönlichen Lebensstil ab. So kann es, ähnlich wie bei der Intelligenz, durchaus sein, dass eine Mutation in einer anderen Ethnie (meist durch Völkerwanderungen übertragen) ebenso häufig auftritt, aber durch andere Ernährung oder Umwelteinflüsse seltener zu Krankheiten führt. Eine Laktoseintoleranz bei Ethnien, die sich wenig von laktosehaltiger Nahrung ernähren, führt dort also nicht zu einer "Volkskrankheit", eben weil der Umstand dort nicht auffällt. Für das Individuum ist die Nahrung ebenso wie das Rauchen oder der Alkoholkonsum für das persönliche Risiko maßgeblich.

Es gibt wohl auch Studien, die eine genetische Veranlagung für Mitgefühl, Offenheit gegenüber Neuem, Risikobereitschaft oder Konservatismus nachweisen wollen.

Auswirkungen der Mutationen werden häufig in Magnituden (1.0 - 4.0) angegeben. Gen-kodierte Charaktereigenschaften, blaue Augen, und rote Haar dürften die Magnitude 1.0 aufweisen, während Genmutationen, die zu einem angeborenen Herzfehler führen mit Magnitude 4.0 gekennzeichnet würden.

#### Die Guten

Manche Gene und deren Mutationen haben gute Eigenschaften. Die erst kürzlich beschriebene lebensverlängernde Mutation auf dem [FOXO3-Gen (odds ratio 1.35, CI:1.15-1.57, adjusted p=0.0093) ](https://www.snpedia.com/index.php/rs2149954) ist statistisch leider für Frauen lebensverlängernd. Andere "Methusalem"-Gene wie [LOC101927697](https://www.snpedia.com/index.php/rs2149954) sind nur geringfügig mit einer höheren Lebenserwartung (über 90 Jahre bei Mitteleuropäern) assoziiert. 

"Gute"-Mutationen können auch vor Infektionen wie dem SARS-CoV, oder HI-Virus schützen, bzw. eine schwere Erkrankung verhindern oder abschschwächen.

Hier sei aber anzumerken, dass dies nur für gesunde Personen gilt. Ist das Immunsystem z. B. durch eine andere Erkrankung oder durch eine medizinische Immunsuppression geschwächt, dürften diese positiven Eigenschaften nicht zur Geltung kommen. Ebenso ist es möglich, dass "gute"-Mutationen durch "normale" oder "schlechte" Mutationen relativiert werden. Zeigt FOXO3 ein langes Leben an, kann es durchaus sein, dass das [https://www.snpedia.com/index.php/rs3758391](SIRT1-Gen) gleichzeitig eine normale oder geringere Lebenswerwartung anzeigt.

Es ist also klar, dass man nicht auch durch andere Ursachen schon früh schwer erkrankt oder verstirbt.

#### Die Schlechten

Die allermeisten Menschen merken oder ahnen schon früh im Leben, ob sie eine bestimmte Mutation in sich tragen, auch wenn dies nicht durch ein Gentesz nachgewiesen wurde, weil sie schon seit kindesalter krank sind, oder weil ihre nahen Verwandten an Alkoholismus, Demenz oder ALS leiden. Wer mit drei Augen (Zyklop, Holoprosenzephalie) oder einem Hundeschwanz auf die Welt kommt, braucht wohl keinen Gen-Test. Zu den schlechten zählen aber auch viele Mutationen, die Rheuma, Arthritis, schlechte Augen im Alter, und erhöhte oder verringerte Reaktion auf Medikamente oder geringere Heilungsschancen bedeuten. Spricht man auf manche heute verfügbare Medikamentenwirkstoffe nicht an, kann eine Behandlung einer Krankheit schwierig werden. Manche Krebsarten haben zwar generell gute Heilungsschancen, aber eben nicht für alle, so dass sehr gut statistisch nachweisbar ist, ob man einen Krebs überlebt oder bereits nach 17 Monaten versterben wird.

Noch viel weniger Mutationen sind so gefährlich, dass sie lebensbedrohliche Krankheiten oder Fehlbildungen verursachen und das Leben erheblich verkürzen können oder zu einem frühen Tod führen.

Das Gute: Häufig ist es jedoch so, dass eine Veranlagung für eine Krankheit nur dann "aktiv" ist, wenn gleichzeitig eine bestimmte zweite Mutation in einem anderen Gen aufgetreten ist oder wahrscheinlich vererbt wurde. 

#### Die Häßlichen

Weniger häufig sind "unschöne" SNP. Zu den "Unschönen"-Mutationen zähle ich zum Beispiel die Hasenscharte. Diese kann früh operiert werden und verursacht dann im Laufe des Lebens keine Probleme mehr.

Menschen mit Farbschwäche wissen schon früh, dass sie eine Farbschwäche haben. Es beeinträchtigt ihr Leben zu einem Gewissen Maße, aber nicht so, dass man sie als "krank" bezeichnen würde. 

Eine Veranlagung für mehr Bauchfett ist je nach Ansicht auch unschön, kann aber möglicherweise im späteren Leben zu gesundheitlichen Problemen führen. Meistens lässt sich damit jedoch jahrzehntelang ohne Einschränkungen leben.

Die meisten Menschen haben wahrscheinlich für fast alles ein normales Risiko. In den meisten Fällen sind die Veranlagungen durch Beobachtung, Krankengeschichte oder aus der Familie hinreichend bekannt. Das Risiko für eine bestimmte Erkrankung im Laufe des Lebens ist selten höher als ein Vielfaches im kleinen einstelligen Bereich (z.B. 1,3-faches Risiko, also 30% höheres Risiko), was sich bereits auf ein geringes allgemeines Erkrankungsrisiko bezieht.

Zur häßlichen Wahrheit gehört auch, dass es zahlreiche Mutationen gibt, die z. B. für eine höhere Intelligenz, aber gleichzeitig das Risiko für psychische Krankheiten wie Depression, Schizophrenie oder all die anderen, die man so kennt, erhöhen können.

### Ich der Übermensch

Ich habe also meine genetischen Rohdaten bei einem Anbieter hochgeladen, der keinen erneuten Speicheltest verlangt, sondern maschinenlesbare Daten verwenden kann, wie sie Ancestry anbietet, sofern man dort einen DNS-Test gemacht hat.

Ich bin R1b1b2a1a1 [gs114](https://www.snpedia.com/index.php/gs114) und mt-Haplogruppe H-Europäer. [Mein Blut ist A-Negativ [rs8176719(I;D)](https://www.snpedia.com/index.php/rs8176719)
[rs8176746(C;C)](https://www.snpedia.com/index.php/rs8176746), [rs8176747(G;G)](https://www.snpedia.com/index.php/rs8176747), habe wahrscheinlich ein Typ-0-Allel von meinem Vater geerbt [rs8176719(I;D)](https://www.snpedia.com/index.php/rs8176719) und Duffy-positiv [rs2814778(A;A)](https://www.snpedia.com/index.php/rs198358), war einmal ziemlich blond [rs35264875(A;A)](https://www.snpedia.com/index.php/rs35264875) und bin ziemlich blauäugig [rs12913832(G;G)](https://www.snpedia.com/index.php/rs12913832) mit doppelkantigen Augenlidern [rs12570134(G;T)](https://www.snpedia.com/index.php/rs12570134) aber ohne Sommersprossen [rs1042602(C;C)](https://www.snpedia.com/index.php/rs1042602). Ich bin mit 1,84m etwas größer als der Durchschnitt [rs2070776(C;T)](rs2070776(C;T)) und [rs1042725(C;T)](https://www.snpedia.com/index.php/rs1042725(C;T)) und ein Überschall schneller Hetero-Igel [rs28936675(G;G)](https://www.snpedia.com/index.php/rs28936675)

#### Dement im Alter?

Zuerst suche ich nach Alzheimer und verwandten Demenz-Genen, die für eine Atropie bestimmter Hirnregionen, Plaque an Neuronen oder deren Demyelinisierung verwantwortlich sein können.

Als [rs1799990(A;G)]-Träger im PRNP-Gen habe ich ein geringeres Risiko für eine Creutzfeldt-Jakob- und einer späten Alzheimer-Erkrankung.

Ebenso deuten die Varianten der Gene: [rs7412(C;C)](https://www.snpedia.com/index.php/rs7412), [APOC1: rs4420638(A;A)](https://www.snpedia.com/index.php/rs4420638), [TOMM:40 rs2075650(A;A)](https://www.snpedia.com/index.php/rs2075650), [APOE: rs405509(A;C)](https://www.snpedia.com/index.php/rs405509), [CLU: rs11136000(C;T)](https://www.snpedia.com/index.php/rs11136000), [rs1800562(G;G)](https://www.snpedia.com/index.php/rs1800562), [rs3851179(A;A)](https://www.snpedia.com/index.php/rs3851179), [TREM2: rs75932628(C;C)](https://www.snpedia.com/index.php/rs75932628), [rs744373(C;T)](https://www.snpedia.com/index.php/rs1799990), [CR1: rs6656401(G;G)](https://www.snpedia.com/index.php/rs6656401), [rs2070045(T;T)](https://www.snpedia.com/index.php/rs2070045), [TF: rs1049296(C;C)](https://www.snpedia.com/index.php/rs1049296), [ABCA7: rs3764650(T;T)](https://www.snpedia.com/index.php/rs3764650), [CTSD: rs17571(C;C)](https://www.snpedia.com/index.php/rs17571), [rs610932(A;C)](https://www.snpedia.com/index.php/rs610932), [rs2302685(T;T)](https://www.snpedia.com/index.php/rs2302685), [PSEN2: rs63750215(A;A)](https://www.snpedia.com/index.php/rs63750215), 
[APP: rs63751039(A;A)](https://www.snpedia.com/index.php/rs63751039), [APP: rs63750264(G;G)](https://www.snpedia.com/index.php/rs63750264), [TAP2: rs241448(T;T)](https://www.snpedia.com/index.php/rs241448), [MTHFD1L: rs11754661(G;G)](https://www.snpedia.com/index.php/rs11754661), [PSEN1: rs63749824(C;C)](https://www.snpedia.com/index.php/rs63749824), [SORL1: rs11218343(T;T)](https://www.snpedia.com/index.php/rs11218343), [PSEN2: rs63749884(G;G)](https://www.snpedia.com/index.php/rs63749884), [PSEN2: rs63750110(A;A)](https://www.snpedia.com/index.php/rs63750110), nicht auf ein besonders erhöhtes Risiko einer Alzheimererkrankung hin, weder auf die, welche bereits um 45-50 Jahre beginnt, noch auf eine im höheren Alter.

Jedoch sind nicht akke "gut": [IL1A: rs1800587(C;T)](https://www.snpedia.com/index.php/rs1800587), [rs744373(C;T)](https://www.snpedia.com/index.php/rs744373), [CR1: rs3818361(C;T)](https://www.snpedia.com/index.php/rs3818361), [SERPINA3: rs4934(A;A)](https://www.snpedia.com/index.php/rs4934), [ATP8B4: rs10519262(A;G)](https://www.snpedia.com/index.php/rs10519262), [ATP8B4: rs10519262(A;G)](https://www.snpedia.com/index.php/rs10519262)

Viele der SNP die mit Demenz und/oder Alzheimer assoziiert sind, sind nur spekulativ und wenig untersucht oder deren Studien gar nicht oder nur einmal bestätigt.

### Ein langes Leben

Ein langes Leben ist möglich: [rs2149954(A;G)](https://www.snpedia.com/index.php/rs2149954), z. B. durch höhere Vitamin D-Werte im Blutserum: [rs2060793(G;G)](https://www.snpedia.com/index.php/rs2060793). Dank erhöhtem Vitamin A-Spiegel (Retinol) [rs1667255(A;C)](https://www.snpedia.com/index.php/rs1667255) bleibe daher bis ins hohe Alter faltenfrei.

Manche Variationen sind ein zweischneidiges Schwert. Zum Beispiel: Langes Leben oder Alzheimer [rs2075650(A;A)](https://www.snpedia.com/index.php/rs2075650). 

### Intelligenz

Dank leicht erhöhtem intrakraniellen Volumens [rs10784502(C;T)](https://www.snpedia.com/index.php/rs10784502) bin ich 2% intelligenter als der Durchschnitt. Leider hing ich nicht lang an der Brust, sonst hätte ich noch weitere 4 IQ Punkte dazu bekommen können [rs1535(A;G)](https://www.snpedia.com/index.php/rs1535).

### Gesundheit

Ich werde wohl keinen Herzinfarkt erleiden [gs297](https://www.snpedia.com/index.php/gs297) und [rs1063192(C;T)](https://www.snpedia.com/index.php/rs1063192) und auch sonstige Herzkrankheiten nicht bekommen [rs17672135(C;T)](https://www.snpedia.com/index.php/rs17672135). 

Ich habe ein gutes Gedächtnis [rs157582(G;G)](https://www.snpedia.com/index.php/rs157582). Ich bin kein Autist [rs3819331(T;T)](https://www.snpedia.com/index.php/rs3819331) und [rs4307059(C;C)](https://www.snpedia.com/index.php/rs4307059) auch kein Schizo [rs464049(C;C)](https://www.snpedia.com/index.php/rs464049). Ich leide nicht unter Angsterkrankungen [rs140701(G;G)](https://www.snpedia.com/index.php/rs140701) und es gibt kein Risiko für ADHD [rs806377(C;C)](https://www.snpedia.com/index.php/rs806377).

 
Das Risiko Asthma zu bekommen [rs4950928(C;G)](https://www.snpedia.com/index.php/rs4950928) liegt bei 50%. Ich werde wohl nicht an der Schilddrüse erkranken [rs180223(G;G)](https://www.snpedia.com/index.php/rs180223).

Ich kann mich dank viel gutem HDL [rs4149274(C;C)](https://www.snpedia.com/index.php/rs4149274), [rs4939883(C;T)](https://www.snpedia.com/index.php/rs4939883), [rs10503669(A;C)](https://www.snpedia.com/index.php/rs10503669), [rs4149268(G;G)](https://www.snpedia.com/index.php/rs4149268), [rs10468017(C;T)](https://www.snpedia.com/index.php/rs10468017), [rs1864163(A;G)](https://www.snpedia.com/index.php/rs1864163), [rs12678919(A;G)](https://www.snpedia.com/index.php/rs12678919) und [rs261332(A;A)](https://www.snpedia.com/index.php/rs261332), ernähren wie ich möchte.

Ich habe möglicherweise ein geringeres Risiko an kolorektalem [rs4143094(G;G)](https://www.snpedia.com/index.php/rs4143094), [rs4939827(C;T)](https://www.snpedia.com/index.php/rs4939827), Lungen- [rs11614913(T;T)](https://www.snpedia.com/index.php/rs11614913), [rs31489(A;C)](https://www.snpedia.com/index.php/rs31489),Bauchspeicheldrüsen- [rs3790844(C;T)](https://www.snpedia.com/index.php/rs3790844), Hoden- [rs995030(A;G)](https://www.snpedia.com/index.php/rs995030) oder Nierenkrebs und -tumoren [rs7105934(A;G)](https://www.snpedia.com/index.php/rs7105934) zu sterben. Ich habe ein geringeres Risiko für Basalzellkarzinome [rs7538876(G;G)](https://www.snpedia.com/index.php/rs7538876) und überhaupt ein geringeres Krebsrisiko dank C-reaktiver Protein Mengen [rs1892534(G;G)](https://www.snpedia.com/index.php/rs1892534). Auch Gehirntumore (und Glioma [rs55705857(A;A)](https://www.snpedia.com/index.php/rs55705857)) sind kein großes Risiko [rs699473(T;T)](https://www.snpedia.com/index.php/rs699473).

COPD, was auch immer das ist bekomme ich wohl auch nicht [rs1828591(A;G)](https://www.snpedia.com/index.php/rs1828591).

Angeblich leide ich weniger an einer Bandscheiben- oder Lendenwirbelsäulenproblemen [rs2073711(T;T)](https://www.snpedia.com/index.php/rs2073711) und habe ein geringeres Risiko für Gicht [rs1165205(A;A)](https://www.snpedia.com/index.php/rs1165205) und nur ein normales Risiko für rheumatische Arthritis [rs10818488(G;G)](https://www.snpedia.com/index.php/rs10818488). Weh tun, tun die Knochen und Gelenke trotzdem fast überall (hat sich wieder verbessert (2023)), aber dafür ein geringeres Risiko für eine Multiple Sklerose [rs3129934(C;C)](https://www.snpedia.com/index.php/rs3129934). Das ALS-Erkrankungsrisiko ist auch nicht erhöht [rs10260404(T;T)](https://www.snpedia.com/index.php/rs10260404).

Ich brauche keine Angst vor Lepra [rs5743618(G;G)](https://www.snpedia.com/index.php/rs5743618) und HIV [rs9264942(C;C)](https://www.snpedia.com/index.php/rs9264942) zu haben.

Parkinson [rs356168(A;A)](https://www.snpedia.com/index.php/rs356168), rs199533(C;C)[https://www.snpedia.com/index.php/rs199533], [rs10501570(T;T)](https://www.snpedia.com/index.php/rs10501570) und [rs6812193(C;T)](https://www.snpedia.com/index.php/rs6812193) habe ich von meinem Opa nicht geerbt. Ich bekomme dank normalen IGF-1-Spiegels [rs2229765(G;G)](https://www.snpedia.com/index.php/rs2229765) kein Diabetes 1 [rs9273363(C;C)](https://www.snpedia.com/index.php/rs9273363). Aber auch an Diabetes 2 werde ich nicht sehr früh erkranken [rs2295490(A;A)](https://www.snpedia.com/index.php/rs2295490).

Ich habe Knochen hart wie Stahl [rs7776725(T;T)](https://www.snpedia.com/index.php/rs7776725) und kleine aber starke Muskeln [rs2854464(A;A)](https://www.snpedia.com/index.php/rs2854464). Ich bin ein sportlicher Sprinter [rs1815739(C;T)](https://www.snpedia.com/index.php/rs1815739). Also, das war mal so. Für zwei Ehrenurkunden in der Schule hats gereicht, für einen Elite-Sportler jedoch nicht [rs7181866(A;A)](https://www.snpedia.com/index.php/rs7181866). Ich bin ganz und gar nicht aggressiv [rs909525(A;A)](https://www.snpedia.com/index.php/rs909525), vielleicht daher auch mein eher niedriger Blutdruck [rs198358(A;G)](https://www.snpedia.com/index.php/rs198358).


Autoimmunerkankungen sind nicht wahrscheinlich [rs763361(C;C)](https://www.snpedia.com/index.php/rs763361).

Ich soll auch angeblich keine Glatze bekommen [rs1385699(C;C)](https://www.snpedia.com/index.php/rs1385699), keine Migräne kennen [rs11172113(C;C)](https://www.snpedia.com/index.php/rs11172113). Seit Mitte 30 wird es da oben jedoch deutlich übersichtlicher.

Meine Eierstöcke bleiben vor Krebs verschohnt [rs4320932(A;G)](https://www.snpedia.com/index.php/rs4320932) und werde keinen Brustkrebs bekommen [rs3750817(T;T)](https://www.snpedia.com/index.php/rs3750817).


**1. Hinweis** 

In Deutschland gibt es bisher keine routinemäßige Genanalyse nach (diesen) oder anderen spezifischen Genmutationen. Sie werden nur dann durchgeführt, wenn [familiäre Häufungen aufgetreten sind](Familienanamnese für z.B. BRCA-1/-2-Mutationen). Die Häuigkeit in der Familie (bisher eine Person pro Generation) ist deutlich unter dem, was vorausgesetzt wird. Zweitens wären alle meine unbekannten Kinder nicht in der Lage eine Verwandtschaft nachzuweisen, da ich wahrscheinlich ein guter Liebhaber und Samenspender war, jedoch aber nicht als Vatermaterial identifiziert wurde.

Eine ancestry.com + promethease.com Analyse kostet ingsesamt unter 100€. Sollte es Verwandte (müttlich) geben, also sowohl Kinder als auch aus Verzweigungen vorheriger Generationen (Herold), wäre ein Test durchaus sinnvoll.

**2. Hinweis** 

Andere Studien widerlegen teilweise die Eigenschaften der o. G. So habe ich anderen Studien nach ein erhöhtes Risiko an einer koronaren Herzkrankheit zu erkranken [rs1333049(C;G)](https://www.snpedia.com/index.php/rs1333049), einen Schlaganfall [rs2943634(C;C)](https://www.snpedia.com/index.php/rs2943634) zu erleiden und doch ene Glatze zu bekommen [rs2180439(T;T)](https://www.snpedia.com/index.php/rs2180439). Möglicherweise an Prostata- [rs2107301(T;T)](https://www.snpedia.com/index.php/rs2107301), Lungen- [rs17487223(T;T)](https://www.snpedia.com/index.php/rs17487223) oder anderen Krebsarten zu erkranken [rs664143(C;T)](https://www.snpedia.com/index.php/rs664143). Studien über geringen Blutdruck wird mit einer Studie über erhöhten Blutdruck [rs891512(A;G)](https://www.snpedia.com/index.php/rs891512(A;G)) widerlegt. Aber auch was die Knochen- und Muskelmasse und Intelligenz betrifft, finden sich zahlreiche Studien die mal das eine mal das andere Behaupten.
 
Auch zeigen manche andere Studien eher ein Bild, dass sich mit meiner Gesundheitslage eher deckt. Zum Beispiel das Risiko einer rheumatischen Arthritis [rs7574865(G;T)](https://www.snpedia.com/index.php/rs7574865) und [rs3738919(C;C)](https://www.snpedia.com/index.php/rs3738919) und Osteoarthritis [rs143383(C;T)](https://www.snpedia.com/index.php/rs143383). Auch wenn bei mir außer schlechter werdender Augen nichts diagnostiziert wurde, tun mir teils Knochen und Gelenke weh und es ist auch in der Familie nicht unbekannt.

Auch die Todesursachen einiger Vorfahren lassen darauf schließen, dass einige Studien weniger korrekt sind als andere. Insgesamt sind jedoch viele meiner Vorfahren eher älter als zu jung gestorben (Ü80).

Es ist auch nicht immer klar, ob alle Ethnien betroffen gleichermaßen betroffen sind und welche anderen Voraussetzungen vorhanden sein müssen. Ich trinke kaum Alkohol, rauche nicht und arbeite auch nicht in einer Kohlegrube. Auch ist es so, dass bei vielen dieser Mutationen, teilweise 40%-50% der Bevölkerung das Risiko tragen. Es also maßgeblich von individuellen Umständen abhängt.

Allgemein kann man sagen, dass einige Studien sehr eindeutig sind und es auch keine widerlegenden Studien gibt. Bei manchen Krankheiten sieht es jedoch anders aus. Bei denen gibt es für jede Studie die A meint und gleich viele die das Gegenteil herausgefunden haben sollen. 

Man muss wirklich genau lesen. Wenn eine Studie besagt, dass für eine Haplogruppe ein erhöhtes Hautkrebsrisiko besteht, dann vergleicht sie das Risiko mit anderen Haplogruppen. Haplogruppen sind einzelne Mutationen, die sozusagen eine Ethnie definieren können. Die allermeisten Europäer haben eine von sechs Haplogruppen. Dabei fallen drei auf die Mittel und Nordeuropäer (die blonden und blauäugigen), und der Rest auf dunkelhaarige Mittel- und Südeuropäer. In Deutschland sind mit ausnahme der Zuwanderer überwiegend drei Haplogruppen vorherrschend R1b, R1a und I, wobei I auf die blonden Skandinavier, Holsteiner und Friesen hinweist.

Es ist also nicht ungewöhnlich, dass Studien aussagen, dass manche Haplogruppen ein höheres Risiko in sich bergen. Das individuelle Risiko innerhalb der Haplogruppe kann jedoch auf Grund anderer Mutationen von der Norm abweichen.

#### Nicht alles Gold was glänzt

Laut Wikipedia stellen Mutationen auf den Genen BRCA-1 und BRCA-2 ein erhöhtes Risiko dar, an Brust-, Pankreas- und anderen (Unterleibs)-Karzinomen zu erkranken. Brustkrebs oder Ovarialkarzinome sind in der Familie, bzw. mir gänzlich unbekannt. In den letzten drei Generationen sind jedoch Krebsarten aufgetreten, die insbesondere den Magendarmbereich und die Bauchspeicheldrüse betreffen.

Laut promethease.com bin ich Träger dieser Mutationen, sowohl auf dem BRCA-1 als auch auf dem BRCA-2-Gen. Ich vermute, dass die erkrankten in den vorherigen drei Generationen (mütterlicherseits) auch diese Mutation aufwiesen. Es besteht also ein deutlich erhöhtes Risiko für mich und auch für mögliche, bisher unbekannte Kinder, an einer dieser Krebsarten zu erkranken. Sowohl Männer als auch Frauen waren in der Familie betroffen. Es sind aber keine Krebsarten bekannt, die bei Frauen eher auftreten, wie zum Beispiel Brustkrebs. Man erkrankt meist erst über dem 60. Lebensjahr. Noch etwas positives:

1) In der Familie wurde die Erkrankung sogar erst mit weit über 70 und 80 Jahren entdeckt. In diesem Alter ist es also durchaus möglich, dass man bereits vorher an einer beliebigen anderen Ursache (z.B. "tödliche Coronaviren") gestorben ist.

2) Die Häufigkeit der Mutation in der mitteleuropäischen Bevölkerung beträgt für rs16942(A;G) über 45%, für rs144848(G;T) 40% und für rs1799966(A;G) 46%. Da jedoch nicht über 45% an diesen Krebsarten erkranken, ist es ein Hinweis, dass der persönliche Lebensstil maßgeblich eine wichtige große Rolle spielt. Nur die Mutation rs1799950(A;G) ist mit einer Häufigkeit von 7,7% ein "besonderer" Risikofaktor. Diese Mutation ist aber nur in sehr geringem Maße mit einem deutlich erhöhten Erkrankungsrisiko verbunden.

### Über Studien

Die Zuverlässigkeit der Studien ist zum Teil sehr unterschiedlich, sowohl in der SNP-Forschung allgemein als auch zwischen verschiedenen Studien zu ein und demselben SNP.  Einige Studien über Mutationen und ihre Auswirkungen auf Körper und Geist werden bis zu 20 Mal von anderen Forschern bestätigt oder widerlegt. Bei anderen Studien ist dies nicht der Fall.

Das bedeutet jedoch nicht, dass eine unbestätigte Studie falsch oder richtig ist. Es bedeutet nur, dass sie aus wissenschaftlicher Sicht weder falsifiziert noch bestätigt wurde und daher keine Behandlungs- oder Handlungsempfehlungen abgeleitet werden können.

Manche Studien veralten mit der Zeit. So kann es durchaus vorkommen, dass eine Studie mehrfach bestätigt wurde und die Wissenschaft erst viel später zu gegenteiligen oder abweichenden Ergebnissen kommt, weil die Datenlage bisher vielleicht nicht ausreichend war oder die Messmethoden verbessert oder verändert wurden.

Manche Studien werden aus politischen Gründen aus dem wissenschaftlichen Diskurs verbannt oder nicht finanziert. Die Frage, ob jemand unter bestimmten Bedingungen eine Veranlagung zur Homosexualität entwickelt oder nicht, stellt sich für die meisten Wissenschaftler nicht. Die Existenz eines solchen Gens oder SNPs wird also nicht untersucht. Hingegen könnte das Wissen um eine gewisse Gewaltbereitschaft durchaus von politischem Interesse sein.

Bestimmte Mutationen, wie z.B. diejenige, die für eine Netzhautablösung verantwortlich sein könnte, treten bei bestimmten Ethnien so deutlich häufiger auf, dass die Beantwortung der Frage, ob eine Veranlagung für eine Netzhautablösung vorliegt, Rückschlüsse auf die Ethnie geben kann.

Die Gendiagnostik bietet große Chancen. So weiß ich heute, dass ich aufgrund von Mutationen, die ich in mir trage, bestimmte Nahrungsmittel schlechter oder besser verwerten kann. Durch Nahrungsergänzungsmittel, den Verzicht auf bestimmte Inhaltsstoffe oder die vermehrte Zufuhr bestimmter Stoffe kann ich die Defizite meines Stoffwechsels teilweise ausgleichen, auch wenn es nur brüchige Fingernägel, schlechtere Augen oder dünnes oder brüchiges Haar sind.

Warum wird die genetische Diagnostik nicht von den Krankenkassen bezahlt? Es gibt doch Studien, die besagen, dass Mundschutz und mRNA-Impfungen vor Ansteckung und Verbreitung von Viren schützen. Dafür gibt es doch auch Geld.

Warum soll eine Studie über die genetische Veranlagung zur Homosexualität schlecht sein? Wenn man sich heute das Geschlecht aussuchen kann, warum sollte man dann nicht auch die sexuelle Orientierung durch Gentherapie verändern können? Natürlich ist das noch nicht möglich, aber wenn es so weit wäre, wäre das ein großer Schritt.

Was spräche dagegen? Es wäre die natürlichste Sache der Welt, so wie das Tragen einer Maske, fünf kleine Piks für die Freiheit oder eine Kunstsprache für die politische Korrektheit, und das ganz ohne Zwang.
