---
title: Der Datenautist
layout: default
category: phantasia
tags: "Cornelius, Römer, twitter, wissenschaftlich, Datenanalyst, Listen, PR-Aktion, Netzwerke, extremistisch, Durchseuchen, quer"
---
Der angebliche Datenwissenschaftler [Cornelius Römer](https://nitter.net/CorneliusRoemer) stolpert am 21.4.2021 bei Twitter zufällig auf Beiträge der Aktion ["#allesdichtmmachen"](https://nitter.net/search?f=tweets&q=%23allesdichtmachen) und erkennt messerscharf, dass es sich hierbei um eine [koordinierte PR-Aktion](https://nitter.com/CorneliusRoemer/status/1385390024594706432) handeln müsse.

Liest man sich seine [folgenden Tweets](https://web.archive.org/web/20210423002844/https://twitter.com/CorneliusRoemer/status/1385390024594706432) zu dem Thema durch, kommt dem normalen Menschen (heutzutage als "Rechtsextreme" betitelt) das Kotzen. Überhaupt kommt einem bei Personen wie Römer immer das Kotzen, wenn man zufällig was von diesen Personen liest. Die Welt wäre so viel besser dran, wenn diese Drecksschweine ihre Arbeit einfach leise in ihrem kleinen Kämmerlein durchführen würden.

<!-- more -->
Wie es in hochwissenschaftlichen Kreisen anscheinend üblich ist, ruft der sogenannte Datenanalyst [Cornelius Römer](https://nitter.net/CorneliusRoemer) daraufhin zum wiederholten Mal auf (der übrigens mit seinen Prognosen wie viele anderen fast immer falsch lag), [Listen von "Durchseuchern" zu sammeln](https://www.berliner-zeitung.de/news/lockdown-physiker-will-listen-von-schauspielern-und-followern-li.154988), angeblich um extremistische Netzwerke aufzudecken. Potenzielle Extremisten sind demnach alle, welche die Beiträge der Aktion [#allesdichtmachen auf Twitter](https://nitter.net/search?f=tweets&q=%23allesdichtmachen) geteilt hätten. 

Ich, der ungeimpfte, ungetestete Schwurbler und einer von ca. 8 Milliarden Überleber der allergrößten und schlimmsten Pandemie seit Bestehen des Universums erkenne darin jedoch deutliche faschistoide Tendenzen, daher habe ich zur Aufdeckung und Bestrafung von Faschisten und faschistoiden Linksschwurblern Anzeige erstattet, die sich in erster Linie gegen Cornelius Römer richtete und gegen eine zweite Person, deren Namen ich nicht kannte und die Intention nicht klar war.

In einem Tweet kurz darauf tut der faschistoide Datenautist so, als wäre alles nur ein Missverständnis. Sein Sprachgebrauch und Nutzung von Wörtern wie "Durchseucher", "Leugner", "Querdenker" ist in diesem Zusammenhang völlig in Ordnung. Er möchte erst jetzt Zusammenhänge ausfindig machen. Denn Extremisten gibt es für schizophrene Psychotiker überall, an jeder Ecke.

Er ist sich auch nicht der Tragweite seines Handelns bewusst, nachdem aufgrund seines Aufrufs ["Todeslisten"](https://nitter.net/CorneliusRoemer/status/1443334700701978626) von teilenden Personen angefertigt wurden, nur Tage nachdem er aufruft Listen anzufertigen.

    "Wenn jemand eine Liste machen will, hier drunter gecrowdsourct? [@pkreissel](https://nitter.net/pkreissel) kann sowas gut analysieren."

Philip Kreißel, der Bayer, Faschist und Propagandist beim lügenverbreitenden Blog Folksverbetza, dessen Arbeit darin besteht, alle als Querddenker:innen, Rechte, Nationalsozialist:innen und Lüg:nende zu bezeichnen, natürlich ohne Beweise, dafür mit umsomehr Pathos: "Kreml-Lügen widersprechen ist Bürgerpflicht!". Das nennt man heutzutage "wissenschaftlich". Zudem mschürt er Hass und verhetzt die Bürger des Landes. Asoziale Drecksschweine dürfen das.

    "Macht eine Liste... Spürt sie auf..., Treibt sie zusammen"
	
<small>(Antwort auf Cornelius Römers und Philip Kreißels Aufruf zum Erstellen von Listen von Kritikern)</small>

Manche gute Mensch:enden können sich halt immer herausreden. All das scheint aber in Ordnung zu sein, Römer wird schließlich von der richtigen Seite finanziert, von denen, die diese Scheindemokratie und Rechtsstaat am Leben halten, um ihre faschistoide Idee durchzusetzen, während tausende Wissenschaftler und unbescholtene Bürger mit 2G- und 3G-Regelungen zu einer Impfung gezwungen werden und von Medien, Politik und anderen Drecksfaschos und Weltenrettern beleidigt und existenziell zerstört werden. Wahrscheinlich wird Römer von Bill Gates finanziert. Hat jemand Informationen und kann die Netzwerke um Römer aufdecken, es gibt doch gute Datenbanken.

Das Anlegen von Listen von extremistischen Akteuren (die Definition obliegt offensichtlich jedem selbst) ist wohl für alle Forscher und Forsch:enden, mit oder ohne Qualifikation erlaubt. Es ist davon auszugehen, dass alle Personen, die Cornelius Römer folgen, Faschisten sind. Ich rufe hiermit alle Forscher und Forsch:enden auf faschistische Netzwerke um die Person Römer aufzudecken und distanzieren sie sich umgehend von dieser gefährlichen Person.

Aus der <a data-bs-toggle="collapse" href="#staatsanwaltschaft" role="button" aria-expanded="false" aria-controls="#staatsanwaltschaft">Antwort der Staatsanwaltschaft [Bildergalerie]</a>, die sich auf ein Ermittlungsverfahren gegen P.K. bezieht, geht nicht hervor, ob das Verfahren gegen Cornelius Römer oder die anderen Personen noch läuft. Wahrscheinlich nicht. 

Die Begründung zur Einstellung des Ermittlungsverfahrens kann man sich hier durchlesen:

<div class="row collapse" id="staatsanwaltschaft"> 
	<div class="col col-12">
        <div class="card card-body" style="mh-77">
		  	<div id="staatsanwaltschaftCtrls" class="carousel slide" data-bs-ride="carousel">
		  		<div class="carousel-inner" style="background-color: #333">
					<div class="carousel-item active">
						<div class="carousel-caption d-none d-md-block">
							<h5>Cornelius Römer</h5>
							<p>Der Datenanalyst</p>
					  	</div>
						<div class="d-flex justify-content-center">
					  		<img src="/assets/images/staatsanwaltschaft_1.jpg" alt="Cornelius Römer">
				  		</div>
					</div>
					<div class="carousel-item">
						<div class="carousel-caption d-none d-md-block">
							<h5>Cornelius Römer</h5>
							<p>Der Datenanalyst</p>
					  	</div>
						<div class="d-flex justify-content-center">
					  		<img src="/assets/images/staatsanwaltschaft_2.jpg" alt="Cornelius Römer">
				  		</div>
					</div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#staatsanwaltschaftCtrls" data-bs-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Vorheriges</span>
			  	</button>
			  	<button class="carousel-control-next" type="button" data-bs-target="#staatsanwaltschaftCtrls" data-bs-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Nächstes</span>
			  	</button>zum wiederholten mal auf 
            </div>
        </div>
    </div>
</div>

Es wird ziemlich deutlich, dass es Zeit wird auszuwandern, nur wohin? Meine Datenanalyse prognostiziert, dass es ganz böse enden wird. Ich freu mich drauf.