---
title: Die Bush-Doktrin
category: ego
layout: default
tags: "ego, Irak, Krieg, NATO, Geilenkirchen, DtA, Völkerrecht, Viren, Russland"
---
Irgendwann um den Beginn des Irak-Krieges herum findet auf dem NATO-Stützpunkt Geilenkirchen eine Informationsveranstaltung für den Deutschen Anteil statt.

Ein Stabsoffizier oder General informiert uns über die neue so genannte ***[Bush-Doktrin](https://de.wikipedia.org/wiki/National&lowbar;Security&lowbar;Strategy&lowbar;vom&lowbar;September&lowbar;2002)***, die einen Präventivkrieg legitimieren soll. Das bedeutet, dass die USA in Zukunft, wenn sie es für notwendig erachten, ausgewachsene konventionelle Kriege beginnen werden, wenn sie davon ausgehen, dass die Sicherheit der Vereinigten Staaten von Amerika unmittelbar (völkerrechtlich gedeckt), vor allem aber auch in der Zukunft (völkerrechtswidrig) bedroht erscheint. Ebenso wird eine Strategie für die Eindämmung Russlands und Chinas (Containment-Strategie) verfolgt. Zumindest schriftlich festgehalten ist eine "Impfung gegen ABC-Kampfstoffe". Die Medien werden in den nächsten Jahren viele neue Gefahren (Anthrax, EHEC) und Pandemien erkennen und regelmäßig die Fachbezeichnungen der Grippestämme (H1N1, H5N3, etc.) kommunizieren.

**Ausblick**

Der Irak-Krieg wurde mit hanebüchenen Begründungen (Massenvernichtungswaffen, mobile ABC-Kampffahrzeuge) legitimiert, die sich im Nachhinein als vorgeschoben erwiesen haben.

Auf der einen Seite habe ich es jetzt mit Leuten zu tun, die sagen "Ja, der Saddam, der muss weg, so oder so", egal ob er jetzt eine Gefahr darstellt oder nicht, auf der anderen Seite werde ich in der Diskussion darüber indirekt als Mörder bezeichnet.

**Retrospektive**

Zumindest wurde man zu dieser Zeit noch nicht als Nazi bezeichnet, wenn man eine abweichende Meinung hatte. Die gute alte Zeit, als die meisten Spinner noch kein Internet hatten und einfach nur die eigene Familie oder die Kneipe vollgesülzt haben. 20 Jahre später werden hochkarätige Politiker von Format, aber auch Politikbetreib:ende ohne Format, dafür ohne Ausbildung und Perspektive sich damit erniedrigen, das Volk über Kurznachrichten zu beleidigen.

Die Soldaten, mit denen ich mich außerhalb der Arbeit abgebe, sehen die Bush-Doktrin jedoch kritisch, ebenso wie der Stabsoffizier / General der den Vortrag gehalten hat.
