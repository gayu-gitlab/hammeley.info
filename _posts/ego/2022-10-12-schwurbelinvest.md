---
title: Schwurbelinvest
category: ego
layout: default
---
Wenn ein Schwurbler entgegen aller Warnungen, Prognosen und Ankündigungen seine hart verdienten Euros gegen eine weiche Währung wie dem Rubel tauscht, bei einer Wechselstube, die miserable Wechselkurse anbietet und oben drauf einem noch eine pauschale Gebühr in Höhe von 10,00€ abknöpft, das Land des Rubels einen Krieg beginnt, worauf hin die gesamte westliche Welt gegen das Land des Rubels Sanktionen verhängt und in den Wahrheit verbreitenden Medien von Superdeutschland vom schnellen Untergang des Rubellandes, seiner Wirtschaft und des Militärs berichtet und schwadroniert wird und der Schwurbler demnächst trotzdem einen Gewinn einfährt (Inflation und Gaskosten nicht eingerechnet), dann sollten alle Schwurbler der Welt wegen ihrer Schwurbelei stolz auf sich sein.

Vielleicht tauscht der Schwurbler die Rubel auch einfach in polnische Zloty um, und geht in Polen einkaufen. Der Zloty hat gegenüber dem Rubel seit 2020 deutlich abgewertet. Da ist sogar der Preis für ein 49€ Ticket schon mit drin. Es würde sich aber sicherlich auch lohnen, die Rubel in USD, bzw. in USD-Stable-Coins wie BUSD oder USDT zu tauschen. Dort gibt es derzeit ganze 8% Zinsen (p.a. im Tagesgeld).

![Schwurbelinvest Deluxe](){:data-src="/assets/images/rubel.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

Vielleicht haben ja ein paar Nichtschwurbelnde den Rat des Schwurblers angenommen und ein paar Rubel gekauft, zumindest zu dem Zeitpunkt, als die Währung kurzzeitig abgeschmiert ist.

**Hinweis (für die Stasi, den Staatsschutz und Moralapostel):** Der V.i.S.d.W. verfügt über keinerlei Rubelreserven, hat nie über Rubelreserven verfügt, und wird auch niemals über Rubelreserven verfügen. Die einzige wahre Währung ist der Euro (und Krypto). Wirklich.
