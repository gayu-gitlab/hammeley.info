---
title: Deine Gene sind meine Gene und meine sind Dein
category: ego
layout: default
deepl: 2023-02-19
tags: "Ahnen, Familie, Aufzeichnung, Forschung, Digitalisierung, family, ancestry, Tschudi, Hammeley, Schweiz, Aegidius, Staatsbibliothek, Schlageter, von Faber, Wellschmidt, Amerika, Weltkrieg, Ripperger, Kollektivbeleidigung, 1910, Blessing"
---
Ich betreibe nun seit einiger Zeit Ahnenforschung.

Ich begann mit der Befragung der Familie und durchsuche Aufzeichnungen, die meine Forschungen in Teilen bis ins 18. Jahrhundert bringt.

Später nutze ich die Möglichkeiten der Digitalisierung bei familysearch.org und ancesty.de und der genetischen Diagnostik.

**Ausblick:**

Wie erwartet stammen meine Vorfahren überwiegend aus drei Gegenden.

*Mütterlicherseits* kommen die näheren und nachweisbaren Vorfahren überwiegend aus:

1. dem Schwarzwald und dem Schweizer Baselland
2. der Schwäbischen Alb um Blaubeuren
3. Oberfranken rund um Schwarzenbach an der Saale
4. (mit Ausläufern nach Thüringen und Sachsen)

Aus der Schweizer Linie, die im Schwarzwald um Sulzburg / Laufen siedelte, ist ein Burkhardt Tschudin als erster von Lausen in der Schweiz in das heutige Deutschland eingewandert.

Daten aus oder vor dem 17. Jahrhundert sind oft nicht vorhanden, schwer zu entziffern oder schwer zuzuordnen. Sie können auch gefälscht sein.

Diese Schweizer Linie mit den Familiennamen ([**Schudy, Schudi, Schudin, (von) Tschudi oder Tschudin, amerikanisiert auch Judy**](https://de.wikipedia.org/wiki/Tschudi_(Familie)) ist aber sehr gut dokumentiert. Natürlich gab es aber auch schon vor 1000 Jahren uneheliche oder untergeschobene Kinder und Waisen, die mit einem anderen Familiennamen aufwuchsen. Daher sind die Angaben, wenn überhaupt, nicht immer korrekt oder eindeutig. Zudem kann die Frage nach der Herkunft unterschiedlich beantwortet werden. 

Sicher scheint jedoch, dass der Familienname in all seinen Variationen auf einen Vorfahren zurückgeht, der erstmals mit diesem Namen im Kanton Glarus in Erscheinung trat. Es handelt sich also nicht um einen Familiennamen, der zufällig an verschiedenen Orten entstanden sein könnte, ohne dass eine Verbindung zwischen den Familien besteht. Alle Personen mit einer dieser Namensvarianten sind miteinander verwandt.

<!-- more -->

Mit an Sicherheit grenzender Wahrscheinlichkeit ist ein Bruder des Großvaters von [Aegidius "Gilg" Tschudi] (https://de.wikipedia.org/wiki/Aegidius_Tschudi) einer meiner direkten Vorfahren. Der im [Jahrbuch des deutschen Adels, 1898, digitalisiert von der Russischen Staatsbibliothek](https://viewer.rsl.ru/ru/rsl01004441804?page=916&rotate=0&theme=white) genannte Heinrich Tschudi, ist mein 16-facher Urgroßvater.

Unsicherer ist eine Verbindung um 1326, als Heinrich III. Tschudi, der Urgroßvater meines 16-fachen Urgroßvaters, eine Katharina von Port geheiratet haben soll. Diese soll laut Urkunden ein Nachkomme der "von Faber" gewesen sein, die heute vielleicht noch durch die Bleistifte von Faber & Castell aus Stein in Franken bekannt sind.

Ebenso tauchen die Familiennamen Sprecher von Bernegg, von Seedorf, von Windegg, von Buerglen und andere bekanntere Namen dieser Zeit auf.

Noch unsicherer sind die Aufzeichnungen vom 12. bis zum 9. Jh. Hier sind sich die Historiker nicht einig, inwieweit die Aufzeichnungen wahr oder erfunden sind, um sich eventuell Vorteile zu verschaffen. Neuere Erkenntnisse deuten jedoch darauf hin, dass die Angaben durchaus der Wahrheit entsprechen.

**Haben und Sein**

Ein Teil der Vorfahren dieser Linie dürfte an der schweizerischen Reformation und Gegenreformation (Calvin, Rütli, Zwingli) beteiligt gewesen sein.

Im 20. Jahrhundert soll die Schwarzwälder Linie auch mit den Schlageter (evtl. Lina Schlageter oo Tschudin) verbunden gewesen sein. Es soll bis in die 1960er Jahre noch Kontakt zwischen "den" Schlageters und meinem Großvater bestanden haben, der zu dem Zeitpunkt bereits in der Nähe von Ulm wohnte.

Auf der Seite meiner Großmutter mütterlicherseits (aus Franken) war die Tochter (A. Wellschmidt geb. Ripperger) einer Schwester meiner Urgroßmutter (Seidel) immerhin noch mit dem Kunstmaler [Helmut Wellschmidt](https://helmutwellschmidt.de/gemaelde) verheiratet. Seine späteren Werke erinnern mich als Laien ein wenig an Hieronymus Bosch oder [Felix Nussbaum](https://de.wikipedia.org/wiki/Felix_Nussbaum).

Auch in und um Berlin finden sich noch wenige Artefakte der Familie "(von) Tschudi". [Hugo von Tschudi](https://de.wikipedia.org/wiki/Hugo_von_Tschudi), der zeitweise Direktor der Nationalgalerie in Berlin war wurde eine Straße in Berlin-Kaulsdorf gewidmet. Nach dem Förderer der Luftfahrt [Georg von Tschudi](https://de.wikipedia.org/wiki/Georg_von_Tschudi) wurde die gleichnahmige Straße am Flughafen BER und [Potsdam](https://www.tagesspiegel.de/potsdam/landeshauptstadt/wer-war-eigentlich-tschudi-7569892.html) benannt.

*Väterlicherseits* kommen die meisten näheren nachweisbaren Vorfahren:

- aus dem Stuttgarter Raum (südlich bis Herrenberg, Holzheim, Nellingen, Reutlingen, Kirchheim unter Teck), aber auch über Göppingen (Schlat) bis nach Ulm bei den neueren Generationen.
- aus dem Raum Cottbus

Die Linie meiner Großmutter väterlicherseits kann ich nur bis zu ihren Eltern zurückverfolgen (über die Geburtsurkunde meiner Großmutter). Meine Großmutter muss meinen Großvater in Cottbus kennengelernt haben, als dieser dort unter anderem als Treuhänder der Tuchfabrik "Erich Schröder und Co. KG" an der Jägerbrücke 2 tätig war. Er selbst wurde in Ulm geboren und kann meine Großmutter nur durch seinen Aufenthalt in Cottbus kennengelernt haben.

Auch die bisherigen Nachforschungen auf der Seite meines Großvaters väterlicherseits, Karl-Gustav Hammeley, enden bereits um 1795 bei meinem Ururgroßvater (Josef Hammeley oo Barbara Maunz). Die Familienteile Maunz, Lohmann, Steinmez, Thumm u.a. lassen sich aber mindestens bis ins 16. Jahrhundert zurückverfolgen.

Interessanterweise gibt es zu Beginn des 20. Jahrhunderts sowohl einen bekannten Karl Hammeley als auch einen bekannten Theodor Maunz.

Dr. Karl Hammeley beschäftigte sich um 1910 in seiner preisgekrönten Dissertation mit den Themen "Kollektivbeleidigung" (z.B. Soldaten sind Mörder oder Bullenschweine) und "Verächtlichmachung" (u.a. von Staatsoberhäuptern). Ich habe die 43-seitige und auch für Laien kurzweilige Abhandlung über die Beleidigung im Allgemeinen und die Kollektivbeleidigung im Speziellen aus der Staatsbibliothek in Berlin und stelle es hier als [eingescanntes und komprimiertes, aber lesbares PDF (14,24MB)](/assets/Dr-Karl-Hammeley_Die-Kollektivbeleidigung_compressed.pdf). Lesen lohnt sich. Gerade in diesen Jahren (2021-2023) ist das Buch aktuell wie nie. Bin sogar etwas stolz. :)

Theodor Maunz, war nach der Gründung der BRD ebenso ein prägender Verfassungsrechtler, Professor an der LMU und bayerischer Kultusminister. Einer seiner Schüler war u.a. der spätere Bundespräsident Roman Herzog. Obwohl Teile der Familie in bayerische Familien um Geyern eingeheiratet und die Namensübereinstimmung des "Findelkindes" Josef Hammeley zu deutlich ist, um nicht nachzuforschen und es in und um München lebende genetische Verwandte gibt (z.B. Adams), konnte bisher keine Verbindung zu diesen beiden Personen nachgewiesen werden.

### Auswanderungen

Viele Vorfahren sind zwischen dem 18. und 20. Jahrhundert in die USA ausgewandert und besiedeln heute zahlreiche Bundesstaaten (Kalifornien, Colorado, Iowa, Missouri, Wisconsin, Illinois, Tennessee, Ohio, Florida, Virginia, Pennsylvania, Upstate New York und New York City). 

Einige Einwohner von Ohio kennen vielleicht [Louis W. III Blessing](https://www.youtube.com/watch?v=qsjDSDcw0Eg) und Lou Blessing, die beide erfolgreiche Senatoren in Ohio sind bzw. waren. Diese stammen von Leonhard Blessing (meinem Ur-Ur-Großonkel) aus Schlat (Göppingen) ab, dessen Schwester (meine Ur-Ur-Großmutter) meinen Ur-Ur-Großvater heiratete. Dieser ließ sich zunächst in Cincinnati (Ohio) nieder.

### Familienaufzeichnungen

Wenn man Ahnenforschung betreibt, findet man oft alte Dokumente, Schulzeugnisse, Telefonbucheinträge, Urteile, Fotos, Tagebücher oder Archivbestände usw., die interessante Geschichten erzählen.

Mein Urgroßvater (mütterlicher Großmutter) hat z. B. in Retrospektive seine Erfahrungen seiner Odyssee in Kriegsgefangenschaft während des Ersten Weltkriegs aufgeschrieben.

![Stationen in russischer Gefangenschaft im Ersten Weltkrieg](/assets/images/gefangenschaft.jpg){:style="display:block; margin-left:auto; margin-right:auto; max-width: 100%"}
|: Stationen in russischer Gefangenschaft im Ersten Weltkrieg :|

