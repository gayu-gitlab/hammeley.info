---
title: Neues Leben
category: ego
layout: default
tags: "ego Leben, Spielbank, Vater, Berlin"
---
Ich kriege noch nicht viel mit von der Welt da draußen, aber das ist sie im Jahr 1978:

[Video: Berlin im Jahr 1978](https://www.youtube.com/watch?v=dL83w0fvae0)

**Ausblick:** In den nächsten Jahren werde ich wohl erfahren, dass ich in der ausgebomten Kirche (4:03), bzw. in dem Neubau daneben getauft wurde. In dem Gebäude mit den bunten Lichtern und den vielen Geschäften (3:37) scheint wohl mein Vater zu arbeiten. Die Leute gehen da abends hin, legen Geld auf den Tisch und hoffen, dass sie mehr Geld zurück bekommen, als sie hingelegt haben. Das klappt wohl meistens nicht.
