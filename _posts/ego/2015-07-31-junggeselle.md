---
title: Junggeselle der Wissenschaft für Rechnen, Informationstechnologie und Geschäft
category: ego
layout: default
deepl: 2023-02-19
---
Ich habe nun auch die Abschlussarbeit bestanden und die Urkunde erhalten, die mich offiziel zum "Bachelor of Science (Honours) in Computing, Information Technology and Business" macht.

Damit der Abschluss auch in Deutschland offiziell anerkannt wird, kostet das noch einmal 300€ für die KMK. Das ist notwendig, um weiter zu studieren oder im öffentlichen Dienst zu arbeiten.

So ein Abschluss gibt schon ein gutes Gefühl, aber das wird nur von kurzer Dauer sein. Im IT-feindlichen Deutschland nützt das Diplom nichts. Das immer totalitärer werdende Deutschland kann nur Fax oder Überwachungsstaat (bis in die 2020er).

**Ausblick:** Wie angekündigt, werde ich ab jetzt kaum noch Alkohol trinken. Es wird Jahre dauern, bis ich ein Bier trinke. Weitere Jahre vergehen, bis ich im Sommer ab und zu ein Radler trinke. Kurz- und mittelfristig sehe ich keinen gesundheitlichen Vorteil.