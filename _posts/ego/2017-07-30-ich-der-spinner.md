---
title: Ich, der Spinner
category: ego
layout: default
deepl: 2023-02-19
tags: "Corona, Pandemie, Statistik, Berlin, Brandenburg, Virus, Freiheitsrechte, Plattensee, Ungarn"
---
Ein Halbbruder heiratet. Ich erzähle meiner Schwägerin von der Vetternwirtschaft bei meinem früheren Arbeitgeber, dem Amt für Statistik Berlin-Brandenburg, und dass in den nächsten Jahren ein "eigentlich harmloser Virus zur Einschränkung von Freiheitsrechten" eingesetzt werden soll. Ich ernte ungläubige Blicke.

Danach fahre ich für zwei Wochen nach Ungarn zu meinem Vater an den Plattensee. Er vermietet dort im Sommer fünf Ferienwohnungen. Er fragt mich, ob es in den nächsten Jahren eine Virusepidemie oder eine Wirtschaftskrise geben wird. Ich sage ihm, dass es wahrscheinlich noch ein oder zwei Jahre dauern wird.

Er erzählt mir, dass er jetzt seine Familie hier in Ungarn hat.

Wahrscheinlich haben sie das alles vergessen, wie so viele Menschen vieles vergessen.
