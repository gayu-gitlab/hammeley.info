---
title: Ein neuer Lebensabschnitt
category: ego
layout: default
tags: "ego, Schule, Berlin, Zeugnisse"
---
Wir haben heute unsere Zeugnisse erhalten. Ich bin in der Jahrgangsstufe 11 der Robert-Blum-Schule (Gymnasium), der Einführungsphase der gymnasialen Oberstufe, wie es in Berlin heißt.

Das Zeugnis ähnelt dem Zeugnis vieler Nicht-mehr-zur-Schule-Geher, wobei ich eigentlich kaum Fehltage habe. Die besten Noten meines Abschlußzeugnisses sind:

- 3 Wirtschaftslehre (Profilbereich)
- 3 Informatik (Profilbereich)
- 3 Sport
- 3 Wirtschaftslehre (Wahlpflichtfach)

Der Rest: acht Fünfen und zwei Sechsen

Im Halbjahr zuvor hatte waren die Noten noch so verteilt:

- 3 Wirtschaftslehre (Profilbereich)
- 3 Physik
- 2 Sport
- 2 Informatik (Wahlpflichtfach)
- 3 Wirtschaftslehre (Wahlpflichtfach)

Der Rest: zwei Vieren, fünf Fünfen

Zu schlecht für die Schule, aber noch zu gut um im Radio einen Preis für das schlechteste Zeugnis der Stadt zu gewinnen. Ein Jahr vorher hatte ich noch einige 2-er und nur eine 5 und keine 6.

**Rückblick**

Während meiner Zeit auf der Oberschule sterben die Eltern einer Mitschülerin bei einem Überfall auf ihren Lottoladen. Ein Bruder einer Mitschülerin stirbt durch Messerstiche gewaltsam innerhalb eines kurzen Zeitabstands.

Im Fernsehen gibt es viele tolle neue Serien:

- ["Star Trek - The next Generation]()
- ["Twin Peaks"](https://www.youtube.com/watch?v=yFMaEIHIrGw)
- ["Profit"](https://www.youtube.com/watch?v=L0n7Jd0zZZY&origin=https://hammeley.info/ego/1996/06/19/ein-neuer-lebensabschnitt)
- ["Picket Fences"](https://www.youtube.com/watch?v=Dv3sAOdGkGI&origin=https://hammeley.info/ego/1996/06/19/ein-neuer-lebensabschnitt)

und zahlreiche Serien mit Familien "Al Bundy", "Bill Cosby", dieser Heimwerker usw.

Irgendwann in diesem vergangenen Lebensarbschnitt bekam ich meinen ersten PC, einen Highscreen 486 DX-33 (von Vobis). Der zweite PC war ein Pentium später nachgerüstet mit einem CD-ROM-Laufwerk und einer [Diamond Monster 3D](https://de.wikipedia.org/wiki/3dfx_Voodoo_2)-Grafikerweiterungskarte.

**Ausblick:**

In ein paar Jahren erfahre ich, dass sich eine Mitschülerin von der Grundschule umgebracht hat.

Da ich bereits 18 bin, ohne Schule jetzt auch viel Zeit habe und ich über die 50.000 D-Mark verfügen kann, habe ich eigentlich alles was es für das junge Leben und die Exploration der Welt braucht. Große Weltreisen mache ich zwar keine, aber ich mache trotzdem ausreichend Erfahrungen.
