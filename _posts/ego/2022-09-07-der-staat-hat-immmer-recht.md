---
title: Der Staat hat immer recht
category: ego
layout: default
---
**Klageerwiderung**
**(Begründung)**

**Aktenzeichen: x X xxx/22**

Sehr geehrter Herr Dr. x,

in dem Rechtsstreit

- x. /. Hammeley, x.

wird zu

- xxx
- xxx

wie folgt geantwortet:

Der Beklagte ist ein ungeimpfter "Schwurbler", welcher gleichzeitig am links- wie auch am rechtsextremistischen Rand außerhalb der Gesellschaft zu verorten ist. Ihm wurde im Zeitraum vom 25.11.2021 bis zum 31.3.2022 rechtmäßig der Zugang zu öffentlichen Verkehrsmitteln in Berlin, seiner Arbeitsstätte beim ITZ Bund und der Universität Potsdam (zwecks Studiums) verwehrt. Der Zugang zum sozialen Sicherungssystem der Bundesrepublik Deutschland ist im selben Zeitraum rechtmäßig verweigert worden.
<!-- more -->
Aufgrund der irrsinnigen Annahme des Beklagten, Deutschland wäre eine funktionierende Demokratie und Sozialstaat, kam es zum devianten Verhalten des Beklagten und Quertreibers und zu dem Unvermögen, die neue Realität zu akzeptieren.

Die selbst verschuldete Mittellosigkeit des Beklagten, die unmittelbar nach dem Einbehalten des Gehalts durch die damalige Arbeitgeb:ende "wegen Arbeitsverweigerung" eingetreten ist, führten zum Ausbleiben von freiwilligen Beitragszahlungen an die Krankenversichernden (AOK). Der entstandene Fehlbetrag wurde kurzerhand durch Pfändung auf dem Girokonto des Beklagten eingefroren (und ist so bis dato). So kam es zum ersten Mietrückstand in Höhe von einer Monatsmiete.

Der fehlende Zugang zum Sozialsystem verlängerte die Mittellosigkeit bis zum 31.3.2022, sodass drei weitere Mietzahlungen ausblieben.
Die "derzeitig rückständigen Zahlungen" in Höhe von xxxx,xx € sind abzüglich der vom Beklagten am tt.mm.jj beantragten, wegen Mängeln zu mindernden Summe in Höhe von xxx,xx € (unter 2.) und in Höhe von xx,xx € (unter 4.) bis dato nicht zu beanstanden.

Der Beklagte ist laut Mehrheitsmeinung und Meinung zahlreicher Regier:enden unter anderem ein "Egoist" (Gerhart Baum, FDP), ein "RAF-naher Verschwörungstheoretiker" (Dr. Markus Söder, CSU), ein "von der Demokratie Abgewandter" (Olaf Scholz, SPD), ein "rechtsextremer Aasgeier" (Winfried Kretschmann, Grüne) und ein einer "Minderheit zugehöriger Terrorist" (Marie-Agnes Strack-Zimmermann, FDP), "Sozialschädling" (Rainer Stinner, FDP), "Covidiot" (Saska Christina Esken, SPD), der nur knapp der "Beugehaft" (Boris Palmer, Grüne) entkam.

Schon deshalb ist von einem vorsätzlichen Selbstverschulden auszugehen. Ungeimpfte, und "von Menschenfeindlichkeit durchtränkte" (Jan Böhmermann, ZDF / Beitragsservice) Querdenker wie der Beklagte haben in der Bundesrepublik Deutschland keinen Platz mehr und sollten unter allen Umständen an den Rand des gesellschaftlichen Lebens (z. B. durch Obdachlosigkeit) gedrängt werden.

Selbst dann, wenn in ferner Zukunft die Corona-Politik der Jahre 2019 bis zum heutigen Tag auf Druck des natürlich stattfindenden gesellschaftlichen Wandels aufgearbeitet wurde und der Beklagte gesellschaftlich und finanziell rehabilitiert ist, ist eine Räumung auch bei Fremdverschulden, z. B. durch vorsätzliches oder fahrlässiges Einbehalten von Gehältern durch Arbeitgeb:ende oder durch vorsätzliches oder fahrlässiges Ausbleiben von Überweisungen von Leistungen zum Lebensunterhalt durch Sozialtrag:ende gerechtfertigt.

Es wird darauf hingewiesen, dass der hier Beklagte gegen das Jobcenter Friedrichshain-Kreuzberg eine Klage vor dem Sozialgericht Berlin unter dem Aktenzeichen X xxx XX xxx/xx XX eingereicht hat. Wird im Tenor des Klageanspruchs entschieden, können die durch den Vermieter geltend gemachten Mietrückstände unverzüglich durch den Beklagten befriedigt werden.
Es wäre daher sinnvoll, eine Entscheidung des Sozialgerichts Berlin abzuwarten, bis eine Entscheidung in dieser Sache getroffen wird.

Zur Sicherung seiner Interessen gegenüber der ehemaligen Arbeitgeb:enden wurde ein Laptop des Models Lenovo T490 (auch SINA Workstation genannt) in Besitz genommen. Nach Übergang in das Eigentum des Beklagten könnte nach Veräußerung des Gegenstands ein Teil der Mietrückstände beglichen werden. Der Wert des Laptops und seiner Komponenten übersteigt auch heute noch die Summe des im Dezember 2021 einbehaltenen Gehalts.

Für Deutschland

x. Hammeley
