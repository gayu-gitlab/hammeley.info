---
title: The Spirit makes you move
category: ego
layout: default
tags: "ego, Loveparade, Techno, Berlin, Frauen"
---
Ich nehme das erste Mal an der [Loveparade](https://www.youtube.com/watch?v=vl-SfKwOoBE) auf dem Ku'damm in Berlin teil. 120.000 Menschen feiern. Es sind viele hübsche junge Frauen anwesend. Fast alle sind etwas älter als ich, was dem Spaß aber keinen Abbruch tut. Ich kann mit Schulfreunden auf einen LKW steigen und verbrenne mir dabei den Arm an einem gerippten Alu-Auspuff.

Ich bin jetzt sogenannter "Raver" oder auch "Rehwoh auf der Lohfpahrähd", wie das zahlreiche Gäste der Stadt nennen. nü.

In den nächsten Jahren wird es kommerzieller, die Wagen werden von immer mehr "szenefremden" Konzernen gesponsert, die Auflagen werden immer strikter, aber nicht sicherer.

Überhaupt sind "kommerz" in der Szene und "Konsum(tempel)" aus dem Osten so Begriffe dieser Jahr.

**Ausblick** In den Jahren 1996/97 schlich ich mich als "Hete" Undercover auf den CSD und hatte sogar Spaß dabei. Nur das Arschgegrabsche habe ich dort sein lassen. Mehr als 10 Jahre später war ich auch noch auf dem Lesbisch-Schwulen Stadtfest. Mehr als 30 Jahre später ist das alles nicht mehr so einfach.
