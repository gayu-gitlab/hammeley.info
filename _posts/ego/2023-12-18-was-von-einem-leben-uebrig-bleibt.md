---
title: Was vom Leben übrig bleibt
layout: default
category: ego
tags: "Leben, und, sterben"
---
**18. Dezember 2023 18:14**

Begleitet man einen Sterbenden, ist der Eintritt des Todes, anders als das Sterben ansich, recht unspektakulär und friedlich. Stirbt man an einer unheilbaren und schmerzhaften Krankheit, hört nach Stunden in denen sich die Schmerzen, das Atmen, der Bewusstseinszustand, die Ansprechbarkeit, die Gesichtszüge und die Hautfarbe mehrmals ändern, irgendwann einfach die Atmung auf und der Körper bleibt regungslos liegen. Statt einem tiefen Atmen und einen Röcheln tritt eine Stille ein. Einige Muskeln bewegen sich noch ein wenig, bis auch da keine Regung mehr zu erkennen ist. Das war's. Was bleibt ist ein Körper der innerhalb von zwei Stunden erstarrt und nach einem Tag wieder beweglich wird, bis er zum verwesen unter die Erde gebracht wird.

**Was bleibt sonst noch vom einem Leben?**

Kinder, vielleicht Enkel, ein paar schöne und ein paar schlechte Erinnerungen bei den Hinterbliebenen, unerfüllte Hoffnungen und Wünsche, Anrüche gegen sich selbst und an Andere. Ein kleines oder ein großes Erbe, vielleicht auch Schulden, vielleicht aber auch gar nichts davon. Gehört man nicht zu denen, denen man einen Straßennamen oder ein Denkmal widmet, bleibt nicht wirklich viel von einem Leben von fast 3.977 Wochen oder 27.836 Tagen.

**Alles hat seine Zeit**

<figure>
    <blockquote>
        <p>
            Ein jegliches hat seine Zeit, und alles Vorhaben unter dem Himmel hat seine Stunde:<br>
            Geboren werden hat seine Zeit, sterben hat seine Zeit; pflanzen hat seine Zeit, ausreißen, was gepflanzt ist, hat seine Zeit;<br>
            töten hat seine Zeit, heilen hat seine Zeit; abbrechen hat seine Zeit, bauen hat seine Zeit;<br>
            weinen hat seine Zeit, lachen hat seine Zeit; klagen hat seine Zeit, tanzen hat seine Zeit;<br>
            Steine wegwerfen hat seine Zeit, Steine sammeln hat seine Zeit; herzen hat seine Zeit, aufhören zu herzen hat seine Zeit;<br>
            suchen hat seine Zeit, verlieren hat seine Zeit; behalten hat seine Zeit, wegwerfen hat seine Zeit;<br>
            zerreißen hat seine Zeit, zunähen hat seine Zeit; schweigen hat seine Zeit, reden hat seine Zeit;<br>
            lieben hat seine Zeit, hassen hat seine Zeit; Streit hat seine Zeit, Friede hat seine Zeit.<br>
        <br>
            Man mühe sich ab, wie man will, so hat man keinen Gewinn davon.<br>
            Ich sah die Arbeit, die Gott den Menschen gegeben hat, dass sie sich damit plagen.<br>
            Er hat alles schön gemacht zu seiner Zeit, auch hat er die Ewigkeit in ihr Herz gelegt; nur dass der Mensch nicht ergründen kann das Werk, das Gott tut, weder Anfang noch Ende.<br>
            Da merkte ich, dass es nichts Besseres dabei gibt als fröhlich sein und sich gütlich tun in seinem Leben.<br>
            Denn ein jeder Mensch, der da isst und trinkt und hat guten Mut bei all seinem Mühen, das ist eine Gabe Gottes.<br>
            Ich merkte, dass alles, was Gott tut, das besteht für ewig; man kann nichts dazutun noch wegtun. Das alles tut Gott, dass man sich vor ihm fürchten soll.<br>
            Was geschieht, das ist schon längst gewesen, und was sein wird, ist auch schon längst gewesen; und Gott holt wieder hervor, was vergangen ist.<br>
        </p>
    </blockquote>
    <figcaption class="blockquote-footer">
        <cite title="Prediger Salomo (Kohelet 3,1)">Prediger Salomo(Kohelet) (Pred 3,1)</cite>
    </figcaption>
</figure>