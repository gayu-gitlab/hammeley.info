---
title: Fahrradtag
layout: default
category: ego
---
**Hinweis: Das Lesen von Beiträgen auf dieser Seite könnte für einige Gemüter:Innen verstörend wirken. Die Beiträge auf dieser Seite sind Saattiere.**

Mit dem Alter, ich gehe ja langsam auf die 50 zu, wird das Entdecken neuer und das Besuchen weit entfernter Orte immer unwichtiger. Dieser natürliche Wandel kommt mir natürlich gelegen, da ich mir ferne Reisen eh nicht leisten kann. Statt mit dem Bumsbomber nach Thailand, geht es nun vermehrt in die heimeliche Heimat und in die nahegelegenen Ostgebiete. Aber auch in der näheren Umgebung besucht man häufiger die Orte, die einem schon früher eine Freude bereitet haben. Mit dem Germania-Ticket ist alles viel einfacher und günstiger geworden. Also nahm ich mir ein wenig Auszeit.

Nachdem ich am 19. April (zum Bicycle-Day) eine fast 10 stündige Fahrradtour hinter mir hatte, bei der ich allerdings nur bis zum Treptower Park kam, aber dennoch ganz schön fertig war nach dem Trip, habe ich mir vorgenommen, dass ich mir ein Deutschlandticket kaufe und so oft wie möglich durch den Tagespendelbereich fahre.

![Fahrradtour](){:data-src="/assets/images/bodetal/fahrradtour.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload"}
