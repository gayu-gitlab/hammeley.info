---
title: Osterferien in Österreich
layout: default
category: ego
tags: "ego, Österreich, Urlaub, Berlin, Eltern, Kirchbichl, La-Belle, Terror"
---
Ich bin das erste Mal ohne Eltern im Urlaub. In Berlin organisieren die Bezirke Reisen für Kinder in den Ferien. Früher nannte man das wohl "Kinderlandverschickung".

Ich verbringe die Osterferien mit Schulkameraden und vielen anderen unbekannten Kindern anderer Schulen bei [Familie Schön in Kirchbichl](https://www.weberhof-tirol.at/) in Tirol, Österreich. Sie haben einen Bauernhof direkt am Ufer des Inns.

Sie hatten einen großen Bernhardiner und es gab gutes Essen. Der Sohn der Familie hat mich und einen Freund mal mit dem Traktor mitgenommen, mit dessen Schaufel er eine Rampe gebaut hat, über die wir dann gerast sind. Das hat Spaß gemacht. Gab aber danach Ärger.

Wir verbringen viel Zeit im Freien. Im Flussbett des Inns, in der Ortschaft und in den Bergen. Es gibt viele Aktivitäten, gutes Essen.

Als ich am Sonntag, den 6.4.1986 mit dem Reisebus am Rathaus Friedenau wieder in Berlin ankomme und abgeholt werde, sehe ich auf der Fahrt nach Hause, eine "kaputte Baustelle" in der Rheinstraße, ganz in der Nähe, wo der Reisebus hält und vor ein paar Wochen noch nicht da war.

**Ausblick**

Ein paar Tage später sehe ich die Bilder im Fernsehen. Etwa 24 Stunden vor meiner Ankunft wurde dort ein "Terroranschlag" in der Diskothek La Belle verübt.

Erst viele Jahre später informiere ich mich bewusst über das Ereignis.

!["Diskothek La Belle am 6. April 1986"](){:data-src="/assets/images/dpa-high-36766630-1.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload"}

<small>Bildquelle: https://www.tip-berlin.de/wp-content/uploads/2021/04/dpa-high-36766630-1.jpg</small>
