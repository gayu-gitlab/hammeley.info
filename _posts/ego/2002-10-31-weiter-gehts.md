---
title: Weiter gehts
category: ego
layout: default
tags: "ego, Verpflichtung, Bundeswehr"
---
Ich habe heute eine Verpflichtungserklärung unterschrieben. Statt für fünf Jahre bin ich nun für acht Jahre verpflichtet.

Durch die Regelungen des Berufsförderungsdienstes, der die Eingliederung der Soldatinnen und Soldaten in das zivile Leben nach der Dienstzeit erleichtern soll, verlängert sich die aktive Dienstzeit jedoch nur um eineinhalb Jahre. Da ich mich aus der Wehrpflicht heraus verpflichtet habe, musste ich mich für fünf Jahre verpflichten, statt der üblichen vier Jahre für ungediente Bewerber.
