---
title: Der Kunstmäzen
layout: default
category: ego
---
Von Bitcoin hat inzwischen jeder gehört. Ein paar technisch versiertere Personen haben sicherlich auch schon von den sogenannten NFT (non-fungable token) gehört. Meistens im Zusammenhang mit komischen digitalen Bildern von Affen. 

Die Kryptowelt ist kompliziert und neu, bietet aber fast revolutionäre Möglichkeiten. Es ist in etwa so wie vor 20 Jahren, als das Internet und der Mobilfunkmarkt eine neue Zeit eingeläutet haben. Erinnert sich noch wer an [call-by-call](https://www.youtube.com/watch?v=9gD-xUeTlIA "Dieter Hallervorden Billiger telefonieren") oder Preselection oder kostenloses Telefonieren mit Werbung alle zwei Minuten bei Otelo? Ungefähr in diesem Stadium befindet sich der Kryptomarkt heute. Viele Ideen werden es nicht schaffen, manche Kryptowährungen, Marktplätze oder gelobte Kryptoexperten sind skandalgeplagt, werden verschwinden und vergessen werden und auf dem Weg zum Mainstream wird es viele Pleiten und verurteilte Betrüger geben, ganz so wie damals. Erinnert sich noch wer an "Ich bin schon drin"-Boris Becker oder die Telekom-Aktie oder an die zahlreichen anderen Skandale im Neuen Markt? So ist es gerade in der Krptowelt. Große Chancen und großes Risiko und "Wozu brauche ich ein Mobiltelefon, bin ich Manager oder was?"

Während die bekannten Kryptowährungen wie übliche Währungen benutzt werden und damit spekuliert, gespart, angelegt, investiert oder konsumiert wird, sind NFT ähnlich, aber doch ganz verschieden. Auch sie besitzen einen Wert den man auf einem Markt feststellen kann. Anders als bei den üblichen (Krypto)-Währungen, ist bei NFT jedoch ein spezielles Objekt, Recht oder ein Anteil mit dem NFT digital verbrieft. So ähneln NFT eher Anteilsscheinen, Beglaubigungen, Erb- oder Stimmrechten, Ausweisen, Geburtsurkunden oder Zeugnissen, Visa, Aufenthaltstitel oder Fischereirechten. Aber auch Besucherlisten, Eintrittskarten, historische Dokumente können als NFT zu registriert oder verewigt werden.

Die Anwendungsmöglichkeiten sind fast unbegrenzt. Derzeit beschränkt sich das Anwendungsgebiet der NFT fast ausschließlich auf digitale Sammlerstücke von denen es zwar Kopien geben kann, aber eben nur ein Original.

Es gibt also NFT, die vom Besitzer veräußert werden können und es gibt NFT, die an eine einzelne Person gebunden sind. Ein Identitatsnachweis ist natürlich nicht veräußerbar, ein Recht ein bestimmtes Stück Land zu nutzen schon eher. Aber auch ein unveräußerliches und unvererbbares Nießbrauchrecht könnte als NFT-Dokument irgendwann Gültigkeit haben. In Deutschland wahrscheinlich eher so um das Jahr 2070.
 
Ich habe heute meine Anteile an APENFT erhöht. Ich bin nun im Besitz von über 45.000.000 APENFT (ca. 17,00€) von derzeit 267.000.000.000.000 im Umlauf befindlichen  APENFT. Wenn ich richtig gerechnet habe, bin ich durch den Kauf nun mit ca. einem 0.1723-Millionstel (1,723*10^-5%) an einer kleinen Kunstsammlung in Hong Kong beteiligt.

Das teuerste Werk der Sammlung ist derzeit "Femme Nue Couchee Au Collier" von Pablo Picasso (ca. 15. Mio Euro), aber auch Andy Warhols ["Three Self-Portraits"](https://cdn.coingape.com/wp-content/uploads/2021/06/28090356/Three-Self-Portrait-NFT.jpeg "Three Self-Portraits by Andy Warhol) mit einem Wert von ca. 2,5 Millionen Euro und das Time Magazine vom 2. April 1965 mit dem Titel ["Computer in Society"](https://content.time.com/time/covers/0,16641,19650402,00.html "Time Magazine -Computer in Society, 2. April 1965"), mit einem Wert von ca. 200.000€ gehören dazu.

Vielleicht kann ich meine Stimmrechte an dem Projekt nutzen und ein Bild von [Helmut Wellschmidt](https://helmutwellschmidt.de "Helmut Wellschmidt") ersteigern lassen. Diese dürften noch für unter 10.000€ zu haben sein.

Ich werde aber auf jeden Fall ab sofort immer einen Schal tragen und mit dicker Brille herumlaufen. Ich glaube das macht man in der Kunstszene so.
