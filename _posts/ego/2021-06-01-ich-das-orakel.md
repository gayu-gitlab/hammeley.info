---
title: Ich, das Orakel
category: ego
layout: default
tags: "Studium, Open, Univewrsity, Politik, ITZ-Bund, Ukraine, Krieg, Politik, gendern, Innen, Corona, Virus, Pandemie, Verschwörung, RAF, Regierung"
---
Irgendwann im Juni bei meinem derzeitigen Arbeitgeber fragt mich eine auch dort arbeitende Studentin, ob ich demnächst von einem Krieg in der Ukraine ausgehe.

Ich antworte sinngemäß, dass ich nicht glaube, dass es in diesem Jahr noch, also in den Herbst und Winter hinein einen Krieg geben wird, wenn, dann scheint mir der nächste Frühling logischer, da es selbst Hitler kurz vor Stalingrad, nicht weit östlich der Ukraine schwer hatte.

Auf die Frage, "wann im Frühling" fiel wohl auch erschreckend genau das Datum um den 24.2.2022. Da ich aber keine direkte Leitung zu Putin habe, kann ich natürlich keine genaueren Angaben machen.

Von einer dauerhaften Eroberung Kiews oder Odessas bin ich in dem Gespräch auch nicht ausgegangen. Die Ziele Russlands waren eigentlich seit Jahren bekannt. Eine Eroberung der ganzen Ukraine ist sicherlich nur ein Hirngespinst der deutschen Medien und Politikbetreib:enden.

Über die Prognose "Oktober 2022" äußere ich mich nicht.
<!-- more -->
Es ist manchmal ganz schön erschreckend, wie genau ich "die Zukunft einfach so voraussagen kann", dabei ist doch das ganze Weltgeschehen völlig unvorhersehbar, sodass die Politik immer Ad-hoc reagieren und entscheiden muss und alternativlose Lösungen präsentiert. Wie die das immer so schaffen?

Ach ja, bei der Arbeit ordnete die Referatsleit:ende jetzt "Gendersprachpflicht" an. Es gibt jetzt "Sachbearbeitende", "Nutzende" und "Mitarbeitende". Die geschriebenen Texte hören sich nun an, wie Beiträge der Aktuellen Kamera. Vergleiche sind jedoch nicht gewünscht. Aber was weiß ich denn schon? Ich bin jetzt Nazi und bald Mitglied der [Corona-RAF](https://www.zeit.de/news/2022-02/19/soeder-duerfen-keine-corona-raf-bekommen) [(archiv)](https://ich-habe-mitgemacht.de/index.php?option=com_content&view=article&id=188:wir-duerfen-am-ende-keine-corona-raf-bekommen&catid=9&highlight=WyJjb3JvbmEtcmFmIl0=&Itemid=101).

**Ausblick:** Zum 25.11.2021 beschließt der Senat von Berlin asozialen und egoistischen Vaterlandsverrätern wie mir den Zugang zum öffentlichen Personennahverkehr und zu meiner Arbeitsstelle zu verwehren. Im Land Brandenburg gelten ähnliche Regelungen, die es mir nicht erlauben, die Universität Potsdam zu besuchen. Die Online-Lehre wurde jedoch vorher wieder beendet. Mein Studium ist somit wieder beendet.

Es scheint, als hätte sich die "Prognose" von Prof. Dr. C. S. (Astrophysiker), der in seinen bisherigen Zoom-Vorlesungen noch eindrücklich die Impfung empfahl nicht bewahrheitet. Drei Jahre später gibt es immernoch Freiheitseinschränkungen und ein faschistoides Infektionsschutzgesetz. Naja, was weiß ich schon. Anders als das "freie Lehr:ende", wie es jetzt bei der Uni Potsdam heißt, der außerhalb seines Fachgebiets den jungen Studier:enden Lebenstipps gibt, stelle ich eine Gefahr für den Volkskörper dar, weil ich auf das im Grundgesetz verbriefte Recht auf körperliche Unversehrtheit bestehe.

Beim ITZ-Bund unterstellen mir die Referatsleit:ende Frau A. W. vom Typ "Justin Bieber Lesbier:ende" und eine andere Personalperson in einem unverschämten Ton "Arbeitsverweigerung" vor (nach elf Monaten Tätigkeit mit 4:30 aufstehen). Man kündigt an, das Gehalt für die letzten zwei Tage die ich von zu Hause verbracht habe (im Heim-Büro), weil ich nicht auf die Arbeitsstelle durfte zurückfordern zu wollen. Das ITZB wird mir kurzerhand den VPN-Zugang sperren und mein Gehalt für den Dezember 2021 einbehalten.

Ich darf mich nun den Fragen der Verwandten stellen: ...

Entgegen der Wahrheitsbekundungen des letzten und des amtierenden Gesundheitsministers werde ich nicht an einer Corona-Infektion versterben, obwohl ich es mir wünsche. Deutschland hat neue Heilige, u. a. "Christian Drosten". Der Chefvirologe des Landes präsentiere sich gerne in der Öffentlichkeit, solange es keinen Gegenwind gab, machte sich dann aber "dünne", wie man es von dem Typen Menschen halt kennt.

Im Sommer 2022 wird das Infektionsschutzgesetz wiederholt novelliert. Die Landesregierungen können nun nach gut dünken (ohne wissenschaftliche Grundlage) Grundrechte einschränken. In den vorherigen zwei Jahren machte die Regierung noch den Anschein, auf Basis von wissenschaftlichen Kennzahlen wie ***R-Wert***, ***Verdoppelungszahl***, ***Inzidenz***, ***Bettenbelegung*** und ***Fallsterblichkeit*** zu entscheiden. In den Medien wurde solange darüber berichtet, dass nun alle Bürg:enden auch Wissenschaf:telnde sind. Die Begriffe und Schein-wissenschaftlichen Debatten werden leise beendet. Das ist nicht mehr nötig. Das Stimmvolk begehrt auch nicht mehr auf. Macht ja nichts. Wir werden ja alle sterben, als einzige in Europa.

Ich wünsche mir, das Land würde von einem Asteroiden getroffen werden. Ich kann die Dummheit dieser Menschen nicht mehr ertragen, die sich Tag ein Tag aus immer wieder aufs Neue verarschen lassen und mit einer Überzeugtheit dem ***offiziellen Narrativ*** ohne Widerworte glauben und folgen. Jegliche Vergleiche mit früheren Epochen sind jedoch nach wie vor ***Verschwörungstheorien***, auch wenn es nur ein Vergleiche bleiben. Der Begriff ***Verschwörungstheorie*** wird nun auch fast täglich von der Tagesschau benutzt. So zum Beispiel, als im September 2022 die Gaspipeline Nordstream 2 zerstört wird.

Seit einigen Jahren werden ja ***Quertreiber***, die nicht per Gesetz bestraft werden können, ***sanktioniert***, um sie gefügig zu machen, wenn das sogenannte [***nudging***](https://de.wikipedia.org/wiki/Nudge) nicht richtig wirkte. Das betrifft nun aber nicht mehr nur "faule Arbeitslose", sondern auch Menschen, die z. B. keine Masken tragen oder eine abweichende Meinung haben. Anders als in der DDR, im Dritten Reich, China und Russland wird in den deutschen Medien ausschließlich die Wahrheit verkündet.

Anders als im Überwachungsstaat China, in dem die Menschen nur noch mit einem Digitalen Impfzertifikat Zutritt zu Gebäuden und Einrichtungen erhalten, ist der Zutritt mit einem digitalen Zertifikat (bzw. Luca-App) in Deutschland das Gegenteil von dem was in China passiert. Es geht schließlich um das Überleben unserer Demokratie.

Die Begriffe ***Sanktion***, ***Maßnahmen*** oder ***Narrativ*** sind nun fest im Vokabular der Deutschen verankert, nachdem es bereits vor Jahren die Begriffe ***Terrorist***, ***Gefährder*** und ***Störer*** schafften. Weiterhin wird in den ***Leitmedien*** über "[immer autoritärer werdende Regierungen](https://taz.de/Demokratie-weltweit-auf-dem-Rueckzug/!5816845/) und über den Rückzug von "Demokratien" berichtet - jedoch nur in anderen Ländern.

**Hinweis:** Der Sprachgebrauch in diesem Artikel, ist dem der Politikbetreib:enden und Medienvertret:enden dieser Zeit nachempfunden. Weitere Informationen sind unter [ich-habe-mitgemacht.de](https://ich-habe-mitgemacht.de) erhältlich.
