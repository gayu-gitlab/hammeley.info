---
title: Der Hammeleyla ist geyla
category: ego
layout: default
---
Wer etwas anderes behauptet ist Nazi.

<div id="carouselGeyla" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-inner ratio ratio-16x9">
        <div class="carousel-item active">
            <img data-src="/assets/images/hammeleyla/hammeleyla-12.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Snowfitto">
            <div class="carousel-caption d-none d-md-block">
                <h5>The Artist</h5>
                <p>Word. By DSH.</p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/hammeleyla/hammeleyla-1.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Hammeleyla in Berlin, Abendland">
            <div class="carousel-caption d-none d-md-block">
                <h5>Berlin, Abendland</h5>
                <p>Flughafen BER, Terminal 1+2, Abflüge</p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/hammeleyla/hammeleyla-2.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Hammeleyla in Zgorzelec, Polen">
            <div class="carousel-caption d-none d-md-block">
                <h5>Zgorzelec, Polen</h5>
                <p>Andere Länder andere Sitten</p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/hammeleyla/hammeleyla-3.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Hammeleyla in Salzburg, Oberdeutschland">
            <div class="carousel-caption d-none d-md-block">
                <h5>Salzburg, Oberdeutschland</h5>
                <p>Altstadt ganz ok, aber sonst nur die Berge im Hintergrund</p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/go_west/rheinfall.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Hammeleyla in Salzburg, Oberdeutschland">
            <div class="carousel-caption d-none d-md-block">
                <h5>Neuhausen, Bergdeutschland</h5>
                <p>Totaler Rheinfall</p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/hammeleyla/hammeleyla-4.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Hammeleyla in Strasbourg, La France">
            <div class="carousel-caption d-none d-md-block">
                <h5>Strasbourg, La France</h5>
                <p>Bonjour à tous</p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/hammeleyla/hammeleyla-5.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Hammeleyla in Luxembourg, Luxembourg">
            <div class="carousel-caption d-none d-md-block">
                <h5>Luxembourg, Luxembourg</h5>
                <p>Schöne Landschaft, Stadt könnte schöner sein.</p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/hammeleyla/hammeleyla-6.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Hammeleyla in Lübeck, Unterland">
            <div class="carousel-caption d-none d-md-block">
                <h5>Lübeck, Unterland</h5>
                <p>Walla, Holstein-Tor</p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/hammeleyla/hammeleyla-7.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Hammeleyla in Arnhem, Nederduitsland">
            <div class="carousel-caption d-none d-md-block">
                <h5>Arnhem, Nederduitsland</h5>
                <p>Lustige Sprache</p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/hammeleyla/bln-dub.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Tour d'Europe">
            <div class="carousel-caption d-none d-md-block">
                <h5>Tour d'Europe</h5>
                <p>1.700 km und zürück</p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/hammeleyla/hammeleyla-8.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Ljubljana">
            <div class="carousel-caption d-none d-md-block">
                <h5>Ljubljana</h5>
                <p>Schönszte Frauen</p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/hammeleyla/hammeleyla-9.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Zagreb">
            <div class="carousel-caption d-none d-md-block">
                <h5>Zagreb</h5>
                <p>Europäische Krypto-Hauptstadt</p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/hammeleyla/hammeleyla-10.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Split">
            <div class="carousel-caption d-none d-md-block">
                <h5>Split</h5>
                <p>Zweitschönste Altstadt des Universums</p>
            </div>
        </div>
        <div class="carousel-item">
            <img data-src="/assets/images/hammeleyla/hammeleyla-11.jpg" class="rounded d-block mh-100 w-auto mx-auto lazyload" alt="Ereignisse bei der Bahnfahrt (6.12.23)">
            <div class="carousel-caption d-none d-md-block">
                <h5>Symbolbild</h5>
                <p>"Aufgrund eines sehr unschönen Ereignisses, verzögert sich unsere Fahrt auf unbestimmte Zeit. - Aufgrund der Zerstörung de.. werden wir zum Ostkreuz zurück..</p>
            </div>
        </div>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselGeyla" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Vorheriges</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselGeyla" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Nächstes</span>
    </button>
</div>

**Achtung: Ein Hinweis für die Stasi! Aufgrund dieser Fotos lassen sich keine Rückschlüsse über die Aufenthaltsorte des gezeigten Staatsfeinds ziehen.**

- [Ljubljana](https://www.youtube.com/watch?v=zRVFJvsVZvY&origin=https://hammeley.info/ego/2023/10/22/hammeleyla-ist-geyla)
- [Split Altstadt [yt ext]](https://www.youtube.com/watch?v=obAHIjpIWBA&origin=https://hammeley.info/ego/2023/10/22/hammeleyla-ist-geyla/)
