---
title: Schulfrei
category: ego
layout: default
tags: "ego, Schule, Berlin, Smog"
---

Wir haben in den nächsten Tagen schulfrei, wegen Smog-Alarm. Es ist leider das erste und letzte Mal, dass wir wegen schlechter Luft nicht in die Schule müssen.

[Video: Smog-Alarm in Berlin](https://www.youtube.com/watch?v=_TMaQfUrRWY)
