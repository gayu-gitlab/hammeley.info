---
title: Der Bund der Bünde
category: ego
layout: default
---
Ich bin beruflich zur Unterstützung meines Zweit-Studiums an der Universität Potsdam beim ITZBund gelandet. Ich arbeite hier in Teilzeit auf einer E6-Stelle mit anteiliger IT-Zulage. Von meinem Nettogehalt von weniger als 1.000 € leiste ich noch "freiwillige" Beiträge zur Krankenkasse in Höhe von ca. 206 €. Aufgrund meines sparsamen Lebenswandels komme ich damit über die Runden (trotz Miete für eine Zwei-Zimmer-Wohnung in Berlin-Friedrichshain).

Zu meinen Aufgaben gehört es, Notebooks für die Kunden des ITZB technisch für die Weiterverwendung vorzubereiten. Dazu gehört die Annahme und Registrierung von Lieferungen, das Aufspielen von kundenspezifischen "Images" und die Versandvorbereitung. Das Aufspielen geschieht allerdings automatisiert. Ich muss die Notebooks nur auspacken, auf eine Ladestation packen und anschalten. Danach werden die Maschinen wieder eingepackt und für den Versand vorbereitet. Ich nehme auch defekte Maschinen (Rücksendungen) von Anwendern an und versende sie zwecks Reparatur an einen Dienstleister und verschicke neue oder reparierte Maschinen an verschiedene Kunden.

Ich beginne meinen Dienst meistens um 06.00 Uhr morgens. Wegen der "Pandemie" arbeiten fast alle Angestellten des Hauses von zu Hause aus. Ich bin häufig fast allein im Haus. Die "Premium"-Kunden des ITZB sind zufrieden, "dass es jetzt endlich reibungslos läuft".

Meine Arbeitskollegen fallen teils mehrere Tage aus. Nicht so sehr wegen einer Corona-Infektion, sondern wegen der Impfung gegen eine Corona-Infektion. Sagen sie.
