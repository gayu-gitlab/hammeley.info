---
title: Die NATO ruft
category: ego
layout: default
tags: "ego, Bundeswehr, Verpflichtung, Pilot, Geilenkirchen, NATO, NAEWCF, offizier"
---
Alle Lehrgänge sind bestanden. Es dauerte nicht lange bis ich auf meinen Dienstposten versetzt werde. Ich bin inzwischen Unteroffizier (ohne Portemonnaie) und seit heute in der Flugberatung in Geilenkirchen eingesetzt, die hier Aeronautical Information Service (AIS) heißt, und nicht einer Flugbetriebsstaffel (F-Staffel), sondern dem Support Wing / Airfield Services Squadron (SWAO) untergeordnet ist. Der [NATO E3-A-Verband](https://www.bundeswehr.de/de/organisation/luftwaffe/organisation-/zentrum-luftoperationen/ddo-dta-naew-hq-fhp-geilenkirchen) (AWACS) ist ein multinationaler und der einzig fliegende Verband der NATO.

![NAEWCF E3-A Component Geilenkirchen](){:data-src="https://upload.wikimedia.org/wikipedia/commons/3/32/20170313_AWACS-09.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload"}

Hier ist alles etwas größer, internationaler und besser ausgestattet. Die Verpflegung ist mit der bei der Bundeswehr nicht zu vergleichen. Es gibt eine reichhaltige Auswahl zum Frühstück, Mittag- und Abendessen, bequeme gepolsterte Sitzbänke, und Rumpsteak mit Sauce Hollondaise statt Brot, Wurst, Käse und Brühe. Es gleicht eher einem Hotel-Buffet, als einer Kantine. Im Supermarkt [NATEX](https://natex.de) auf der "Base" gibt es alles, was es sonst auch gibt. Es gibt noch zwei andere Läden für Kleidung und Haushaltsbedarf.
Ich erhalte ein Gutscheinheft, mit dem ich monatlich um die fünf Liter Spirituosen, mehrere Pfund Kaffee und mehrere Stangen Zigaretten zollfrei einkaufen kann. Ich habe für all das keine Verwendung. Das Budget wird bei Gelegenheit verschenkt.

Das Unterkunftsgebäude hat eine Küche, einen Waschraum (mit funktionierender Waschmaschine), Einzelkabinenduschen, anders als bei der Bundeswehr (mit Ausnahme von Kaufbeuren). Es gibt hier ausschließlich Einzelzimmer.

Die Arbeit ist im Prinzip die gleiche. Ich habe nur viel weniger zu tun, weil hier weniger Flieger starten und landen als in Landsberg und es auch keinen Sichtflug-Verkehr gibt. Aufgrund des besonderen Auftrags und des Sicherheitsbedürfnisses gibt es hier auch keine Wehrpflichtigen.
