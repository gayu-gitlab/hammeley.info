---
title: Juegos De Seducción
category: ego
playlist: soda
---

Nach ca. zwei Jahren und 120 Stunden wöchentlichem Unterricht habe ich heute die Prüfung des Spanisch-Kurses am [Instituto Cervantes](https://berlin.cervantes.es/de/default.shtm) bestanden und habe das <a data-bs-toggle="collapse" href="#nivel_inicial" role="button" aria-expanded="false" aria-controls="#nivel_inicial">Certificado de Español - Nivel Inicial</a> erhalten. Mit dem Nivel Inicial ist man durchaus in der Lage sich zu unterhalten und Dinge des täglichen Lebens zu bestreiten.

<div class="row collapse" id="nivel_inicial"> 
	<div class="col col-12">
        <div class="card card-body" style="mh-77">
			<div class="carousel-caption d-none d-md-block">
				<h5>Certificado de Español</h5>
				<p>Nivel Inicial</p>
			</div>
			<div class="d-flex justify-content-center">
				<img data-src="/assets/images/nivel_inicial.jpg" class="rounded lazyload" alt="Nivel Inicial">
			</div>					
		</div>
	</div>
</div>

**Rückblick:** Es ist ja gerade Wirtschaftskrise und in Berlin gibt es viele Spanierinnen und Frauen aus Südamerika. Ich habe natürlich gleich die Chance gesehen und einen Spanischkurs begonnen, um die schönsten Dinge der Welt mit Zugewanderten zu bestreiten.

**Ausblick:** Ich belege noch die Kurse B1.1 und B1.2. Für die es zwar noch Kursprüfungen und Teilnahmebescheinigungen bei Bestehen, aber kein Zertifikat mehr gibt. Das Certificado de Español - Nivel Intermedio gibt es erst bei Bestehen des Kurses B2.5 und einer zusätzlichen Prüfung. Die Kurse ab der Stufe B1 sind deutlich kleiner. Während wir auf A1-Niveau noch ca. 10 Personen waren, melden sich für das B1.2 Niveau vielleicht noch 3-4 an. 

Mit dem Bestreiten der schönen Dinge im Leben, mit den "Inmigrantes de Espana y de Latino America" ist nichts geworden.