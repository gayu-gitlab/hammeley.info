---
title: Der Kriegsmüdigkeit entgegenwirken
layout: default
category: phantasia
libreddit_base: "libreddit.kavin.rocks,reddit.invak.id,reddit.simo.sh,libreddit.strongthany.cc,libreddit.pussthecat.org,libreddit.northboot.xyz,libreddit.kylrth.com,libreddit.projectsegfau.lt"
---
**Hinweis:** Dieser Beitrag dient der Aufrechterhaltung der Kriegsmoral der deutschen Bevölkerung und wirkt der Kriegsmüdigkeit entgegen. Der Endsieg steht kurz bevor!!!

- [Der Kriegsmüdigkeit entgegenwirken](https://libreddit.kavin.rocks/r/CombatFootage/comments/14d4lnc/ukrainian_special_forces_enter_a_russian_trench/)
- [und die Kriegsmoral steigern](https://libreddit.kavin.rocks/r/CombatFootage/comments/14f862v/russian_clowns_taking_trench/)
- [mit Heldensagen](https://libreddit.kavin.rocks/r/CombatFootage/comments/14dfpm0/ukrainian_troops_evacuating_a_wounded_comrade/)
- [von waghalsigen Männern](https://libreddit.kavin.rocks/r/CombatFootage/comments/14cv2po/2_rpgs_fired_at_close_range_from_russian_trench/)
- [und tollkühnen Piloten](https://libreddit.kavin.rocks/r/CombatFootage/comments/14hsaq9/second_angle_of_the_ka52_avoiding_an/)
- [in stählernen Fahrzeugen](https://libreddit.kavin.rocks/r/CombatFootage/comments/14cskri/inside_a_ukrainian_t80bv_loading_and_firing/)
- [mit wumms](https://libreddit.kavin.rocks/r/CombatFootage/comments/146mcfg/russian_ka52_destroys_a_moving_m2a2_bradley_with/)
- [oder Doppelwumms](https://libreddit.kavin.rocks/r/CombatFootage/comments/14ii86o/lancet_strikes_a_ukrainian_tank_leading_to_its/)
- [oder Mann gegen Mann](https://libreddit.kavin.rocks/r/CombatFootage/comments/14ckgpf/ukrainian_soldier_eliminates_a_nearby_russian/)
- [in blühenden Landschaften](https://libreddit.kavin.rocks/r/CombatFootage/comments/14ij9l0/translated_disbandment_of_the_57th_motorized/)
- [mit deutschen Gewehren](https://libreddit.kavin.rocks/r/CombatFootage/comments/14ftofg/ukrainian_soldiers_eliminate_russian_of_the_83rd/)
- [mit Adleraugen](https://libreddit.kavin.rocks/r/CombatFootage/comments/14k6jmx/footage_of_the_combat_work_of_snipers_of_the/)
- [und den besten Tötungsfähigkeiten](https://libreddit.kavin.rocks/r/CombatFootage/comments/1326ysu/ukrainian_sniper_targeting_russian_soldiers/)
- [fast wie im Computerspiel](https://libreddit.kavin.rocks/r/CombatFootage/comments/14f5241/berlingo_a_ukrainian_special_battalion_continues/)
- [oder beim Paintball spielen](https://libreddit.kavin.rocks/r/CombatFootage/comments/14gcb7p/itv_news_inside_the_trenches_on_the_ukrainian/)
- [wenn man sich nicht ergibt](https://libreddit.kavin.rocks/r/CombatFootage/comments/14fajfl/several_russian_soldiers_taken_prisoner_without_a/)
- [und es einen nicht vorher trifft](https://libreddit.kavin.rocks/r/CombatFootage/comments/14eg5el/tos1a_solntsepyok_burns_out_the_positions_of_the/)
- [so ganz unerwartet](https://libreddit.kavin.rocks/r/CombatFootage/comments/14g1r3j/a_ukrainian_bm21_grad_122mm_multiple_rocket/)
- [dann bleibt nur zurückschießen](https://libreddit.kavin.rocks/r/CombatFootage/comments/14brcs9/intense_firefight_as_a_ukrainian_soldier_defends/)
- [in deutschen Fahzeugen](https://libreddit.kavin.rocks/r/CombatFootage/comments/14bvygl/leopard_2_rams_into_burning_m2a2_bradley_vehicle/)
- [aber ohne Radar](https://libreddit.kavin.rocks/r/CombatFootage/comments/14drovi/a_ukrainian_5n63s_flap_lid_b_radar_is_attacked_by/)
- [oder anderem Material](https://libreddit.kavin.rocks/r/CombatFootage/comments/14c0zg5/a_russian_lancet_loitering_munition_strikes_a/)
- [weiß man gar nicht, dass man gleich stirbt](https://libreddit.kavin.rocks/r/CombatFootage/comments/14di43r/raf_striking_uaf_with_artillery_and_rockets_in/)
- [denn oft kommt es aus der Luft](https://libreddit.kavin.rocks/r/CombatFootage/comments/14ajzin/footage_of_russian_air_defense_shooting_down_a/)
- [mit etwas Glück wird man gerettet](https://libreddit.kavin.rocks/r/CombatFootage/comments/14fxz98/footage_of_an_artillery_shell_hitting_a_group_of/)
- [mit oder ohne Beine](https://libreddit.kavin.rocks/r/UkraineWarVideoReport/comments/14iboa0/couple_of_russians_take_nades_one_gets_amputated/)
- [aber oft wart das Glück nur kurz](https://libreddit.kavin.rocks/r/CombatFootage/comments/14gfo8p/an_attempted_extraction_of_ua_soldiers_from_a/)
- [ohne Glück, liegt man einfach so rum](https://libreddit.kavin.rocks/r/CombatFootage/comments/14bbfka/azov_sso_3rd_separate_assault_brigade_neutralizes/)
- [und liegt](https://libreddit.kavin.rocks/r/SPACEWORLD37/comments/14g61ht/translated_a_ukrainian_soldier_from_the_5th/)
- [oder wird in der Gegend verteilt](https://libreddit.kavin.rocks/r/CombatFootage/comments/12vx0m5/part_2_nsfw_ua_soldier_eliminates_russian_soldier/)
- [dann braucht es Nachschub an Menschenmaterial,](https://libreddit.kavin.rocks/r/CombatFootage/comments/14arut1/foreign_volunteer_unit_rogue_in_a_fierce/)
- [der Verschleiß an Fachkräften ist groß](https://libreddit.kavin.rocks/r/CombatFootage/comments/14a7iie/civilian_rescue_boat_in_kherson_is_targeted_by/)
- [weil man auch nachts nicht sicher ist](https://libreddit.kavin.rocks/r/CombatFootage/comments/149racr/flare_lights_up_ukranian_battlefield_at_night/)
- [oder Minen einem den Gar aus machen](https://libreddit.kavin.rocks/r/CombatFootage/comments/14asmtl/ukrainian_tank_in_belogorovka_hitting_a_mine_2023/)
- [oder einem nur ein Bein abreissen](https://libreddit.kavin.rocks/r/CombatFootage/comments/14jm3ik/uaf_soldiers_steps_on_mine_but_still_manages_to/)
- [oder mit einem Hörschaden davon kommt](https://libreddit.kavin.rocks//r/CombatFootage/comments/14hqk0x/insane_unseen_inside_pov_when_humvee_hits_mine/)
- [Für das deutsche Publikum](https://libreddit.kavin.rocks/r/CombatFootage/comments/149hvwd/panzerhaubitze_2000_operating_in_the_area_around/)
- [gibt es nur Erfolgsmeldungen](https://libreddit.kavin.rocks/r/CombatFootage/comments/149851d/russian_position_south_of_donetsk_city_is_hit_by/)
- [und klinisch sauberes Filmmaterial und schnöde Zahlen](https://libreddit.kavin.rocks/r/CombatFootage/comments/1497cm1/multiple_ukrainian_bm21_grad_firing_towards/)
- [oder schöne Dokus mit Wumms](https://libreddit.kavin.rocks/r/CombatFootage/comments/14efab2/danish_caesar_8x8_155mm_self_propelled_artillery/)
- [ohne menschliche Tragödien](https://libreddit.kavin.rocks/r/CombatFootage/comments/1497cm1/multiple_ukrainian_bm21_grad_firing_towards/)
- [alles wirkt so wunderbar einfach](https://libreddit.kavin.rocks/r/CombatFootage/comments/149hmlh/ukrainian_artillery_strikes_against_reararea/)
- [auch für den Gegner](https://libreddit.kavin.rocks/r/CombatFootage/comments/14b4h1s/a_ukrainian_artillery_piece_is_destroyed_by/)
- [den man mal über-, mal unterschätzt hat.](https://libreddit.kavin.rocks/r/CombatFootage/comments/149j0az/a_ukrainian_bukovelad_ew_complex_is_struck_by_a/)
- [Dronen, auch des 'Gegners',](https://libreddit.kavin.rocks/r/CombatFootage/comments/14a5p4t/long_video_russian_4th_brigade_of_lpr_uses_tanks/)
- [können jedes Ziel aufklären](https://libreddit.kavin.rocks/r/CombatFootage/comments/149x2nl/more_lancet_combat_footage_from_ukraine/)
- [auch die besten Panzer der Welt](https://libreddit.kavin.rocks/r/CombatFootage/comments/144c8lu/first_footage_of_a_knocked_out_leopard_as_a_uaf/)
- [einer nach dem anderen](https://libreddit.kavin.rocks/r/CombatFootage/comments/14c4bf4/compilation_of_new_clips_featuring_russian/)
- [gerade dann, wenn sie schießen](https://libreddit.kavin.rocks/r/CombatFootage/comments/14fxta8/lancet_drone_strike_on_active_ukrainian_leopard_2/)
- [sind halt schon recht groß die Dinger](https://libreddit.kavin.rocks/r/CombatFootage/comments/1463nts/three_russian_fpv_drones_attack_german_leopard_2/)
- [aber auch in den Kleinen wird's einem nicht kalt](https://libreddit.kavin.rocks/r/CombatFootage/comments/14g8f40/footage_of_a_uaf_tank_destroyed_by_lancet/)
- [in denen aber](https://libreddit.kavin.rocks/r/CombatFootage/comments/1454hxy/good_quality_video_of_destroying_of_ukrainian/) weder  [Baerbock](https://www.auswaertiges-amt.de/de/newsroom/interview-aussenministerin-baerbock-faz/2553542), noch [Hofreiter](https://www.n-tv.de/politik/Je-mehr-Waffen-wir-liefern-desto-schneller-endet-der-Konflikt-article23548982.html) oder [Strack-Zimmermann](https://www.fr.de/politik/ukraine-russland-krieg-strack-zimmermann-fdp-waffen-panzer-lieferungen-zr-91782908.html) sitzen
- [auch keine TikToker oder Regenbogenfahnenschwing:ende](https://libreddit.kavin.rocks/r/CombatFootage/comments/145xe8b/russian_lancet_drone_damages_a_german_leopard_2/)
- [sondern meist zum Sterben Gezwungene](https://libreddit.kavin.rocks/r/CombatFootage/comments/14dfbqs/destruction_of_armored_vehicles_of_the_armed/)
um die Interessen ein paar Weniger durchzusetzen. HEIL!. 

- [Ist das nicht schön?](https://libreddit.kavin.rocks/r/CombatFootage/comments/14bbfka/azov_sso_3rd_separate_assault_brigade_neutralizes/)
- [Fast wie Silvester in Berlin](https://libreddit.kavin.rocks/CombatFootage/comments/14zeess/3rd_assault_brigade_clearing_a_treeline_they_got/)

Achtung, Achtung! Alles Fake-News. Schauen sie nur die Beiträge der tageeschau an. Nur diese erzählen die ganze Wahrheit. ["Unsere Waffen helfen, Menschenleben zu retten"](https://www.auswaertiges-amt.de/de/newsroom/interview-aussenministerin-baerbock-faz/2553542) (das Baerbock 22.09.2022). Die Lieferung der F16 wird die Offensive in Ordnung bringen. Diesmal wirklich. Mehr Waffen, mehr Frieden! Mehr Waffen für mehr Endfrieden!

Referenzen:

[https://responsiblestatecraft.org/2023/04/18/just-how-many-us-troops-and-spies-do-we-have-in-ukraine/](https://responsiblestatecraft.org/2023/04/18/just-how-many-us-troops-and-spies-do-we-have-in-ukraine//)
