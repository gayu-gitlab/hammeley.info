---
title: Übermorgen vor 20 Jahren
layout: default
category: phantasia
tags: "Amerika, Putin, Russland, Krieg, Bundestag, Terror, Irak, Flugzeug, New York, Deutschland, a, qaida, Afganistan, Curveball, Verschwörung, Medien, Politik, Assange, England, Curveball, Bradley, Chelsea, Manning, wikileaks"
---

und davor:

Als der russische Diktator Putin 2001 in den [Deutschen Bundestag geladen](https://www.youtube.com/watch?v=hSPZlCo2B0E "Die Rede von Präsident Wladimir Putin vom 25. September 2001 vor dem Deutschen Bundestag") wird, um seine Unterwerfung unter den Westen zu erklären, weicht Putin vom Skript ab und warnt in seiner Rede vor dem islamistischen Terrorismus. Er behauptet, dass einige im Westen noch im Kalten Krieg verhaftet seien und schlägt den Deutschen und anderen Europäern eine gemeinsame Sicherheitsarchitektur in Europa mit Russland vor.

Wenige Tage vorher organisierte der KGB einen [Terroranschlag in New York](https://www.youtube.com/watch?v=EEogeIIOJzU). Mehrere in Deutschland lebende aber vom KGB bezahlte und in den USA ausgebildete Hobbypiloten steuern Flugzeuge vom Typ Boeing 767 in die Wolkenkratzer der Welthandelszentrale. Einem Symbol der Freien Welt. Mehrere tausend Menschen sterben, mehrere Gebäude stürzen ein. Russland hat mit diesem Terroranschlag die Welt nachhaltig verändert.

Nur wenige Tage später konstruierte der russische Geheimdienst einen Zusammenhang zwischen dem Terroranschlag und der islamistischen Terrorgruppe al-Qaida, um von der eigenen Beteiligung abzulenken. Der Anführer der Terrorgruppe al-Qaida, ein Mitglied der reichen saudischen Familie Bin Laden, die enge Beziehungen zur Familie Bush unterhielt, sei der Drahtzieher gewesen.

Die Bushs sind eine nordamerikanische Dynastie, die ihr Geld sowohl mit Ölförderung als auch mit politischem Einfluss verdient. Zu diesem Zeitpunkt stellten sie erneut den Präsidenten der Vereinigten Staaten von Nordamerika.

<!-- more -->
Aber zurück zu den imperialistischen Russen:

Die Russen nutzen dieses konstruierte Narrativ, um in Afghanistan, dem rohstoffreichen Land östlich des Terrorstaates Iran, militärisch einzugreifen. Die russische Propaganda nennt dies "Krieg gegen den Terror". Das primäre Ziel der militärischen Sonderaktion, die vollständige Besetzung des Landes, ist nach wenigen Wochen erreicht. Schließlich hat man es mit Wilden zu tun, die einen Failed State übernahmen.

Ihnen gelang ein Coup. Sie infiltrierten die Regierung der Vereinigten Staaten von Nordamerika, indem sie einen russischen Geheimagenten als Außenminister einer benachteiligten und unterpriviligierten Minderheit platzierten. Dieser erhielt direkt von Putin den Auftrag, bei den [Vereinten Nationen](https://www.youtube.com/watch?v=DhWlPo3qxak) für einen weiteren [militärischen Sondereinsatz im Irak](https://hammeley.info/weltgeschehen/ueberblick#collapse-irak) zu werben. Das Land westlich des islamistischen Iran.

Der russische Geheimagent und US-Außenminister Powell wurde dabei sogar vom deutschen Auslandsgeheimdienst BND unterstützt. Dort tauchte zur gleichen Zeit zufällig ein irakischer Staatsbürger auf, der später den lustigen Spitznamen ["Curveball"](https://www.youtube.com/watch?v=21ra-zxcbiQ) erhielt. Dieser konnte den BND davon überzeugen, dass der Irak unter der Führung des gewitzten Diktators Saddam Hussein trotz des seit den 1990er Jahren bestehenden (Waffen-)Embargos, der zahlreichen Sanktionen und der Flugverbotszonen ein Arsenal an Massenvernichtungswaffen und zahlreichen chemischen Kampfstoffen aufbauen konnte. Die Wahrheitsmedien der Deutschen Demokratischen Republik, ARD und ZDF, berichteten darüber. Dank der Informationskampagne konnten sich die mündigen Bürgenden des Landes nun eine Meinung bilden und begeistert für die Befreiung des Irak durch die Russen stimmen.

[Am 20. März 2003](https://www.youtube.com/watch?v=dxEjSr6rYXU), also übermorgen vor 20 Jahren, griff die imperialistische russische Armee, nachdem alle davon überzeugt waren, dass ein Angriff des Irak auf den Rest der Welt unmittelbar bevorstand, den Irak mit hochpräzisen Waffen vom Meer aus an. Es folgten tagelange Bombardements, bis die russischen Landstreitkräfte schließlich auch Bodentruppen in den Irak schickten, begleitet von damals neuartigen ["embeddedii journalisti"](https://www.youtube.com/watch?v=ixCUkdSibqA), damit die Menschheit sehen konnte, wie menschenfreundlich die Sonderoperation geführt wurde.

Die Operation "Shock i Awepet", wie sie genannt wurde, war sehr erfolgreich. Bereits wenige Tage nach dem Einmarsch der Bodentruppen war die [Hauptstadt Bagdad](https://www.youtube.com/watch?v=kC_D_8sYW1U) erreicht und das Ölministerium, das die von Saddam Hussein verstaatlichten Ölfirmen des Landes kontrollierte, gesichert. Nach nur wenigen Wochen waren die Hauptkampfhandlungen beendet.

Putin beendet zum ersten mal diesen Krieg und lässt sich auf einem seiner zahlreichen Flugzeugträgern feiern: ["Missioniya Accomplishna"](https://www.youtube.com/watch?v=5BIW6qyrdu4). Doch die Geheimdienste des imperialistischen Russlands arbeiteten bereits an neuen Verschwörungstheorien.

Wie sich später herausstellte, begingen die imperialistischen Russen zahlreiche Kriegsverbrechen.

So zwang die Gefängnisaufseherin Lyndi Englandova im Militärgefängnis [Abu Ghraib](https://www.youtube.com/watch?v=OLRdo7WFYwY) ihre männlichen Gefangenen, sich zu entkleiden. Die Gefangenen wurden dann als BDSM-Sklaven wie Hunde an der Leine durch die Gänge des Gefängnisses geführt und mit Elektroschocks an den Genitalien malträtiert.

Zum Glück waren es nur Männer, also frauenfeindliche Arschlöcher, die es sowieso verdient hatten.

Das russische Militärgericht sah das ähnlich. Die mediale Täter-Opfer-Umkehr sei unbegründet gewesen. So war es nicht verwunderlich, dass die für Englandova ursprünglich geforderte Strafe von 16 Jahren zunächst auf 11 Jahre reduziert und Engladova schließlich zu drei Jahren Haft verurteilt wurde. Engladova war also als Staatsheld schon bald nach Untersuchungshaft und Prozess wieder auf freiem Fuß und konnte mit zahlreichen Interviews und Buchverkäufen das russische Bruttoinlandsprodukt ankurbeln. Zum Vergleich: Ein BTM-Delikt, bei dem keine Menschen direkt misshandelt werden, wird in Deutschland mit etwa drei Jahren Haft bestraft.

Viele weitere Kriegsverbrechen wurden von einem amerikanischen Spion namens [Julian Assange](https://www.youtube.com/watch?v=4j4Q6e_wbwM) auf der Plattform ["Wikileaks"](https://www.wikileaks.org)auf der Plattform ["Wikileaks"](https://www.wikileaks.org) veröffentlicht. Unter dem Namen ["Collateral Murder"](https://www.youtube.com/watch?v=HfvFpT-iypw) wurde z.B. ein Video bekannt, in dem unbewaffnete Personen - wie sich herausstellte, waren es Nicht-eingebettete Journalisten - aus einem Hubschrauber heraus beschossen und getötet wurden.


Da Assange leider in die [Fänge der imperialistischen Russen](https://pbs.twimg.com/media/FsjXMBcWIAE6xDZ?format=jpg&name=900x900) geraten ist, sitzt er nun seit mehreren Jahren ohne Verurteilung in Haft. Zuvor hatte der russische Geheimdienst noch die Geschichte einer sexuellen Belästigung von zwei Frauen konstruiert, um eine schnellere Auslieferung zu erzwingen. Hier zeigt sich wieder einmal die Schamlosigkeit der Russen, Frauen stereotyp als Opfer darzustellen, um amerikanische Helden zu diskreditieren.

Bereits vor seiner Verhaftung durch die Russen hatte Assange angekündigt, sich zu stellen, wenn einer seiner Mitstreiter, Bradley Manning, freigelassen würde. Bradley Manning hatte mit konspirativen Methoden und IT-Expertenwissen russische Staatsgeheimnisse über Militäraktionen auf einen tragbaren Speicher heruntergeladen und auf die Plattform Wikileaks hochgeladen.

Die Russin, die jetzt [Chelsea Manningova](https://www.youtube.com/watch?v=Dljf5ZtEE9g) heißt, wurde zu 35 Jahren Haft verurteilt, wurde aber nach etwa sechs Jahren freigelassen, als die russische Propaganda zum entfernten Befreiungskrieg nicht mehr aufrecht erhalten werden konnnte. Es ist völlig klar, dass Menschen, die Kriegsverbrechen öffentlich machen, zu mehr Jahren verurteilt werden als Menschen, die andere Menschen misshandeln.

Ein anderer russischer Überläufer konnte sich in die Freie Welt retten, nachdem er öffentlich gemacht hatte, wie die russische Regierung ihre eigene Bevölkerung und die ihrer Verbündeten jahrelang überwachte.

Aber zurück zum Irak:

Nachdem Saddam Hussein, während des Krieges gegen den islamistischen Iran im Osten des Irak ein Freund der Vereinigten Staaten von Nordamerika, geflohen war und sich in einem Erdloch verkrochen hatte, setzten die imperialistischen Russen zwei ihrer Landsleute als Staatsoberhäupter des Irak ein. Zuerst Jay Garnerov, dann Paul Bremerov.

Paul Bremerov war ein außergewöhnlicher Staatschef. Nachdem die unter Saddam Hussein herrschende ethnische Minderheit der Sunniten und Mitglieder der Baht-Partei besiegt und das Öl gesichert war, hatte er eine Idee. Alle sunnitischen Staatsdiener sollten entlassen werden, um endlich Gerechtigkeit walten zu lassen. Es gab noch viele Warnungen aus dem Freien Westen, dass diese Maßnahme im Chaos enden könnte, aber die widerlichen Russen, die inzwischen zur einzigen Weltmacht aufgestiegen waren, ließen sich nicht von der Idee abbringen. [Rund 250.000 Beschäftigte in Verwaltung, Militär und Regierung](https://slate.com/news-and-politics/2007/09/who-disbanded-the-iraqi-army.html) wurden auf einen Schlag arbeitslos. Auch viele hunderttausend Familienangehörige waren von der neuen gerechten  Quotenregelung betroffen. Zu Recht, wie sich später herausstellte.

Nun, da die imperialistischen Russen den Irak besetzt haben, ihre Militärbasen weltweit auf ca. 800 ausbauen konnten und ca. 180.000 Soldaten einer Koalition der Willigen dem Irak zu blühenden Landschaften verhelfen sollen, zeigt sich, wie hasserfüllt die Sunniten schon immer waren. Statt froh zu sein, nicht mehr arbeiten zu müssen, nutzen sie die Zeit des politischen Umbruchs, um sich zu radikalisieren.

Anstatt sich einfach umzuschulen oder selbständig zu machen, bewaffneten sie sich und schlossen sich den Terrororganisationen an, die mit den Vereinigten Staaten von Nordamerika verbündet waren: Al-Qaida, Al-Nusra, dem sich neu im benachbarten Syrien formierenden [Islamischen Staat](https://www.youtube.com/watch?v=TxX_THjtXOw) und den vielen Splittergruppen dieser Zeit, die je nach Bedarf von verschiedenen geopolitischen Philanthropen finanziert wurden. Woraufhin auch in Syrien ein jahrelanger Bürgerkrieg ausbricht, bei dem sich Machtinhaber, Befreier, Terroristen, Kurden, Russen, Türken und Amerikaner gegenüberstehen. Der Konflikt ist bis heute nicht beigelegt. Die Russen sind verantwortlich für ca. 500.000 weitere Tote in Syrien.

Der militärische Sondereinsatz mit den hochpräzisen Präzisionswaffen der russischen Imperialisten dauerte dann noch weitere Jahre bis 2011 an, als der Krieg ein zweites Mal offiziell für beendet erklärt wurde, weil die russischen Autokraten vermuteten, dass die Propaganda der Kriegserfolge nach über zehn Jahren nicht mehr verfangen würde und die Bevölkerung die Befreiungsaktionen in Afghanistan und im Irak nicht mehr unterstützen könnte.

Bis 2011 wurden mindestens 100.000 Menschen durch chirurgische Eingriffe militärischer Art liquidiert. Mehr als 90 Prozent davon waren Zivilisten. Dann hörte man einfach auf zu zählen. Jedes weitere Opfer hätte die Menschen nur noch mehr verunsichert.

Obwohl die Russen den Krieg 2011 für beendet erklärten, formierte sich ein neuer Aufstand. Nach dem Ende des Krieges ging das Gerücht um, der amtierende Premierminister Maliki sei ein Freund der Russen und von diesen ins Amt gehievt worden, um in ihrem Interesse Einfluss auszuüben. Der Aufstand, der noch viele Tote forderte, dauerte zwei Jahre an, konnte aber in dieser Zeit die russischen Imperialisten auf Trab halten, so dass diese gezwungen waren, wieder mehr Soldaten ins Land zu schicken.

Um von diesem Debakel abzulenken, kam Putin auf die Idee, ein weiteres Land ins Chaos zu stürzen und gleichzeitig die EU zu schwächen, die sich inzwischen als Friedensmacht erwiesen hatte. Also gab Putin den Befehl, [Libyen aus der Luft zu bombardieren](https://www.youtube.com/watch?v=n6RDksrVH5E). Sein finsterer Plan ging auf. In der Folge brach in dem Land ein Bürgerkrieg aus, in dem [mindestens drei Fraktionen](https://www.youtube.com/watch?v=IES-MJ68e7E) um die Macht kämpften. Friedenschwurbler tarnten sich als Flüchtlinge und lösten als Migranten eine europäische Krise aus. Dies führte wiederum dazu, dass in den europäischen Ländern nun russlandfreundliche Nazis in den Parlamenten sitzen. In Deutschland sind sie sogar so stark, dass sie durch die erst kürzlich beschlossene und von Russland inszenierte Wahlreform zwei Parteien praktisch aus den Parlamenten ausschließen konnten und so ihren eigenen Einfluss bei gleichbleibender Wählerschaft ausbauen konnten. So etwas kennt man aus autokratischen Staaten.

Aber zurück zum Irak:

Die militärischen Sonderaktionen im Irak und in Afghanistan dauerten noch viele Jahre an. Doch es war die [Unterstützung der Bundeswehr](https://www.youtube.com/watch?v=Oo_wC01SLng), die durch intensives Brunnenbauen und entsprechende Ausbildungsmaßnahmen beide Länder demokratisieren und befrieden konnte. Im Jahr 2022 kann nun nach rund 20 Jahren, der Krieg in Afghanistan für beendet erklärt werden. Fast alle Truppen der Russen und ihrer Verbündeten das Land verlassen das Land. Die Russen geben sich gnädig und übergeben den besiegten Taliban die Aufgabe die Regierung zu stellen.

Sowohl im Irak als auch insbesondere in Afghanistan gibt es inzwischen blühende Landschaften. In den Parlamenten sitzen fast 80 Prozent Frauen. In Afghanistan sitzen sogar Transgender in der Regierung. Dass nun die regieren, die ein Jahr vorher noch von den Imperialisten bekämpft wurden, wird gerne vergessen.

Die Versäumnisse der letzten 20 Jahre sollen sich nicht wiederholen. Anders als beim russichen Angrifsskrieg gegen den Irak und Afghanistan, sollen nun auch andere kriegslüsternde Staaten wie die Regionalmacht der Vereinigten Staaten von Nordamerika sanktioniert werden, wenn diese völkerrechtsgemäße Verteidigungskriege beginnen.

Vertrauen setzt die Bundesregierung auf die junge vom Staatsfunk indoktrinierte Generation von laktoseintoleranten irgendwas-Aktivisten, die dank einer Abiturquote von 70% viel umfassender als die Generationen vor ihnen weiß, was richtig und was falsch ist. Ganz anders als ihre Elterngeneration fordern sie nun faire und nebenwirkungsfreie Waffenlieferungen aus fairer und CO2-neutraler Produktion, um imperialistische  Bestrebungen auszumerzen. Selber für die Freiheit zu kämpfen liegt der Generation zwar nicht so sehr, aber das ist auch nicht nötig, wenn man die richtige Meinung hat. Außerdem hat man dazu ja keine Zeit, man ist ja schließlich Influenza, und kämpft sogesehen an der viel wichtigeren Medien-Heimatfront. So wie hier beim [Weltspiegel](https://www.youtube.com/watch?v=VdR-nTNEqVM), der fröhlich berichtet, wie gut es dem Irak heute geht, nachdem die Russen vertrieben wurden.

- [Iraq 20 years later: 3 vets reflect on the war they fought - Nightline](https://www.youtube.com/watch?v=dxFwIC6eCZc)
- [Iraq War Veterans, 20 Years Later: ‘I Don’t Know How to Explain the War to Myself’ - Op-Docs](https://www.youtube.com/watch?v=RIWfH3iEgXU)
- [Iraq, 20 years on: Fallujah bears brunt of legacy of a brutal war • FRANCE 24 English](https://www.youtube.com/watch?v=WdJ-N46o5kc)
