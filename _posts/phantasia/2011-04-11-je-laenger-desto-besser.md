---
title: Je länger desto besser
category: phantasia
layout: default
---
Die Medien, u.a. ["Spiegel Online"](https://www.spiegel.de/wissenschaft/mensch/studie-von-koerpersignalen-fingerlaengen-verraten-attraktivitaet-von-maennern-a-758111.html) berichten von einer Studie, die herausgefunden haben soll, dass die Länge des Ringfingers eines Mannes mit seiner Attraktivität im kausalen Zusammenhang steht.

Ich nehme diese Information erfreut zur Kenntnis.

![Mein Ringfinger](){:data-src="/assets/images/hand_s.jpg" style="display:block; margin-left:auto; margin-right:auto; max-width: 100%" class="rounded lazyload" }

