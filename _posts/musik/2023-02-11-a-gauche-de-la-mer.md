---
title: A gauche de la mer
category: musik
layout: default
tags: "90er, Tresor, Detroit, House, Techno, AOL, Warehouse, Model, 500, Juan Atkins, Blake Baxter, Kevin Saunderson, Rap"
---
<hmmly-player id="agdlm" data-playlist-id="agdlm"></hmmly-player>
<!-- more -->
Als ich in den 1990ern häufiger im Tresor war, kam ich natürlich in Kontakt mit Detroit House / Techno. Dank des damals sich in den Kinderschuhen befindlichen Internets und meinem Modem einem [Elsa Microlink 28.8 TQV](https://de.wikipedia.org/wiki/Datei:Elsa_MicroLink_288TQV_mod01_res.jpg) und den zahlreichen [AOL CDs](https://media.npr.org/assets/news/2010/05/24/aoldisks-e6c88c4d7b360d6e0ceb65934d589a8513c414e3-s1100-c50.jpg) mit denen man sich wiederholt bis 90 Stunden, später sogar bis zu 1.000 Stunden oder drei Monaten kostenloses Internet besorgen konnte, habe ich damals ein interessantes Hobby entwickelt. - Die Stadt Detroit.

Nicht nur die DJs, die House- und Technomusik und die "Warehouse"-Tradition waren für mich interessant, sondern auch allgemein die Geschichte der Stadt [Detroit](https://de.wikipedia.org/wiki/Detroit). So kam es, dass ich mir über die letzten 30 Jahre ein recht gutes Wissen über die Stadt angeeignet habe.

Ich kenne die Gründung der Stadt als Fort durch die Franzosen - daher der Name, welche Straßen den ursprünglichen "Indian-Trails" nachempfunden sind, warum es teils eine chaotische Straßenführung gibt, was die Stadt mit Rassimus(z. B. [Autobahn durch Black Bottom](#agdlm){: #iSVgOCiT93Y .hmmly_link data-receiver="agdlm-player"}) zu tun hat und warum es zu [Ausschreitungen](#agdlm){: #xH-x7uGSDZM .hmmly_link data-receiver="agdlm-player"} kam. Welche [Mauer](#agdlm){: #kz_epu98Ib0 .hmmly_link data-receiver="agdlm-player"} und welche Autobahnen von Bedeutung sind. Welche [Bürgermeister der Stadt](#agdlm){: #MwAioSrD_MQ .hmmly_link data-receiver="agdlm-player"} in welchem Knast gelandet sind, usw. usf. Ich kann einige Bars und Sehenswürdigkeiten der Stadt auf einem Stadtplan zeigen. Ich kann mich fast wie ein einheimischer auf Google Streetview in der Stadt bewegen. Ich kenne die aktuellen Bauprojekte, die alten und neue Probleme der Stadt. Wo man gut mit dem [Fahrrad](#agdlm){: #-4xG5HEGsrk .hmmly_link data-receiver="agdlm-player"} durch die Stadt kommt. Ich kenne die Sportclubs und die neuesten Veranstaltungshallen. Die Tram und Buslinien. Wo es zu den Flughäfen geht oder zum Krankenhaus geht. Ich weiß wie die Bezirke heißen, wo sie sich befinden, wo es teuer ist und welche "Hoods" man lieber meiden sollte, wo es gerade aufwärts geht oder wie man nach [Kanada](#agdlm){: #qorV-_usqmI .hmmly_link data-receiver="agdlm-player"} kommt. Wo H&M einen neuen Laden eröffnet hat, wo man einen überteuerten Hipster-Kaffee bekommt, welche Personen für die Stadt prägend sind, die größten Arbeitgeber usw. usf.


Selbstverständlich kenne ich auch den ein oder anderen DJ und deren Musik und auch diesen einen weißen Typen aus der Dresden St. Ich hatte sogar echte Platten (z. B. von K-Hand † 3.8.2021), aber auch anderen. Es gibt deshalb von mir ein paar Lieder aus Detroit.

**Bonus**

1. [Model 500 - No Ufos, 1985](#agdlm){: #KNz01ty-kTQ .hmmly_link data-receiver="agdlm-player"}
2. [Juan Atkins - Technicolor, 1986](#agdlm){: 8Q0jZWUSe7I .hmmly_link data-receiver="agdlm-player"}
3. [Model 500 - The Flow M38, 1995](#agdlm){: #CDU2tU03j2M .hmmly_link data-receiver="agdlm-player"}
4. [Blake Baxter - Our Luv, 1997](#agdlm){: #75z2x-Und0w .hmmly_link data-receiver="agdlm-player"}
5. [Kevin Saunderson - Future, 2011](#agdlm){: #JwUpF50jHj4 .hmmly_link data-receiver="agdlm-player"}

Seit ca. 2010 kommt aus Detroit auch [hörenswerter Rap / Hip Hop](https://www.youtube.com/watch_videos?video_ids=s87QfplhjEs,l4gk_X5wvx4,RZL2ch9_GHY,r_ootw-igYA,kSZhtSk_F_A,UGvrGicHFoA,5_en0TtWFRI,iFbAREj-998,cnHs-oB2eCM,DMaZ_hRAu0A&origin=https://hammeley.info/ego/2023/02/11/a-gauche-de-la-mer/) jenseits der Chartplatzierungen.
